============================================================
                        LEGAL NOTICE
============================================================

This Legal Notice covers Supersys 1 by librehof.org

Contact: info@librehof.org
Updated: 2024-04-10


License information
============================================================

Files which contains "GPLv3" or "Apache 2.0" together with a 
Copyright Notice, are LICENSED to you under that License.

"GPLv2" can be used as a replacement for "Apache 2.0"

Built files INHERITS the Copyright Notice from their 
respective source files.

This literary work stands on its own when you link to it.
Linking does not create new "derivative" work.

Example-like files without a traceable Copyright Notice, 
and NOT mentioned here, can be considered Public Domain.

Additional License information:
- The meta directory contains identity information (branding)
- Icon derived from freeiconshop.com, free for all use


How to handle Copyright and License
============================================================

Copyright is held by the owner(s) of librehof.org unless
otherwise stated.

Make sure the License Type and Copyright Notice continues 
to be PRESENT in all files that the notice covers.

If you MODIFY a file with a Copyright Notice, ADD yourself 
(or your associated Legal Entity) to the list of 
Copyright Holders in that file.

If you make a request for your work to be merged back into 
the original, you may be asked to give up your Copyright.


Repo re-publishing
============================================================

When publishing a copy of this repo:
- Make sure to state that it is a copy
- Include a reference to the original

When publishing a modified version of this repo:
- Make sure it can be distinguished from the original
- Rename your repo and recreate the meta directory
- Include a reference to the original


-------------------------- END -----------------------------

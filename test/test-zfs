#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"
SUPERCMD=true
SUPERPATH="${SCRIPTPATH}/../bin"
. "${SUPERPATH}/superlib"

#=================================================

# supersys/test-zfs

summary()
{
  info ""
  info "Test ZFS RAID operations"
  info ""
  info "Usage:"
  info " [sudo] ./${SCRIPT}"
  info ""
}

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit
fi

#=================================================

title

checksuperuser
exitonerror ${?}

TESTPATH="${TMPPATH}/supersys"
clearlog "${TESTPATH}/test.log"
exitonerror ${?}

note "TESTPATH=${TESTPATH}"
note "LOGFILE=${LOGFILE}"
note "SUPERPM=${SUPERPM}"

sleep 1

IMAGE="${TESTPATH}/image.disk"
rm -f "${IMAGE}"

#-------------------------------------------------
separator "detach"

cd "${SUPERPATH}"
. ./supersys

# remove hidden bindings
zpool export ztest 1> /dev/null 2> /dev/null

./image-detach all
exitonerror ${?}

#-------------------------------------------------
separator "disk"

./image-create "${IMAGE}" 1600M
exitonerror ${?}

DISK=$(./image-attach "${IMAGE}")

./disk-format ${DISK} quick mbr
exitonerror ${?}

PART1="$(./diskpart-add ${DISK} 1 1M 399M)"
PART2="$(./diskpart-add ${DISK} 2 400M 399M)"
PART3="$(./diskpart-add ${DISK} 3 800M 399M)"
PART4="$(./diskpart-add ${DISK} 4 1200M 399M)"
partcount ${DISK} 4
exitonerror ${?}

#-------------------------------------------------
separator "format"

./format-zfs ${PART1},${PART2} ztest stripe
exitonerror ${?}

#-------------------------------------------------
separator "add,replace devices"

./voldevice-add ${PART3} ztest
exitonerror ${?}

div

./voldevice-replace ${PART3} ${PART4} ztest
exitonerror ${?}

#-------------------------------------------------
separator "subvolumes and mounting"

title "main"
sleep 1

./subvol-create ztest:main
exitonerror ${?}

./dirmount-zfs "${TESTPATH}/main" ztest:main
exitonerror ${?}

echo "main" > "${TESTPATH}/main/test.txt"

title "snapshot"
sleep 1

SNAPSHOT="$(./subvol-save ztest:main)"
exitonerror ${?}

./dirmount-zfs "${TESTPATH}/snapshot" ztest:${SNAPSHOT}
exitonerror ${?}

cmp "${TESTPATH}/main/test.txt" "${TESTPATH}/snapshot/test.txt"
exitonerror ${?} "main and snapshot not equal"

title "branch"
sleep 1

./subvol-branch ztest:main newmain
exitonerror ${?}

./dirmount-zfs "${TESTPATH}/newmain" ztest:newmain
exitonerror ${?}

cmp "${TESTPATH}/main/test.txt" "${TESTPATH}/newmain/test.txt"
exitonerror ${?} "main and newmain not equal"

echo "newmain" > "${TESTPATH}/newmain/test.txt"
echo "changed" > "${TESTPATH}/main/test.txt"

cmp --quiet "${TESTPATH}/main/test.txt" "${TESTPATH}/snapshot/test.txt"
if [ "${?}" = 0 ]; then
  errorexit "should be different 1"
fi

cmp --quiet "${TESTPATH}/main/test.txt" "${TESTPATH}/newmain/test.txt"
if [ "${?}" = 0 ]; then
  errorexit "should be different 2"
fi

#-------------------------------------------------
separator "cleanup"

./dirmount-remove "${TESTPATH}/main"
./dirmount-remove "${TESTPATH}/newmain"
./dirmount-remove "${TESTPATH}/snapshot"

# remove hidden bindings
zpool export ztest

./image-detach all
partcount ${DISK} 0

rm -f "${IMAGE}"

#-------------------------------------------------

ERRORS="$(grep "ERROR: " "${LOGFILE}")"
if [ "${ERRORS}" != "" ]; then
  separator "failed"
  errorexit
else
  separator "end"
fi

## Supersys 1

GPLv3 (C) 2024 [librehof.org](http://librehof.org)


### Summary

- Commands providing a simplified, well-structured interface to part of GNU/Linux
- A short summary with **useful hints** will be printed when given **-h** (--help)
- Made up of POSIX compliant shell scripts
- [Legal notice](NOTICE)


### Details

- [commands](doc/commands.md) **\<-- help texts!**
- [changelog](doc/changelog.md)
- see [test directory](test) for some example code


### Usefulness

- As an **introduction** to GNU/Linux
  - try things out before reading detailed man-pages
  - educate yourself by reading the command summaries (-h)  
  - play around with btrfs and zfs (snapshot capable file systems)  
- Enjoy a **well-structured** suite of commands when working daily with GNU/Linux 
- **Build**/**Examine** disk images
  - Launch image in QEMU/KVM (see [vimage-qcow2](doc/commands.md#vimage-qcow2))
  - Launch image in VirtualBox (see [vimage-vdi](doc/commands.md#vimage-qcow2) or [image-vmdk](doc/commands.md#image-vmdk))
- **Copy**/**Backup** partitions and partition table
- **Implement a lightweight backup/redundancy solution** using zfs|btrfs snapshots
  - Use subvol-timepush to efficiently stream changes to a secondary machine
  - Use subvol-dayrotate + dir-replicate to take daily "complete host" backups with history
  - Use subvol-save + dir-replicate to take intraday backups
- Write **setup scripts** on top of supersys
  - the **structure** of supersys makes your setup scripts become more explicit (easier to understand)


### Scripting

- Make use of **superlib** (part of supersys) when writing scripts on top of supersys
  - [superlib](doc/superlib.md) <-- see superlib repo at [librehof.org](http://librehof.org)
  - [coding](doc/coding.md) <-- some code patterns when writing superscripts 
  - [template](doc/template.sh) <-- script template to start with

  
### CAUTION!

- Use on **your own risk**
  - Test your scrips/commands thoroughly before running on any live system
  - As a user of supersys you are fully responsible for any damage ([license](LICENSE))
- **Backup** important files before trying out on your computer
- Consider using a **virtual machine** for extra safety
- Use **/dev/loop**[N] devices when trying out disk commands:
  - See [image-create](doc/commands.md#image-create) and [image-attach](doc/commands.md#image-attach) (disk simulation)
- Most supersys commands requires superuser elevation (sudo)
  - Make sure you **trust** the source from where you got this code
  - Or start out inside a **virtual machine** 
  - Or review the code before use


### Use in-place
```
# download and use
git clone --depth 2 https://gitlab.com/librehof/supersys1.git
cd supersys1
[sudo] bin/<command>

# update
cd supersys1
git pull --depth 2

# remove
rm -rf supersys1
```


### Use **system-wide** (install)

```
# download
git clone --depth 2 https://gitlab.com/librehof/supersys1.git

# install and use
cd supersys1
sudo ./install
[sudo] <command>

# update installed
cd supersys1
git pull --depth 2
sudo ./install

# uninstall
sudo rm-supersys
```

<br>

### Command forms

- **class**-*action*
- or **action**-*type* 

### Resource classes

|    Class    | Description                                                                              |
|:-----------:|:-------------------------------------------------------------------------------------------|
|   device    | block device (disk, partition or attached image file)                                      |
|    image    | raw image file (possibly sparse), containing disk or partition                             |
|   vimage    | virtual-machine compressed/sparse image file (qcow2, vdi or vmdk)                          |
|    disk     | disk device under /dev (diskpt+diskparts)                                                  |
|   diskpt    | disk's partition table (GPT or MBR)                                                        |
|  diskpart   | disk partition (will have its own sub-device under /dev)                                   |
|     vol     | file-system volume stored on device(s)                                                     |
|  voldevice  | device containing a single volume or part-of-a-RAID's volume                               |
| subvol<br>subdev | sub-volume (if using btrfs or zfs)<br>multiple sub-volumes can share the same device space |
|   rootfs    | directory structure with linux operating system (possibly bootable)                        |
|     dir     | directory in a volume                                                                      |
|  dirmount   | directory pointing to a device volume, network volume, sub-volume, ...                     |
|    file     | file in a volume                                                                           |
|    route    | network routing (gateway, iprange-\>nic, name->ip, ...)                                    |
|     nic     | network interface, with 0..n ip networks                                                   |
|  nicbridge  | virtual network interface, bridging a list of network interfaces                           |
|    nicip    | one ip network (connection to) via a network interface                                     |

<br>

### Working with image files

- **[image-create](doc/commands.md#image-create)**, **[image-attach](doc/commands.md#image-attach)** and **[image-vmdk](doc/commands.md#image-vmdk)** to work with "disk images"
- **[disk-format](doc/commands.md#disk-format)**, **[diskpart-add](doc/commands.md#diskpart-add)**, **[diskpart-list](doc/commands.md#diskpart-list)** and **[device-space](doc/commands.md#device-space)** to prepare and review a "disk"

### Saving and restoring data
 
- **[dir-replicate](doc/commands.md#dir-replicate)**, **[dir-save](doc/commands.md#dir-save)** and [dir-restore](doc/commands.md#dir-restore)
- **[diskpt-save](doc/commands.md#diskpt-save)** and [diskpt-restore](doc/commands.md#diskpt-restore) to save/restore "disk partition tables"
- **[device-save](doc/commands.md#device-save)** and [device-restore](doc/commands.md#device-restore) to save/restore "whole disks" or "partitions"

### Storage

- [Block devices](doc/blockdevices.md)

### File systems

- [Meta data](doc/metadata.md)
- **format**-*type* to create volumes (file systems)
- **dirmount**-*type* and **[dirmount-remove](doc/commands.md#dirmount-remove)** to mount volumes
- you can use **[dirmount-image](doc/commands.md#dirmount-image)** to mount volumes at/inside image files

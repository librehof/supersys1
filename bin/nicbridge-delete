#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# nicbridge-delete * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Delete network interface bridge"
  info ""
  info "Usage:"
  info " ${SCRIPT} <bridge>"
  info ""
  info "Example:"
  info " ${SCRIPT} br0"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

BRIDGE="${1}"

if [ "${BRIDGE}" = "" ] || [ "${2}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

"${SUPERPATH}/sw-ensure" ip @ iproute2

#-------------------------------------------------

ip link delete "${BRIDGE}" type bridge
exitonerror ${?} "Failed to create bridge ${BRIDGE}"

#-------------------------------------------------

#!/bin/sh

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# rootfs-efisdboot * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Setup UEFI boot of linux intel/amd64 [sd-boot/initrd]"
  info ""
  info "Usage:"
  info " ${SCRIPT} <rootdir> [<option> ...] [<param> ...]"
  info ""
  info "Boot options:"
  info " main         put as default boot in loader.conf"
  info " nvram        put in computer's nvram"
  info " menutime=<s> menu display time in seconds, default=0"
  info " overwrite    overwrite existing boot entry"
  info ""
  info "Some kernel params:"
  info " root=        rootfs, like root=UUID=<volid>, root=ZFS=<dataset>"
  info " rootflags=   mount options, like rootflags=subvol=<subvol>"
  info " see: https://wiki.archlinux.org/title/Kernel_parameters"
  info " and: https://docs.kernel.org/admin-guide/kernel-parameters.html"
  info " consider using mitigations=off for better performance (!)"
  info ""
  info "The rootdir can be the current rootfs or a guest rootfs"
  info "A guest rootfs MUST be prepared first, see rootfs-mount"
  info ""
  info "<rootdir> => mount to rootfs partition or subvol"
  info "<rootdir>/boot/efi => mount to esp partition (fat32)"
  info ""
  info "Structure of /boot/efi/"
  info " EFI/"
  info "   boot/ # default loader"
  info "   systemd/ # sd-boot loader"
  info " loader/"
  info "   loader.conf # loader config with default entry"
  info "   entries/<host>.conf # boot entry"
  info " <host>/"
  info "   vmlinuz # linux kernel, copied from /boot"
  info "   initrd  # initramfs or initrd, copied from /boot"
  info ""
  info "initrd = minimal rootfs (cpio archive) built from <rootdir>"
  info ""
  info "If '/boot/efi/loader/entries.srel' contains 'type1':"
  info " will not create host structure above (will rely on OS doing UAPI)"
  info " see: https://uapi-group.org/specifications/specs"
  info ""
  info "If exists (n = 1..9):"
  info " combines fstab.<n> into fstab, and crypttab.<n> into crypttab"
  info ""
  info "UEFI boot order may be stored in the computer's nvram"
  info "Verify boot order in the UEFI boot menu at first reboot"
  info ""
  info "Examples:"
  info " ${SCRIPT} / main overwrite root=UUID=<volid>"
  info " ${SCRIPT} /mnt/rootfs main # after rootfs-mount"
  info ""
  info "See: rootfs-bootmounts, rootfs-mount"
  info ""
}

#-------------------------------------------------

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

ROOTDIR="${1}"

if [ "${ROOTDIR}" = "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

ROOTDIR="$(abspath "${ROOTDIR}")"
exitonerror ${?}

#-------------------------------------------------

MAIN="false"
NVRAM="false"
MENUTIME="0"
OVERWRITE="false"
ROOT="false"
PARAMS="" # kernel params

option()
{
  rooteq="$(startstring "${1}" 5)"
  zfseq="$(startstring "${1}" 4)"
  if [ "${rooteq}" = "root=" ]; then
    ROOT="true"
  elif [ "${zfseq}" = "zfs=" ]; then
    ROOT="true"
  fi
  menutimeeq="$(startstring "${1}" 9)"
  if [ "${1}" = "main" ]; then
    MAIN="true"
  elif [ "${1}" = "nvram" ]; then
    NVRAM="true"
  elif [ "${menutimeeq}" = "menutime=" ]; then
    MENUTIME="$(endstring "${1}" 10)"
    checkint "${MENUTIME}" "menutime"
    exitonerror ${?}
  elif [ "${1}" = "overwrite" ]; then
    OVERWRITE="true"
  elif [ "${1}" != "" ]; then
    if [ "${PARAMS}" = "" ]; then
      PARAMS="${1}"
    else
      PARAMS="${PARAMS} ${1}"
    fi
  fi
}

option "${2}"
option "${3}"
option "${4}"
option "${5}"
option "${6}"
option "${7}"
option "${8}"
option "${9}"
option "${10}"
option "${11}"
option "${12}"

#-------------------------------------------------

# check

if [ ! -d "${ROOTDIR}" ]; then
  errorexit "Missing rootfs directory: ${ROOTDIR}"
fi

if [ "${ROOTDIR}" = "/" ]; then
  if [ ! -d "/bin" ]; then
    errorexit "Missing /bin"
  fi
  if [ ! -d "/dev" ]; then
    errorexit "Missing /dev"
  fi
  if [ ! -d "/boot" ]; then
    errorexit "Missing /boot"
  fi
else
  if [ ! -d "${ROOTDIR}/bin" ]; then
    errorexit "Missing ${ROOTDIR}/bin"
  fi
  if [ ! -d "${ROOTDIR}/dev" ]; then
    errorexit "Missing ${ROOTDIR}/dev"
  fi
  if [ ! -d "${ROOTDIR}/boot" ]; then
    errorexit "Missing ${ROOTDIR}/boot"
  fi
fi

#-------------------------------------------------

# guest?

if [ "${ROOTDIR}" != "/" ]; then

  note "Guest boot setup at ${ROOTDIR}"

  # copy superlib

  GUESTLIB="${ROOTDIR}/bin/superlib"
  if [ -f "${GUESTLIB}" ]; then
    GUESTLIB=""
  else
    cp "${SUPERPATH}/superlib" "${GUESTLIB}"
    exitonerror ${?} "Failed to copy to ${GUESTLIB}"
  fi

  # copy supersys

  GUESTSYS="${ROOTDIR}/bin/supersys"
  if [ -f "${GUESTSYS}" ]; then
    GUESTSYS=""
  else
    cp "${SUPERPATH}/supersys" "${GUESTSYS}"
    exitonerror ${?} "Failed to copy to ${GUESTSYS}"
  fi

  # copy script

  GUESTCMD="${ROOTDIR}/bin/${SCRIPT}"
  if [ -f "${GUESTCMD}" ]; then
    GUESTCMD=""
  else
    cp "${SUPERPATH}/${SCRIPT}" "${GUESTCMD}"
    exitonerror ${?} "Failed to copy to ${GUESTCMD}"
  fi

  # run script inside guest

  "${SUPERPATH}/rootfs-run" "${ROOTDIR}" "/bin/${SCRIPT} / ${2} ${3} ${4} ${5} ${6} ${7}"
  exitonerror ${?}

  # restore

  if [ "${GUESTLIB}" != "" ]; then
    rm -f "${GUESTLIB}"
  fi
  if [ "${GUESTSYS}" != "" ]; then
    rm -f "${GUESTSYS}"
  fi
  if [ "${GUESTCMD}" != "" ]; then
    rm -f "${GUESTCMD}"
  fi

  # done
  exit

fi

#-------------------------------------------------

# ROOTDIR="/" from here on

#-------------------------------------------------

# hostname

checkfile "/etc/hostname"
exitonerror ${?}

HOST="$(loadline "/etc/hostname")"
exitonerror ${?}

if [ "${HOST}" = "" ]; then
  errorexit "Blank /etc/hostname"
fi

#-------------------------------------------------

# get default vmlinuz and initrd

getvmlinuz()
{
  VMLINUZ=""
  if [ -e "${ROOTDIR}vmlinuz" ]; then
    VMLINUZ="${ROOTDIR}vmlinuz"
  elif [ -e "${ROOTDIR}boot/vmlinuz" ]; then
    VMLINUZ="${ROOTDIR}boot/vmlinuz"
  fi
}

getinitrd()
{
  INITRD=""
  if [ -e "${ROOTDIR}boot/initrd-${KVER}" ]; then
    INITRD="${ROOTDIR}boot/initrd-${KVER}"
  elif [ -e "${ROOTDIR}boot/initrd.img-${KVER}" ]; then
    INITRD="${ROOTDIR}boot/initrd.img-${KVER}"
  elif [ -e "${ROOTDIR}boot/initramfs-${KVER}" ]; then
    INITRD="${ROOTDIR}boot/initramfs-${KVER}"
  elif [ -e "${ROOTDIR}boot/initramfs.img-${KVER}" ]; then
    INITRD="${ROOTDIR}boot/initramfs.img-${KVER}"
  elif [ -e "${ROOTDIR}initrd.img" ]; then
    INITRD="${ROOTDIR}initrd.img"
  elif [ -e "${ROOTDIR}boot/initrd.img" ]; then
    INITRD="${ROOTDIR}boot/initrd.img"
  elif [ -e "${ROOTDIR}initramfs.img" ]; then
    INITRD="${ROOTDIR}initramfs.img"
  elif [ -e "${ROOTDIR}boot/initramfs.img" ]; then
    INITRD="${ROOTDIR}boot/initramfs.img"
  elif [ -e "${ROOTDIR}initramfs" ]; then
    INITRD="${ROOTDIR}initramfs"
  elif [ -e "${ROOTDIR}boot/initramfs" ]; then
    INITRD="${ROOTDIR}boot/initramfs"
  elif [ -e "${ROOTDIR}initrd" ]; then
    INITRD="${ROOTDIR}initrd"
  elif [ -e "${ROOTDIR}boot/initrd" ]; then
    INITRD="${ROOTDIR}boot/initrd"
  fi
}

#-------------------------------------------------

# combine fstab/crypttab if exists

addfile1to9 "/etc/fstab" '#'
exitonerror ${?}

addfile1to9 "/etc/crypttab" '#'
exitonerror ${?}

#-------------------------------------------------

# rootdir needs to be a mount-point

MOUNTS="$(mount)"
exitonerror ${?} "Failed to list mounts"

FOUND="$(printf '%s' "${MOUNTS}" | grep " /boot/efi ")"
if [ "${FOUND}" = "" ]; then
  errorexit "Missing /boot/efi mount-point (fat32 esp partition)"
fi

FOUND="$(printf '%s' "${MOUNTS}" | grep " / ")"
if [ "${FOUND}" = "" ]; then
  errorexit "Boot setup only works if rootfs is a mount-point"
fi

MOUNTPOINT="$(printf '%s' "${FOUND}" | head -1)"

# /dev needs to be populated with devices

COUNT="$(ls -1 "/dev" | wc -l)"
if [ "${COUNT}" -lt 2 ]; then
  errorexit "Unpopulated /dev directory (see rootfs-bind)"
fi

ZFS="false"
FOUND="$(printf '%s' "${MOUNTPOINT}" | grep ' type zfs ')"
if [ "${FOUND}" != "" ]; then
  ZFS="true"
fi

#-------------------------------------------------

# list UUID/ZFS mounts in fstab and crypttab

if [ -f "/etc/crypttab" ]; then
  UUIDS="$(grep " UUID=" "/etc/crypttab")"
  if [ "${UUIDS}" != "" ]; then
    note "UUID mounts in /etc/crypttab:"
    for UUID in ${UUIDS}
    do
      note " ${UUID}"
    done
    sleep 1
    ensurecmd cryptsetup @ cryptsetup
    ensurepkg cryptsetup-initramfs 1> /dev/null 2> /dev/null
  fi
fi

if [ -f "/etc/fstab" ]; then
  # uuid
  UUIDS="$(grep "^UUID=" "/etc/fstab")"
  if [ "${UUIDS}" = "" ]; then
    note "No UUID mounts in /etc/fstab"
  else
    note "UUID mounts in /etc/fstab:"
    for UUID in ${UUIDS}
    do
      note " ${UUID}"
    done
  fi
  # zfs
  ZSETS="$(grep "zfs" "/etc/fstab")"
  if [ "${ZSETS}" != "" ]; then
    note "ZFS in /etc/fstab:"
    for ZSET in ${ZSETS}
    do
      note " ${ZSET}"
    done
  fi
  # btrfs
  FOUND="$(grep "btrfs" "/etc/fstab")"
  if [ "${FOUND}" != "" ]; then
    ensurecmd btrfs @ zypper:btrfsprogs btrfs-progs btrfs-tools
    exitonerror ${?}
  fi
  # devmapper
  MAPS="$(grep "^/dev/mapper/" "/etc/fstab")"
  if [ "${MAPS}" != "" ]; then
    note "Mapper mounts in /etc/fstab:"
    for MAP in ${MAPS}
    do
      note " ${MAP}"
    done
  fi
else
  note "No /etc/fstab"
fi

sleep 1

#-------------------------------------------------

action "Checking machine id"

ensurenofile "${ROOTDIR}/var/lib/dbus/machine-id"
exitonerror ${?}

MACHINEID=""
if [ -e "${ROOTDIR}/etc/machine-id" ]; then
  MACHINEID="$(loadline "${ROOTDIR}/etc/machine-id")"
  exitonerror ${?}
fi

if [ "${MACHINEID}" = "" ]; then
  MACHINEID="$(genhexid)"
  echo "${MACHINEID}" > "${ROOTDIR}/etc/machine-id"
fi

mkdir -p "${ROOTDIR}/var/lib/dbus"
ln -s "/etc/machine-id" "${ROOTDIR}/var/lib/dbus/machine-id"
exitonerror ${?} "Failed to make symlink: ${ROOTDIR}/var/lib/dbus/machine-id"

MACHINEID="$(loadline "${ROOTDIR}/etc/machine-id")"
exitonerror ${?}

if [ "${MACHINEID}" = "" ]; then
  errorexit "Blank machine-id"
fi

note " machine-id: ${MACHINEID}"

#-------------------------------------------------

action "Installing systemd bootloader at /boot/efi"

LOADERCONF="/boot/efi/loader/loader.conf"
BOOTCONF="/boot/efi/loader/entries/${HOST}.conf"

# remove existing hooks (efisdboot)

HOOK="/etc/kernel/postinst.d/zz-efisdboot"
ensurenofile "${HOOK}"

HOOK="/etc/initramfs-tools/hooks/zz-efisdboot"
ensurenofile "${HOOK}"

DRACUTDIR="/usr/lib/dracut/modules.d/00efisdboot"
ensurenodir "${DRACUTDIR}"

# use EFI "host" structure if EFI is empty (non-UAPI)

ensuredir "/boot/efi/loader"
exitonerror ${?}

if [ ! -f "/boot/efi/loader/entries.srel" ]; then
  # first time linux
  saveline "host" "/boot/efi/loader/entries.srel"
  exitonerror ${?}
  if [ ! -f "${LOADERCONF}" ]; then
    clearfile "${LOADERCONF}"
    exitonerror ${?}
    appendline "default  ${HOST}.conf" "${LOADERCONF}"
    appendline "timeout  ${MENUTIME}" "${LOADERCONF}"
    appendline "#console-mode max" "${LOADERCONF}"
    appendline "editor   no" "${LOADERCONF}"
    exitonerror ${?}
  fi
fi

# ensure bootctl (separated from systemd on debian 12)
  
ensurecmd bootctl @ systemd-boot 1> /dev/null 2> /dev/null

# install boot loader

if [ "${NVRAM}" = "true" ]; then
  bootctl install --esp-path=/boot/efi
  exitonerror ${?} "bootctl failed"
else
  bootctl install --esp-path=/boot/efi --no-variables
  exitonerror ${?} "bootctl failed"
fi

#-------------------------------------------------

# check UAPI

UAPI="false"

if [ -f "/boot/efi/loader/entries.srel" ] && [ -d "/boot/efi/${MACHINEID}" ]; then
  TYPE="$(loadline "/boot/efi/loader/entries.srel")"
  if [ "${TYPE}" = "type1" ] ; then
    UAPIENTRIES="$(ls -1 /boot/efi/loader/entries/${MACHINEID}*.conf 2> /dev/null)"
    if [ "${UAPIENTRIES}" != "" ]; then
      note "Detected UAPI EFI with machine-id entries (/efi/loader/entries.srel)"
      UAPI="true"
      sleep 1
    fi
  fi
fi

#-------------------------------------------------

# check default boot

FOUNDHOSTMAIN=""
if [ -f "${LOADERCONF}" ]; then
  FOUNDHOSTMAIN="$(grep "^default" "${LOADERCONF}" | grep "${HOST}.conf")"
fi

if [ "${UAPI}" = "true" ]; then
  # machine-id structure
  if [ "${FOUNDHOSTMAIN}" = "" ]; then
    # remove host entry (use UAPI)
    ensurenofile "${BOOTCONF}" note
    exitonerror ${?}
    ensurenodir "/boot/efi/${HOST}" note
    exitonerror ${?}
  else
    # continue with host entry
    note "UAPI machine-id structure with ${HOST}.conf being default"
    UAPI="false"
    sleep 1
  fi
else
  # host structure
  if [ "${FOUNDHOSTMAIN}" != "" ]; then
    # remove machine-id entry/entries
    rm -rf /boot/efi/loader/entries/${MACHINEID}*.conf 2> /dev/null
    ensurenodir "/boot/efi/${MACHINEID}" note
    exitonerror ${?}
  fi
fi

#-------------------------------------------------

# loader.conf

MAINOVERWRITE="false"
if [ "${MAIN}" = "true" ] && [ "${OVERWRITE}" = "true" ]; then
  MAINOVERWRITE="true"
fi

if [ ! -f "${LOADERCONF}" ] || [ "${MAINOVERWRITE}" = "true" ]; then
  if [ "${UAPI}" = "false" ]; then
    note "Writing loader.conf"
    clearfile "${LOADERCONF}"
    exitonerror ${?}
    appendline "default  ${HOST}.conf" "${LOADERCONF}"
    appendline "timeout  ${MENUTIME}" "${LOADERCONF}"
    appendline "#console-mode max" "${LOADERCONF}"
    appendline "editor   no" "${LOADERCONF}"
    exitonerror ${?}
  fi
fi

#-------------------------------------------------

# remove old style hooks (sdboot)

if [ "${UAPI}" = "false" ]; then
  HOOK="/etc/kernel/postinst.d/zz-sdboot"
  ensurenofile "${HOOK}" note
  HOOK="/etc/initramfs-tools/hooks/zz-sdboot"
  ensurenofile "${HOOK}" note
  DRACUTDIR="/usr/lib/dracut/modules.d/00sdboot"
  ensurenodir "${DRACUTDIR}" note
fi

#-------------------------------------------------

# prepare for initrd

iscmd "update-initramfs"
UPDATEINITRAMFS=${?}

iscmd "dracut"
DRACUT=${?}

if [ "${UPDATEINITRAMFS}" != 0 ] && [ "${DRACUT}" != 0 ]; then
  ensurepkg apt:initramfs-tools dracut initramfs-tools
  exitonerror ${?}
fi

iscmd "update-initramfs"
UPDATEINITRAMFS=${?}

iscmd "dracut"
DRACUT=${?}

if [ "${UPDATEINITRAMFS}" != 0 ] && [ "${DRACUT}" != 0 ]; then
  errorexit "Missing update-initramfs or dracut (please install)"
fi

#-------------------------------------------------

# get current kernel

# sanity check
VMLINUZLIST="$(ls -1 /boot | grep "^vmlinuz" 2> /dev/null)"
if [ "${VMLINUZLIST}" = "" ]; then
  warning 'Missing /boot/vmlinuz* (no kernel installed?)'
  keywait 3
fi

getvmlinuz

if [ "${VMLINUZ}" = "" ]; then
  errorexit "Can not find current kernel (/vmlinuz or /boot/vmlinuz)"
fi

VMLINUZ="$(truepath "${VMLINUZ}")"
KERNEL="$(basename "${VMLINUZ}")"

# try extract kernel version
KVER=""
findforward "${KERNEL}" "-"
if [ "${?}" = 0 ]; then
  KVER="$(endstring "${KERNEL}" $((pos+2)))"
fi

#-------------------------------------------------

# RESUME=none

if [ -d "/etc/initramfs-tools" ]; then
  # if no resume file => turn off resume from swap (disable hibernation)
  ensuredir "/etc/initramfs-tools/conf.d"
  exitonerror ${?}
  RESUMEFILE="/etc/initramfs-tools/conf.d/resume"
  if [ ! -e "${RESUMEFILE}" ]; then
    note "Disabling resume from swap (see ${RESUMEFILE})"
    saveline   "# RESUME=<swapdev>|none" "${RESUMEFILE}"
    appendline "RESUME=none" "${RESUMEFILE}"
    exitonerror ${?}
    sleep 1
  fi
fi

#-------------------------------------------------

# ensure up-to-date initrd

# no initrd/initramfs?
INITRDLIST="$(ls -1 /boot | grep "^initr" 2> /dev/null)"

if [ "${UPDATEINITRAMFS}" = 0 ]; then
  # *** update-initramfs ***
  if [ "${INITRDLIST}" = "" ]; then
    # create
    if [ "${KVER}" = "" ]; then
      action "Creating new initrd/initramfs (update-initramfs -c -k all)"
      sleep 2
      update-initramfs -c -k all
      exitonerror ${?} "Failed to create initrd/initramfs"
    else
      action "Creating new initrd/initramfs (update-initramfs -c -k ${KVER})"
      sleep 2
      update-initramfs -c -k "${KVER}"
      exitonerror ${?} "Failed to create initrd/initramfs"
    fi
  else
    # update
    if [ "${KVER}" = "" ]; then
      action "Updating initrd/initramfs (update-initramfs -u -k all)"
      update-initramfs -u -k all
      exitonerror ${?} "Failed to update initrd/initramfs"
    else
      action "Updating initrd/initramfs (update-initramfs -u -k ${KVER})"
      update-initramfs -u -k ${KVER}
      exitonerror ${?} "Failed to update initrd/initramfs"
    fi
  fi
else
  # *** dracut *** (--no-hostonly installs ALL drivers, not just host specific)
  if [ "${KVER}" = "" ]; then
    action "Updating initrd/initramfs (dracut --force --no-hostonly)"
    dracut --force --no-hostonly
    exitonerror ${?} "Failed to update initrd/initramfs"
  else
    action "Updating initrd/initramfs (dracut --force --no-hostonly --kver ${KVER})"
    dracut --force --no-hostonly --kver "${KVER}"
    exitonerror ${?} "Failed to update initrd/initramfs"
  fi
fi

#-------------------------------------------------

# initrd check

getinitrd

if [ "${INITRD}" = "" ]; then
  errorexit 'Can not find current initrd (/initr* or /boot/initr*)'
fi

#-------------------------------------------------

# zfs check

if [ "${ZFS}" = "true" ]; then
  note "Detected ZFS (ignore any cryptsetup errors related to ZFS)"
  sleep 1
  iscmd "dkms"
  if [ "${?}" != 0 ]; then
    warning "Missing dkms command, can not verify ZFS dkms module!"
    info "Press ENTER to continue, CTRL-C to abort"
    keywait
  else
    if [ "${KVER}" = "" ]; then
      warning "Could not get kernel version, can not verify ZFS module!"
      info "Press ENTER to continue, CTRL-C to abort"
      keywait
      ensurepkg linux-headers
    else
      FOUND="$(dkms status | grep "^zfs" | grep "${KVER}")"
      if [ "${FOUND}" = "" ]; then
        ensurepkg linux-headers-${KVER} linux-headers
      fi
      FOUND="$(dkms status | grep "^zfs" | grep "${KVER}")"
      if [ "${FOUND}" = "" ]; then
        warning "Could not find ZFS dkms module for current kernel (${KVER})"
        sleep 1
        info "If ZFS is missing for current kernel the system may not boot!"
        info "Press ENTER to continue, CTRL-C to abort"
        keywait
      else
        note "Detected ZFS dkms module for current kernel (${KVER})"
        sleep 1
      fi
    fi
  fi
fi

#-------------------------------------------------

# ensure vmlinuz/initrd

if [ "${UAPI}" = "false" ]; then

  action "Updating EFI vmlinuz and initrd"

  ensuredir "/boot/efi/${HOST}"
  exitonerror ${?}

  ensurecopy "${VMLINUZ}" "/boot/efi/${HOST}/vmlinuz"
  exitonerror ${?}

  ensurecopy "${INITRD}" "/boot/efi/${HOST}/initrd"
  exitonerror ${?}

  # twice to apply mtime rounding

  ensurecopy "${VMLINUZ}" "/boot/efi/${HOST}/vmlinuz"
  exitonerror ${?}

  ensurecopy "${INITRD}" "/boot/efi/${HOST}/initrd"
  exitonerror ${?}

fi

#-------------------------------------------------

# boot entry

# establish final boot options
if [ "${ROOT}" = "false" ]; then
  # try auto-generate root param
  volumedevice "${ROOTDIR}"
  if [ ! -b "${VOLDEVICE}" ]; then
    errorexit "Failed to generate root parameter (can not find rootdir device)"
  fi
  VOLID="$(volumeid "${VOLDEVICE}")"
  exitonerror ${?} "Failed to generate root parameter (can not find rootdir volid)"
  if [ "${PARAMS}" = "" ]; then
    PARAMS="root=UUID=${VOLID}"
  else
    PARAMS="root=UUID=${VOLID} ${PARAMS}"
  fi
  note "root=UUID=${VOLID}"
fi

if [ "${UAPI}" = "false" ]; then
  # write host entry
  ensuredir "/boot/efi/loader/entries"
  exitonerror ${?}
  if [ ! -e "${BOOTCONF}" ] || [ "${OVERWRITE}" = "true" ]; then
    note "Writing ${HOST}.conf"
    clearfile "${BOOTCONF}"
    exitonerror ${?}
    appendline "title   ${HOST}" "${BOOTCONF}"
    appendline "linux   /${HOST}/vmlinuz" "${BOOTCONF}"
    appendline "initrd  /${HOST}/initrd" "${BOOTCONF}"
    appendline "options ${PARAMS}" "${BOOTCONF}"
    exitonerror ${?}
  fi
else
  # UAPI: check default boot / remove host entry
  FOUND=""
  if [ -f "${LOADERCONF}" ]; then
    FOUND="$(grep "${HOST}.conf" "${LOADERCONF}")"
    if [ "${FOUND}" = "" ]; then
      # remove host entry (use UAPI)
      ensurenofile "${BOOTCONF}" note
      exitonerror ${?}
    else
      note "UAPI machine-id structure with ${HOST}.conf beeing default"
      sleep 1
    fi
  fi
fi

#-------------------------------------------------

# set default boot?

if [ "${MAIN}" = "true" ]; then
  if [ "${UAPI}" = "false" ]; then
    note "Default boot: ${HOST}.conf"
    clearfile "${LOADERCONF}.new"
    exitonerror ${?}
    appendline "default  ${HOST}.conf" "${LOADERCONF}.new"
    exitonerror ${?}
    grep -v "^default" "${LOADERCONF}" >> "${LOADERCONF}.new"
    ensurenofile "${LOADERCONF}"
    exitonerror ${?}
    mv "${LOADERCONF}.new" "${LOADERCONF}"
    exitonerror ${?} "Failed to replace ${LOADERCONF} (system may not boot)"
  else
    note "UAPI default boot"
  fi
fi

#-------------------------------------------------

# update

"${SUPERPATH}/rootfs-efisdupdate"

#-------------------------------------------------

# overwrite UAPI option(s)

if [ "${UAPI}" = "true" ] && [ "${OVERWRITE}" = "true" ]; then
  ENTRIES="$(ls -1 /boot/efi/loader/entries/${MACHINEID}*.conf 2> /dev/null)"
  for ENTRY in ${ENTRIES}
  do
    CONF="${ENTRY}"
    action "Updating options at ${CONF}"

    clearfile "${CONF}.new"
    exitonerror ${?}

    LINES="$(cat "${CONF}")"
    exitonerror ${?} "Failed to load ${CONF}"
    for LINE in ${LINES}
    do
      optionkey="$(startstring "${LINE}" 6)"
      if [ "${optionkey}" = "option" ]; then
        LINE="options ${PARAMS}"
      fi
      appendline "${LINE}" "${CONF}.new"
      exitonerror ${?}      
    done

    ensurenofile "${CONF}"
    exitonerror ${?}

    mv "${CONF}.new" "${CONF}"
    exitonerror ${?} "Failed to replace ${CONF} (system may not boot)"
  done
fi

#-------------------------------------------------

# hooks

if [ "${UAPI}" = "false" ] || [ -d "/boot/efi2" ]; then

  action "Enabling update hooks"

  # kernel hook
  if [ -d "/etc/kernel/postinst.d" ]; then
    HOOK="/etc/kernel/postinst.d/zz-efisdboot"
    note " writing: ${HOOK}"
    saveline '#!/bin/sh' "${HOOK}"
    appendline 'if [ -e "/boot/efi/$(hostname)" ]; then' "${HOOK}"
    appendline '  rootfs-efisdupdate' "${HOOK}"
    appendline 'fi' "${HOOK}"
    chmod +x "${HOOK}"
  fi

  # initramfs hook
  if [ -d "/etc/initramfs-tools/hooks" ]; then
    HOOK="/etc/initramfs-tools/hooks/zz-efisdboot"
    note " writing: ${HOOK}"
    saveline '#!/bin/sh' "${HOOK}"
    appendline 'if [ -e "/boot/efi/$(hostname)" ]; then' "${HOOK}"
    appendline '  if [ "${1}" != "" ]; then' "${HOOK}"
    appendline '    exit 0' "${HOOK}"
    appendline '  fi' "${HOOK}"
    appendline '  rootfs-efisdupdate / wait=mkinitramfs &' "${HOOK}"
    appendline 'fi' "${HOOK}"
    chmod +x "${HOOK}"
  fi

  # dracut module hook
  if [ -d "/usr/lib/dracut/modules.d" ]; then
    DRACUTDIR="/usr/lib/dracut/modules.d/00efisdboot"
    ensuredir "${DRACUTDIR}"
    exitonerror ${?}
    DRACUTSETUP="${DRACUTDIR}/module-setup.sh"
    note " writing: ${DRACUTSETUP}"
    saveline '#!/bin/sh' "${DRACUTSETUP}"
    appendline 'check() {' "${DRACUTSETUP}"
    appendline '  return 0' "${DRACUTSETUP}"
    appendline '}' "${DRACUTSETUP}"
    appendline 'depends() {' "${DRACUTSETUP}"
    appendline '  return 0' "${DRACUTSETUP}"
    appendline '}' "${DRACUTSETUP}"
    appendline 'cmdline() {' "${DRACUTSETUP}"
    appendline '  return 0' "${DRACUTSETUP}"
    appendline '}' "${DRACUTSETUP}"
    appendline 'install() {' "${DRACUTSETUP}"
    appendline '  return 0' "${DRACUTSETUP}"
    appendline '}' "${DRACUTSETUP}"
    appendline 'installkernel() {' "${DRACUTSETUP}"
    appendline '  rootfs-efisdupdate / wait=dracut &' "${DRACUTSETUP}"
    appendline '}' "${DRACUTSETUP}"
  fi

fi

#-------------------------------------------------

note "EFI loaders:"

LOADERDIRS="$(ls -1 "/boot/efi/EFI" 2> /dev/null)"
for LOADERDIR in ${LOADERDIRS}
do
  note " ${LOADERDIR}"
done

note "EFI entries:"

ENTRIES="$(ls -1 "/boot/efi/loader/entries" 2> /dev/null)"
for ENTRY in ${ENTRIES}
do
  note " ${ENTRY}"
done

note "Kernel parameters: ${PARAMS}"

#-------------------------------------------------

info "NOTE: Verify boot order in the UEFI menu on first boot"

sleep 1
sync # disk commit

if [ "${MAIN}" = "true" ]; then
  success "Activated: efi/${HOST} => ${MOUNTPOINT}"
else
  success "Installed: efi/${HOST} => ${MOUNTPOINT}"
fi

#-------------------------------------------------

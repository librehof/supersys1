#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# voldevice-replace * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Replace device in existing RAID file system (volume)"
  info ""
  info "Usage (btrfs):"
  info " ${SCRIPT} <device>|<devid> <newdevice> <vol>"
  info ""
  info "Usage (zfs):"
  info " ${SCRIPT} <device>|<devname> <newdevice> <vol>"
  info ""
  info "vol: /<mountdir>|<device>|<volid>|<label>"
  info ""
  info "BTRFS: optionally use vol-balance afterwards"
  info ""
  info "See also: vol-info, voldevice-remove"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICE="${1}"
NEWDEVICE="${2}"
VOL="${3}"

if [ "${DEVICE}" = "" ] || [ "${NEWDEVICE}" = "" ]; then
  inputexit
fi

if [ "${VOL}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

superlock
exitonerror ${?}

. "${SUPERPATH}/supersys"

NEWDEVICE="$(truepath "${NEWDEVICE}")"
exitonerror ${?}

checkdevice "${NEWDEVICE}"
exitonerror ${?}

volumedevice "${VOL}"
exitonerror ${?}

VTYPE="$(volumetype "${VOLDEVICE}")"
VLABEL="$(volumelabel "${VOLDEVICE}")"

#-------------------------------------------------

if [ "${VTYPE}" = "btrfs" ]; then
  ensurecmd btrfs @ zypper:btrfsprogs btrfs-progs btrfs-tools
  exitonerror ${?}
  if [ "${VLABEL}" = "" ]; then
    VLABEL="$(basename "${VOLDEVICE}")"
  fi
elif [ "${VTYPE}" = "zfs" ]; then
  ensurecmd zpool @ zfsutils-linux zfs
  exitonerror ${?}
  if [ "${VLABEL}" = "" ]; then
    errorexit "Missing volume label for ${VOLDEVICE}"
  fi
else
  errorexit "Unsupported type at ${VOLDEVICE} (${VTYPE})"
fi

#-------------------------------------------------

# check/unmount new device

SPACEM=$("${SUPERPATH}/device-space" "${NEWDEVICE}" mib)
exitonerror ${?}

if [ ${SPACEM} -lt 8 ]; then
  errorexit "Expected ${NEWDEVICE} (${SPACEM} MiB) to be at least 8 MiB"
fi

unmountall "${NEWDEVICE}"
exitonerror ${?}

#-------------------------------------------------

action "Replacing ${DEVICE} with ${NEWDEVICE} at ${VLABEL} (${VOLDEVICE})"

clearfile "${RUNPATH}/voldevice.log"

if [ "${VTYPE}" = "btrfs" ]; then

  mkdir "${RUNMOUNT}"
  mount -t btrfs "${DEVICE}" "${RUNMOUNT}"
  exitonerror ${?} "Failed to mount ${DEVICE}"

  btrfs replace start -Bfr "${DEVICE}" "${NEWDEVICE}" "${RUNMOUNT}" >> "${RUNPATH}/voldevice.log"
  exitonerror ${?} "Failed to replace ${DEVICE} with ${NEWDEVICE} at ${VLABEL} (btrfs)"

  sync # disk commit
  success "Replaced ${DEVICE} with ${NEWDEVICE} at ${VLABEL} (btrfs)"

elif [ "${VTYPE}" = "zfs" ]; then

  FSINFO="$(zpool status "${VLABEL}")"
  exitonerror ${?} "Failed to probe zfs volume (${VLABEL})"

  zpool replace "${VLABEL}" "${DEVICE}" "${NEWDEVICE}" >> "${RUNPATH}/voldevice.log"
  exitonerror ${?} "Failed to replace ${DEVICE} with ${NEWDEVICE} at ${VLABEL} (zfs)"

  sync # disk commit
  success "Replaced ${DEVICE} with ${NEWDEVICE} at ${VLABEL} (zfs)"

fi

#-------------------------------------------------

#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# dirmount-nfsv3 * GPLv3 (C) 2024 librehof.org

summary()
{
  info ""
  info "Mount NFS (Network File System): mountdir => exported nfs"
  info ""
  info "Usage (mount):"
  info " ${SCRIPT} <mountdir> <host>:<path> [\"<options>\"]"
  info ""
  info "Creates <mountdir> if missing*"
  info "(*) permissions inherited from parent directory"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

POINT="${1}"
HOSTSHARE="${2}"
OPTIONS="${3}"

if [ "${POINT}" = "" ] || [ "${HOSTSHARE}" = "" ] ; then
  inputexit
fi

if [ "${4}" != "" ]; then
  inputexit
fi

ensurepkg nfs-common

#-------------------------------------------------

checksuperuser
exitonerror ${?}

POINT="$(truepath "${POINT}")"
exitonerror ${?}

#-------------------------------------------------

# already mounted?
MOUNTED="$(mount | grep "${HOSTSHARE} on " | grep " on ${POINT} ")"
if [ "${MOUNTED}" != "" ]; then
  # silent exit
  exit
fi

# mount-point check
MOUNTED="$(mount | grep " on ${POINT} ")"
if [ "${MOUNTED}" != "" ]; then
  echo "${MOUNTED}"
  errorexit "Underlying ${POINT} points to other mount"
fi

#-------------------------------------------------

if [ ! -d "${POINT}" ]; then
  # new
  ensuredir "${POINT}"
  exitonerror ${?}
else
  # exists
  CONTENT=$(ls -A -1 "${POINT}/")
  if [ "${CONTENT}" != "" ]; then
    error "Underlying ${POINT} is not empty"
    exit 2
  fi
fi

#-------------------------------------------------

action "Mounting ${HOSTSHARE} (nfsv3)"

if [ "${OPTIONS}" = "" ]; then
  mount -t nfs "${HOSTSHARE}" "${POINT}" -o "nfsvers=3"
  STATUS=${?}
else
  mount -t nfs "${HOSTSHARE}" "${POINT}" -o "${OPTIONS},nfsvers=3"
  STATUS=${?}
fi

if [ ${STATUS} != 0 ]; then
  rmdir "${POINT}" # only removed if empty
  error "Failed to mount ${HOSTSHARE} at ${POINT} (exit ${STATUS})"
  exit ${STATUS}
fi

success "Mounted ${HOSTSHARE} at ${POINT}"

#-------------------------------------------------

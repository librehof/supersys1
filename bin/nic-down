#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# nic-down * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Bring network interface down"
  info ""
  info "Usage:"
  info " ${SCRIPT} <nic>"
  info ""
  info "Use nic-list to get available network interfaces"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

NIC="${1}"

if [ "${NIC}" = "" ] || [ "${2}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

"${SUPERPATH}/sw-ensure" ip @ iproute2

#-------------------------------------------------

ip link set "${NIC}" down
exitonerror ${?} "Failed to bring ${NIC} down"

#-------------------------------------------------

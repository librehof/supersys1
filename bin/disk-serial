#!/bin/sh

IMPACT="read"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# disk-serial * GPLv3 (C) 2024 librehof.org

summary()
{
  info ""
  info "Print disk's serial number (given by the vendor)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <diskdevice>"
  info ""
  info "A blank line will be printed if serial number is missing"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DISK="${1}"

if [ "${DISK}" = "" ] || [ "${2}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

DISK="$(truepath "${DISK}")"
exitonerror ${?}

checkdevice "${DISK}"
exitonerror ${?}

#-------------------------------------------------

lsblk --nodeps -n -o SERIAL "${DISK}"

#-------------------------------------------------

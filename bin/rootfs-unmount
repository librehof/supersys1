#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# rootfs-unmount * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Unmount guest rootfs"
  info ""
  info "Usage:"
  info " ${SCRIPT} <rootdir> [<luksdevice>]"
  info ""
  info "Optionally give the luks encrypted device, to close,"
  info "if rootfs is stored encrypted"
  info ""
  info "See: rootfs-mount, device-closecrypt"
  info ""
}

#-------------------------------------------------

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

ROOTDIR="${1}"
CRYPTDEV="${2}"

if [ "${ROOTDIR}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

ROOTDIR="$(truepath "${ROOTDIR}")"
exitonerror ${?}

if [ "${ROOTDIR}" = "/" ]; then
  errorexit "Can not unmount at host's rootfs"
fi

checkdir "${ROOTDIR}"
exitonerror ${?}

if [ "${CRYPTDEV}" != "" ]; then
  CRYPTDEV="$(truepath "${CRYPTDEV}")"
  exitonerror ${?}
  checkdevice "${CRYPTDEV}"
  exitonerror ${?}
  TYPE="$(volumetype "${CRYPTDEV}")"
  if [ "${TYPE}" != "crypto_LUKS" ]; then
    errorexit "Not a luks encrypted device: ${CRYPTDEV}"
  fi
fi

#-------------------------------------------------

"${SUPERPATH}/rootfs-release" "${ROOTDIR}"

FOUND="$(mount | grep "${ROOTDIR}/boot/efi")"
if [ "${FOUND}" != "" ]; then
  # efi
  "${SUPERPATH}/dirmount-remove" "${ROOTDIR}/boot/efi"
fi

FOUND="$(mount | grep "${ROOTDIR}/boot")"
if [ "${FOUND}" != "" ]; then
  # boot
  "${SUPERPATH}/dirmount-remove" "${ROOTDIR}/boot" stack
fi

# root
"${SUPERPATH}/dirmount-remove" "${ROOTDIR}"
exitonerror ${?}

if [ "${CRYPTDEV}" != "" ]; then
  "${SUPERPATH}/device-closecrypt" "${CRYPTDEV}"
  exitonerror ${?}
fi

#-------------------------------------------------

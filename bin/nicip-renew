#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# nicip-renew * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Renew dynamic IP (DHCP) at network interface"
  info ""
  info "Usage:"
  info " ${SCRIPT} <nic>"
  info ""
  info "Example:"
  info " ${SCRIPT} eth0"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

NIC="${1}"

if [ "${NIC}" = "" ] || [ "${2}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

ensurecmd dhclient @ isc-dhcp-client dhcp-client dhclient
exitonerror ${?}

#-------------------------------------------------

action "Releasing dynamic IP address at ${NIC}"

dhclient "${NIC}" -r
exitonerror ${?} "Failed to release dynamic IP address at ${NIC}"

sleep 3

action "Obtaining dynamic IP address at ${NIC}"

dhclient "${NIC}"
exitonerror ${?} "Failed to obtain dynamic IP address at ${NIC}"

success "Renewed dynamic IP address at ${NIC}"

#-------------------------------------------------

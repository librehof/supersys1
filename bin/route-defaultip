#!/bin/sh

IMPACT="read/bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# route-defaultip * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Print host's default network IP address (IPv4)"
  info "or renew default IP address (dhcp)"
  info ""
  info "Usage:"
  info " ${SCRIPT} # print"
  info " ${SCRIPT} renew # then print"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

if [ "${1}" != "" ] && [ "${1}" != "renew" ]; then
  inputexit
fi

if [ "${2}" != "" ]; then
  inputexit
fi

if [ "${1}" != "" ]; then
  checksuperuser
  exitonerror ${?}
fi

ensurecmd ip @ iproute2

if [ ! -e "/sys/class/net" ]; then
  errorexit "Missing /sys/class/net"
fi

#-------------------------------------------------

if [ "${1}" = "renew" ]; then

  INFO=$(ip route get 1 | grep " src ")
  if [ "${INFO}" = "" ]; then
    errorexit "Could not find route (try route-list)"
  fi

  findforward "${INFO}" " src "
  HEAD="${found}"

  findbackward "${HEAD}" " "
  POSSIBLENIC="${found}"
  if [ "${POSSIBLENIC}" = "" ]; then
    errorexit "Could not find a default network interface"
  fi

  NIC="$(ls -1 "/sys/class/net" | grep "${POSSIBLENIC}" | head -1)"
  if [ "${NIC}" = "" ]; then
    errorexit "Could not find a valid default network interface: ${POSSIBLENIC}"
  fi

  "${SUPERPATH}/nicip-renew" "${NIC}"

fi

#-------------------------------------------------

INFO=$(ip route get 1 | grep " src ")
if [ "${INFO}" = "" ]; then
  errorexit "Could not find route (try route-list)"
fi

findforward "${INFO}" " src "

# IP
pos=$((pos+6))
end=$(stringlen "${INFO}")
TAIL="$(substring "${INFO}" ${pos} ${end})"
IP="$(spacefield "${TAIL}" 1)"
if [ "${IP}" = "" ]; then
  errorexit "Could not find a default host IP address"
fi

# check dots
NOTEQUAL="$(replaceinstring "." "" "${IP}")"
if [ "${IP}" = "${NOTEQUAL}" ]; then
  errorexit "Could not find a valid default IPv4 address: ${IP}"
fi

echo "${IP}"

#-------------------------------------------------

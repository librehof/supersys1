#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# dir-save * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Save content INSIDE directory (into compressed archive file)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <dir> [<file>.]7z [password[=<x>]] [\"<pattern>\" ...]"
  info " ${SCRIPT} <dir> [<file>.]zip|tgz|tlz|txz [\"<pattern>\" ...]"
  info ""
  info "Default <file> = <dir>"
  info "Usually mtime is rounded to seconds"
  info ""
  info "Patterns (max 9):"
  info " +pattern : include recursively (only zip/7z)"
  info " :pattern : include NON-recursively (top directory)"
  info " -pattern : exclude recursively"
  info ""
  info "Formats:"
  info " 7z  - uses LZMA (with optional AES-256 password encryption)"
  info " zip - most portable, uses DEFLATE compression"
  info " tgz - uses tar + gzip (DEFLATE)"
  info " tlz - uses tar + lzip (LZMA)"
  info " txz - uses tar + xz (LZMA2)"
  info ""
  info "Use tgz when linux compatibility is important"
  info ""
  info "Pattern examples:"
  info " +*.txt  :  include .txt files recursively"
  info " :*.txt  :  include top .txt files"
  info " -*.txt  :  exclude .txt files recursively"
  info ""
  info "See: dir-restore, dir-round"
  info ""
}

# TODO: support ignore/include file?

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DIR="${1}"
ZIP="${2}"

if [ "${DIR}" = "" ] || [ "${ZIP}" = "" ]; then
  inputexit
fi

. "${SUPERPATH}/supersys"

#-------------------------------------------------

# format

findbackward "${ZIP}" "."
EXT="${found}"

if [ "${EXT}" = "" ]; then
  EXT=${ZIP}
  ZIP="${DIR}.${EXT}"
fi

if [ "${EXT}" = "7z" ] || [ "${EXT}" = "zip" ]; then
  iscmd 7z
  if [ "${?}" != 0 ]; then
    ensurepkg p7zip
    exitonerror ${?}
    ensurepkg p7zip-full
    exitonerror ${?}
  fi
  Z="7z"
elif [ "${EXT}" = "tgz" ]; then
  ensurez "gz"
elif [ "${EXT}" = "txz" ]; then
  ensurez "xz"
elif [ "${EXT}" = "tlz" ]; then
  ensurez "lz"
else
  errorexit "Unsupported zip format: ${EXT}"
fi

#-------------------------------------------------

# add 7z/tar pattern to PARGS

parg()
{
  if [ "${1}" = "" ]; then
    return
  fi

  FILTER="filter"
  first="$(startstring "${1}" 1)"
  pattern="$(endstring "${1}" 2)"

  if [ "${Z}" = "7z" ]; then

    if [ "${first}" = "+" ]; then
      INCLUDE="include"
      PARGS="${PARGS} -ir!${pattern}"
    elif [ "${first}" = ":" ]; then
      INCLUDE="include"
      PARGS="${PARGS} -ir-!${pattern}"
    elif [ "${first}" = "-" ]; then
      PARGS="${PARGS} -xr!${pattern}"
    else
      errorexit "Invalid pattern: ${1} (use +pattern, :pattern or -pattern)"
    fi

  else

    if [ "${first}" = "+" ]; then
      errorexit "Pattern not supported when using ${EXT}: ${1}"
    elif [ "${first}" = ":" ]; then
      INCLUDE="include"
      PARGS="${PARGS} ${pattern}"
    elif [ "${first}" = "-" ]; then
      P=""
      PARGS=" --exclude=${pattern}${PARGS}"
    else
      errorexit "Invalid pattern: ${1} (use :pattern or -pattern)"
    fi

  fi
}

#-------------------------------------------------

# filter and password

FILTER=""
INCLUDE=""

PARGS=""

PWINPUT=""
PASSWORD=""

PWKEY="$(startstring "${3}" 9)"
if [ "${3}" = "password" ] || [ "${PWKEY}" = "password=" ]; then

  if [ "${EXT}" != "7z" ]; then
    errorexit "Password not supported using ${EXT} (try 7z)"
  fi

  PWINPUT="true"
  if [ "${PWKEY}" = "password=" ]; then
    PASSWORD="$(endstring "${3}" 10)"
    if [ "${PASSWORD}" = "" ]; then
      errorexit "Blank password"
    fi
    warning "Password on command line (history -c)"
  fi

  parg "${4}"
  parg "${5}"
  parg "${6}"
  parg "${7}"
  parg "${8}"
  parg "${9}"
  parg "${10}"
  parg "${11}"
  parg "${12}"

else

  parg "${3}"
  parg "${4}"
  parg "${5}"
  parg "${6}"
  parg "${7}"
  parg "${8}"
  parg "${9}"
  parg "${10}"
  parg "${11}"

fi

#-------------------------------------------------

if [ "${INCLUDE}" = "" ]; then
  if [ "${Z}" = "7z" ]; then
    PARGS=" -ir!*${PARGS}"
  else
    PARGS="${PARGS} ."
  fi
fi

#-------------------------------------------------

if [ ! -d "${DIR}" ]; then
  errorexit "Unknown directory: ${DIR}"
fi

DIR="$(abspath "${DIR}")"
exitonerror ${?}

if [ -e "${ZIP}" ]; then
  errorexit "Output already exists: ${ZIP}"
fi

ZIP="$(abspath "${ZIP}")"
exitonerror ${?}

if [ "${PWINPUT}" = "true" ]; then
  if [ "${PASSWORD}" = "" ]; then
    ZIPNAME="$(basename "${ZIP}")"
    secretinput "Password for ${ZIPNAME}:"
    PASSWORD="${SECRET}"
  fi
  if [ "${PASSWORD}" = "" ]; then
    errorexit "Blank password"
  fi
fi

#-------------------------------------------------

DIRNAME="$(basename "${DIR}")"

if [ "${FILTER}" = "" ]; then
  if [ "${PASSWORD}" = "" ]; then
    ZIPINFO="${DIRNAME} into ${ZIP}"
  else
    ZIPINFO="${DIRNAME} into AES encrypted ${ZIP}"
  fi
else
  if [ "${PASSWORD}" = "" ]; then
    ZIPINFO="${DIRNAME} (filtered) into ${ZIP}"
  else
    ZIPINFO="${DIRNAME} (filtered) into AES encrypted ${ZIP}"
  fi
fi

#-------------------------------------------------

action "Saving content of ${ZIPINFO}"

clearfile "${STDFILE}"

if [ "${EXT}" = "7z" ] || [ "${EXT}" = "zip" ]; then

  IFS=" "
  if [ "${PASSWORD}" = "" ]; then
    info "cd \"${DIRNAME}\"; 7z a \"${ZIP}\" -t${EXT} -mx=9${PARGS}"
    cd "${DIR}"
    7z a "${ZIP}" -t${EXT} -mx=9${PARGS} > /dev/null 2> "${STDFILE}"
    STATUS=${?}
  else
    info "cd \"${DIRNAME}\"; 7z a \"${ZIP}\" -t${EXT} -mx=9 -mhe=on -p******${PARGS}"
    cd "${DIR}"
    7z a "${ZIP}" -t${EXT} -mx=9 -mhe=on "-p${PASSWORD}"${PARGS} > /dev/null 2> "${STDFILE}"
    STATUS=${?}
  fi
  IFS="
"

elif [ "${EXT}" = "tgz" ]; then

  info "cd \"${DIR}\"; tar -cpz -f \"${ZIP}\"${PARGS}"
  cd "${DIR}"
  IFS=" "
  tar -czf "${ZIP}"${PARGS} > /dev/null 2> "${STDFILE}"
  STATUS=${?}
  IFS="
"

elif [ "${EXT}" = "txz" ]; then

  info "cd \"${DIR}\"; tar -cpJ -f \"${ZIP}\"${PARGS}"
  cd "${DIR}"
  IFS=" "
  tar -cJf "${ZIP}"${PARGS} > /dev/null 2> "${STDFILE}"
  STATUS=${?}
  IFS="
"

elif [ "${EXT}" = "tlz" ]; then

  info "cd \"${DIR}\"; tar -cp --lzip -f \"${ZIP}\"${PARGS}"
  cd "${DIR}"
  IFS=" "
  tar --lzip -cf "${ZIP}"${PARGS} > /dev/null 2> "${STDFILE}"
  STATUS=${?}
  IFS="
"

else

  errorexit "Unknown: ${EXT}"

fi

if [ -f "${ZIP}" ]; then
  inheritowner "${ZIP}"
fi

if [ ${STATUS} != 0 ]; then
  notetail "${STDFILE}" 3
  errorexit "Failed to save content of ${DIR} into ${ZIP}"
fi

#success "Saved content of ${ZIPINFO}"

#-------------------------------------------------

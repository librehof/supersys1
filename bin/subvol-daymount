#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# subvol-daymount * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Ensure all dayrotate snapshots are mounted"
  info ""
  info "Usage:"
  info " ${SCRIPT} <subdir> <historydir>"
  info ""
  info "Only needed for ZFS, silent exit if BTRFS"
  info ""
  info "See: subvol-dayrotate"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

SUBDIR="${1}"
HISTORYDIR="${2}"

if [ "${SUBDIR}" = "" ] || [ "${HISTORYDIR}" = "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

SUBDIR="$(truepath "${SUBDIR}")"
exitonerror ${?}

checkdir "${SUBDIR}"
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

# find subdir's mount point

MPOINT="${SUBDIR}"
COUNT=9
while :
do
  # check
  MOUNTED="$(mount | grep " on ${MPOINT} ")"
  if [ "${MOUNTED}" != "" ]; then
    break
  fi
  # next
  if [ "${MPOINT}" = "/" ]; then
    break
  fi
  MPOINT="$(dirname "${MPOINT}")"
  if [ "${MPOINT}" = "" ]; then
    break
  fi
  COUNT=$((COUNT-1))
  if [ "${COUNT}" = 0 ]; then
    break
  fi
done

if [ "${MOUNTED}" = "" ]; then
  errorexit "Could not find mount point for ${SUBDIR}"
fi

#-------------------------------------------------

# historydir (relative to parent-of-subdir)

PARENT="$(dirname "${SUBDIR}")"
exitonerror ${?} "Could not get parent directory of ${SUBDIR}"

cd "${PARENT}"
exitonerror ${?} "Could not change directory to ${PARENT}"
sleep 1

HISTORYDIR="$(truepath "${HISTORYDIR}")"
exitonerror ${?}

#-------------------------------------------------

# get type of mount point (btrfs or zfs)

FSTYPE=""

findforward "${MOUNTED}" " type btrfs"
if [ "${?}" = 0 ]; then
  # silent exit if btrfs
  exit
fi

findforward "${MOUNTED}" " type zfs"
if [ "${?}" = 0 ]; then
  ensurecmd zpool @ zfsutils-linux zfs
  exitonerror ${?}
  FSTYPE="zfs"
  if [ "${MPOINT}" != "${SUBDIR}" ]; then
    errorexit "ZFS: Expected subdir to be a mount-point (${SUBDIR})"
  fi
  if [ "${MAX}" != "" ]; then
    errorexit "No support for max when using zfs"
  fi
  ZFSPATH="$(spacefield "${MOUNTED}" 1)"
  if [ "${ZFSPATH}" = "" ]; then
    errorexit "ZFS: Unexpected mount output"
  fi
fi

if [ "${FSTYPE}" = "" ]; then
  note "${MOUNTED}"
  errorexit "Expected mount point to btrfs or zfs"
fi

#-------------------------------------------------

action "Mounting days and months inside ${HISTORYDIR} ..."

# will only run "one" at a time

scriptlock "subvolday" 30
exitonerror ${?}

#-------------------------------------------------

ensuredir "${HISTORYDIR}"
exitonerror ${?}

# ensure dayN and monthN mounts
LINES="$(zfs list -H -d 1 -t snapshot "${ZFSPATH}")"
exitonerror ${?} "Failed to list zfs snapshots for ${ZFSPATH}"
COUNT=0
for LINE in ${LINES}; do
  SNAPSHOT="$(tabfield ${LINE} 1)"
  LONGNAME="$(basename "${SNAPSHOT}")"
  findforward "${LONGNAME}" "@"
  exitonerror ${?} "Unexpected zfs list output"
  SHORTNAME="$(endstring "${LONGNAME}" $((pos+2)))"
  DAYPREFIX="$(startstring "${SHORTNAME}" 3)"
  MONTHPREFIX="$(startstring "${SHORTNAME}" 5)"
  if [ "${DAYPREFIX}" = "day" ] || [ "${MONTHPREFIX}" = "month" ]; then
    MOUNTED="$(mount | grep " on ${HISTORYDIR}/${SHORTNAME} ")"
    if [ "${MOUNTED}" = "" ]; then
      # mount missing day/month inside historydir
      ensuredir "${HISTORYDIR}/${SHORTNAME}"
      exitonerror ${?}
      mount -t zfs "${ZFSPATH}@${SHORTNAME}" "${HISTORYDIR}/${SHORTNAME}"
      exitonerror ${?} "Failed to mount zfs ${ZFSPATH}@${SHORTNAME} at ${HISTORYDIR}/${SHORTNAME}"
      COUNT=$((COUNT+1))
    fi
  fi
done

#------------------------------------------

# short info

if [ "${COUNT}" = 1 ]; then
  note "Mounted 1 snapshot"
elif [ "${COUNT}" -gt 1 ]; then
  note "Mounted ${COUNT} snapshots"
fi

#------------------------------------------

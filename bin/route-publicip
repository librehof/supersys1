#!/bin/sh

IMPACT="read"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# route-publicip * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Lookup and print host's public network IP address"
  info ""
  info "Usage:"
  info " ${SCRIPT} [info]"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

if [ "${2}" != "" ]; then
  inputexit
fi

METHOD="${1}"

if [ "${METHOD}" != "" ] && [ "${METHOD}" != "info" ]; then
  inputexit
fi

ensurecmd dig @ apt:dnsutils apt:knot-dnsutils dnf:bind-utils bind-tools

#-------------------------------------------------

if [ "${METHOD}" = "info" ]; then
  iscmd curl
  if [ "${?}" = 0 ]; then
    echo "-------------------------------------"
    echo "curl https://ipinfo.io/ip"
    echo "-------------------------------------"
    echo ""
    IP="$(curl --max-time 5 -s "https://ipinfo.io/ip")"
    echo ${IP}
    echo ""
  fi
  echo "-------------------------------------"
  echo "host myip.opendns.com"
  echo "-------------------------------------"
  echo ""
  host myip.opendns.com resolver1.opendns.com
  echo ""
  echo "-------------------------------------"
  echo "dig myip.opendns.com"
  echo "-------------------------------------"
  dig +time=5 +tries=1 myip.opendns.com ANY @resolver1.opendns.com
  echo "-------------------------------------"
  exit
fi

IP="$(dig +short +time=5 +tries=1 myip.opendns.com @resolver1.opendns.com)"
exitonerror ${?} "Failed to lookup host's public ip address"

if [ "${IP}" = "" ]; then
  # try another method
  ensurecmd curl @ curl
  IP="$(curl -s "https://ipinfo.io/ip")"
  exitonerror ${?} "Failed to lookup host's public ip address"
fi

echo ${IP}

#-------------------------------------------------

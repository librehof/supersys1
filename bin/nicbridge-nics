#!/bin/sh

IMPACT="read"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# nicbridge-nics * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "List bridged network interfaces in the system"
  info ""
  info "Usage:"
  info " ${SCRIPT} [<bridge>|info]"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

METHOD="${1}"

if [ "${2}" != "" ]; then
  inputexit
fi

#-------------------------------------------------

if [ "${METHOD}" != "info" ]; then

  LINES="$(bridge link)"
  exitonerror ${?} "Failed to list active bridges"

  LINES="$(printf '%s' "${LINES}" | grep " master ")"
  for LINE in ${LINES}; do
    nic="$(spacefield "${LINE}" 2)"
    nic="$(replaceinstring ":" "" "${nic}")"
    master="$(spacefield "${LINE}" 6)"
    if [ "${master}" != "master" ]; then
      errorexit "Unexepected output"
    fi
    bridge="$(spacefield "${LINE}" 7)"
    if [ "${bridge}" = "" ]; then
      errorexit "Unexepected output"
    fi
    if [ "${METHOD}" != "" ]; then
      if [ "${METHOD}" != "${bridge}" ]; then
        continue
      fi
      printf "${nic}\n"
    else
      printf "${nic}\t${bridge}\n"
    fi
  done
  exit ${?}

else

  # info
  bridge link
  exitonerror ${?} "Failed to list bridged network interfaces"

fi

#-------------------------------------------------


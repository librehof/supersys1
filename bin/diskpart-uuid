#!/bin/sh

IMPACT="read/write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# diskpart-uuid * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Print (or change) GPT partition uuid"
  info ""
  info "Print (lowercase):"
  info " ${SCRIPT} <diskdevice> <ordinal>"
  info ""
  info "Change:"
  info " ${SCRIPT} <diskdevice> <ordinal> <uuid>"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DISK="${1}"
ORDINAL="${2}"
UUID="${3}"

if [ "${DISK}" = "" ] || [ "${ORDINAL}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

if [ "${UUID}" = "" ]; then
  IMPACT="read"
else
  UUID="$(lowerstring "${UUID}")"
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

DISK="$(abspath "${DISK}")"
exitonerror ${?}

checkdevice "${DISK}"
exitonerror ${?}

TRUEDISK="$(truepath "${DISK}")"
exitonerror ${?}

PT="$("${SCRIPTPATH}/diskpt-type" "${DISK}")"
exitonerror ${?}

if [ "${PT}" != "mbr" ] && [ "${PT}" != "gpt" ]; then
  errorexit "Unknown partition table"
fi

if [ "${PT}" = "mbr" ]; then
  errorexit "Partition table (mbr) does not support uuid"
fi

#-------------------------------------------------

partdevice "${TRUEDISK}" ${ORDINAL}

checkdevice "${PARTDEVICE}"
exitonerror ${?}

#-------------------------------------------------

"${SUPERPATH}/sw-ensure" apk:sgdisk+ zypper:gptfdisk gdisk
exitonerror ${?}

if [ "${UUID}" = "" ]; then

  # print uuid

  GUIDLINE="$(sgdisk --info=${ORDINAL} "${DISK}" | grep "Partition unique GUID: ")"
  exitonerror ${?} "Failed to get uuid of ${DISK}:${ORDINAL}"

  UUID="$(substring "${GUIDLINE}" 24 59)"
  DASH="$(substring "${UUID}" 9 9)"
  LEN=$(stringlen "${UUID}")
  if [ ${LEN} != 36 ] || [ "${DASH}" != "-" ]; then
    exitonerror ${?} "Failed to read uuid from ${DISK}:${ORDINAL} (sgdisk)"
  fi

  UUID="$(lowerstring "${UUID}")"
  echo "${UUID}"

else

  # change uuid

  LEN=$(stringlen "${UUID}")
  if [ ${LEN} != 36 ]; then
    errorexit "Expected UUID (XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX), got ${UUID}"
  fi

  clearfile "${STDFILE}"
  sgdisk "--partition-guid=${ORDINAL}:${UUID}" "${DISK}" >> "${STDFILE}" 2>&1
  if [ ${?} != 0 ]; then
    notetail "${STDFILE}" 3
    errorexit "Failed to change uuid of ${DISK}:${ORDINAL} (gpt)"
  fi

  partscan "${TRUEDISK}"

  sync # disk commit

  success "Changed partition uuid at ${DISK}:${ORDINAL} to ${UUID}"

fi

#-------------------------------------------------

#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# format-btrfs * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Format device(s) with new btrfs volume"
  info ""
  info "Usage - no redundancy:"
  info " ${SCRIPT} <device>,... <label> [stripe|portable] [compress]"
  info ""
  info "Usage - data at two places, 2+ devices:"
  info " ${SCRIPT} <device>,... <label> mirror [compress]"
  info ""
  info "File system characteristics:"
  info "- Snapshot (CoW) and RAID capable"
  info "- SSD friendly from kernel 4.14+ (prolonging SSD life)"
  info "- Copy-on-write (CoW) with Checksumming (CRC)"
  info "- Disable CoW+CRC for databases and vm:s (see dir-nocow)"
  info "- Optional Compression (zlib is default)"
  info "- Make sure relatime is used (shall be default)"
  info "- Keep number of snapshots at a reasonable number"
  info "- Recursive (momentary) snapshots not supported"
  info "- For high availability, use mirror with at least 3 devices"
  info "- https://btrfs.wiki.kernel.org/index.php/FAQ"
  info ""
  info "Example - single (stripe at voldevice-add):"
  info " ${SCRIPT} /dev/loop0 somedata"
  info ""
  info "Example - stripe (spanning two devices for speed):"
  info " ${SCRIPT} /dev/loop0,/dev/loop1 s2 stripe"
  info ""
  info "Example - mirror for redundancy with 3 devices:"
  info " ${SCRIPT} /dev/loop0,/dev/loop1,/dev/loop2 m3 mirror"
  info ""
  info "Example - linux portable (be careful with user ids):"
  info " ${SCRIPT} /dev/sdm1 someusb portable"
  info ""
  info "See: subvol-create, voldevice-add, dir-nocow"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICES="${1}"
LABEL="${2}"

if [ "${DEVICES}" = "" ] || [ "${LABEL}" = "" ]; then
  inputexit
fi

if [ "${3}" = "compress" ]; then
  TYPE="stripe"
  OPT="${3}"
else
  TYPE="${3}"
  OPT="${4}"
fi

if [ "${TYPE}" = "" ]; then
  TYPE="stripe"
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

checkvolumelabel "${LABEL}"
exitonerror ${?}

DEVICELIST="$(replaceinstring "," " " "${DEVICES}")"

#-------------------------------------------------

ensurecmd btrfs @ zypper:btrfsprogs btrfs-progs btrfs-tools
exitonerror ${?}

#-------------------------------------------------

# check/unmount devices

SMALLM=""

IFS=","
for DEVICE in ${DEVICES}
do
  DEVICE="$(truepath "${DEVICE}")"
  exitonerror ${?}

  checkdevice "${DEVICE}"
  exitonerror ${?}

  SPACEM=$("${SUPERPATH}/device-space" "${DEVICE}" mib)
  exitonerror ${?}

  if [ ${SPACEM} -lt 8 ]; then
    errorexit "Expected ${DEVICE} (${SPACEM} MiB) to be at least 8 MiB"
  fi

  if [ ${SPACEM} -lt 256 ]; then
    warning "Device ${DEVICE} is smaller then 256 MiB (${SPACEM} MiB)"
    keywait 3
  fi

  if [ ${SPACEM} -le 8192 ]; then
    SMALLM=${SPACEM}
  fi

  unmountall "${DEVICE}"
  exitonerror ${?}
done
IFS="
"

#-------------------------------------------------

action "Formatting ${DEVICES} (${LABEL})"

FLAGS=""

addflags()
{
  if [ "${FLAGS}" = "" ]; then
    FLAGS="${1}"
  else
    FLAGS="${FLAGS} ${1}"
  fi
}

if [ "${SMALLM}" != "" ]; then
  note "Using mixed format because of small device (${SMALLM} MiB)"
  addflags "--mixed -O ^extref -O ^skinny-metadata"
  sleep 1
elif [ "${TYPE}" = "portable" ]; then
  addflags "-O ^extref -O ^skinny-metadata"
fi

if [ "${TYPE}" = "portable" ]; then
  COUNT="single"
elif [ "${TYPE}" = "stripe" ]; then
  COUNT="any"
  if [ "${DEVICES}" != "${DEVICELIST}" ]; then
    # multiple
    addflags "-m raid0 -d raid0"
  fi
elif [ "${TYPE}" = "mirror" ]; then
  COUNT="multiple"
  addflags "-m raid1 -d raid1"
else
  errorexit "Unsupported type: ${TYPE}"
fi

TYPEINFO="${TYPE} btrfs"

#-------------------------------------------------

# check

DEVICECOUNT="single"
if [ "${DEVICES}" = "${DEVICELIST}" ]; then
  # got single
  if [ "${COUNT}" = "multiple" ]; then
    errorexit "Expected multiple devices: ${DEVICES}"
  fi
else
  # got multiple
  if [ "${COUNT}" = "single" ]; then
    errorexit "Expected single device: ${DEVICES}"
  fi
fi

#-------------------------------------------------

# format

clearfile "${RUNPATH}/format.log"
exitonerror ${?}

FIRSTDEVICE=""

IFS=","
for DEVICE in ${DEVICES}
do
  note "Wiping header: ${DEVICE}"
  wipefs -f -a "${DEVICE}" >> "${RUNPATH}/format.log"
  exitonerror ${?} "Failed to wipe ${DEVICE}"
  if [ "${FIRSTDEVICE}" = "" ]; then
    FIRSTDEVICE="${DEVICE}"
  fi
done
IFS="
"

if [ "${FLAGS}" != "" ]; then
  note "flags: ${FLAGS}"
fi

IFS=" "
mkfs.btrfs -L "${LABEL}" ${FLAGS} ${DEVICELIST} >> "${RUNPATH}/format.log"
exitonerror ${?} "Failed to format ${DEVICES} with ${TYPEINFO}"
IFS="
"

#-------------------------------------------------

# enable compression

if [ "${OPT}" = "compress" ]; then

  action "Enabling compression"

  superlock
  exitonerror ${?}

  mkdir "${RUNMOUNT}"
  mount -t btrfs "${FIRSTDEVICE}" "${RUNMOUNT}"
  exitonerror ${?} "Failed to mount ${FIRSTDEVICE}"

  chattr +c "${RUNMOUNT}"
  exitonerror ${?}  "Failed to enable compression on ${DEVICES}"

  success "Enabled compression"

fi

#-------------------------------------------------

sync # disk commit

DEVICES="$(replaceinstring "/dev/" "" "${DEVICES}")"
success "Formatted ${DEVICES} (${LABEL}) with ${TYPEINFO}"

#-------------------------------------------------

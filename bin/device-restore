#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# device-restore * GPLv3 (C) 2024 librehof.org

summary()
{
  info ""
  info "Restore device from file"
  info ""
  info "Usage:"
  info " ${SCRIPT} <targetdevice> <file>"
  info ""
  info "file:"
  info " <name>.<space>mib.<voltype>.<z> ==> partclone"
  info " <name>.<space>mib.<z> ==> expand raw"
  info " <name>.<z> ==> expand raw (no space check)"
  info " <name> ==> raw restore"
  info ""
  info "See: device-save"
  info "Supported compression (z): see file-compress"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

TARGET="${1}"
FILE="${2}"

if [ "${TARGET}" = "" ] || [ "${FILE}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

TARGET="$(truepath "${TARGET}")"
exitonerror ${?}

checkdevice "${TARGET}"
exitonerror ${?}

FILE="$(abspath "${FILE}")"
exitonerror ${?}

checkfile "${FILE}"
exitonerror ${?}

#-------------------------------------------------

# input: "<space>mib"
# output: SM = <space in MiB> | ""

spacemib()
{
  SM=""
  len=$(stringlen "${1}")
  if [ ${len} -gt 3 ]; then
    sep=$((len-2))
    right="$(endstring "${1}" ${sep})"
    if [ "${right}" = "mib" ]; then
      sep=$((sep-1))
      left="$(startstring "${1}" ${sep})"
      isint "${left}"
      if [ ${?} = 0 ]; then
        SM=${left} # space in MiB
      fi
    fi
  fi
}

#-------------------------------------------------

EXT="" # file extension or empty
Z=""   # compression command or empty
FS=""  # if z: file system (voltype) or empty
SM=""  # if z: space in MiB or empty

findbackward "${FILE}" "."
EXT="${found}"
END=$((pos-2))

if [ "${EXT}" = "lz" ] || [ "${EXT}" = "gz" ] || [ "${EXT}" = "xz" ] || [ "${EXT}" = "bz2" ]; then

  ensurez "${EXT}" # Z=<command>

  findbackward "${FILE}" "." ${END}
  FIELD="${found}"
  END=$((pos-2))
  if [ "${FIELD}" != "" ]; then
    spacemib "${FIELD}" # second field
    if [ "${SM}" = "" ]; then
      FS="${FIELD}"
      findbackward "${FILE}" "." ${END}
      FIELD="${found}"
      if [ "${FIELD}" != "" ]; then
        spacemib "${FIELD}" # third field
      fi
    fi
  fi

fi

if [ "${SM}" = "" ]; then
  FS=""
fi

if [ "${EXT}" = "zip" ]; then
  warning "Will not expand zip file, press CTRL-C to abort"
  keywait 5
fi

if [ "${EXT}" = "7z" ]; then
  warning "Will not expand 7z file, press CTRL-C to abort"
  keywait 5
fi

#-------------------------------------------------

# check

TARGETB="$("${SUPERPATH}/device-space" "${TARGET}")"
exitonerror ${?}

TARGETINFO=$(sizeinfo ${TARGETB})

checkblocksize ${TARGETB} 128 "${TARGET}"
exitonerror ${?}

checkblocksize ${TARGETB} 2048 "${TARGET}" warn

TARGETM=$(( TARGETB / MIB ))

SOURCEB="$(filesize "${FILE}")"
exitonerror ${?}

SOURCEINFO=$(sizeinfo ${SOURCEB})

FILEINFO="$(basename "${FILE}")"

if [ "${Z}" = "" ]; then

  # ** raw **

  checkblocksize ${SOURCEB} 128 "${FILE}"
  exitonerror ${?}

  checkblocksize ${SOURCEB} 2048 "${FILE}" warn

  if [ ${SOURCEB} -gt ${TARGETB} ]; then
    errorexit "Source ${FILE} (${SOURCEINFO}) is larger then ${TARGET} (${TARGETINFO})"
  fi

  if [ ${SOURCEB} != ${TARGETB} ]; then
    note "Source ${FILE} (${SOURCEINFO}) is smaller then ${TARGET} (${TARGETINFO})"
    keywait 3
  fi

  FILEINFO="${FILEINFO} (${SOURCEINFO})"

elif [ "${SM}" != "" ]; then

  # ** compressed with space info **

  if [ ${SM} -gt ${TARGETM} ]; then
    errorexit "Source ${FILE} (${SM} MiB) is larger then ${TARGET} (${TARGETM} MiB)"
  fi

  if [ ${SM} != ${TARGETM} ]; then
    note "Source ${FILE} (${SM} MiB) is smaller then ${TARGET} (${TARGETM} MiB)"
    keywait 3
  fi

  if [ "${FS}" = "" ]; then
    FILEINFO="${FILEINFO} (${SM} MiB uncompressed)"
  else
    FILEINFO="${FILEINFO} (${SM} MiB unpartcloned)"
  fi

fi

#-------------------------------------------------

if [ "${FS}" != "" ]; then
  "${SUPERPATH}/sw-ensure" partclone
  exitonerror ${?}
  FOUND="$(which partclone.${FS} 2> /dev/null)"
  if [ "${FOUND}" = "" ]; then
    errorexit "No support for ${FS} volume (missing partclone.${FS})"
  fi
fi

#-------------------------------------------------

# caution

info "Will restore ${TARGET} (${TARGETINFO}) using ${FILEINFO}"

if [ "${Z}" != "" ] && [ "${SM}" = "" ]; then
  clearfile "${STDFILE}"
  ${Z} -l "${FILE}" >> "${STDFILE}" 2>> "${STDFILE}"
  notetail "${STDFILE}"
  note "Press CTRL-C to review size, will not be able to auto-verify"
  keywait 5
fi

info "Quickly press CTRL-C to abort!"
keywait 5

# remove any mount-point associated with target device

unmountall "${TARGET}"
exitonerror ${?}

#-------------------------------------------------

# raw

if [ "${Z}" = "" ]; then

  action "Restoring ${TARGET} (${TARGETINFO}) using ${FILEINFO} [raw]"

  # bulk (MiB-blocks)
  SOURCEM=$(( SOURCEB / MIB ))
  if [ ${SOURCEM} != 0 ]; then
    PROGRESS="$(dd --help 2>&1 | grep progress)"
    if [ "${PROGRESS}" != "" ]; then
      dd "if=${FILE}" "of=${TARGET}" bs=${MIB} count=${SOURCEM} status=progress
    else
      dd "if=${FILE}" "of=${TARGET}" bs=${MIB} count=${SOURCEM}
    fi
    exitonerror ${?} "Failed to restore ${TARGET} (${TARGETINFO}) from ${FILE} (${SOURCEINFO})"
  fi

  # tail (128B-blocks)
  STARTB=$(( SOURCEM * MIB ))
  TAILB=$(( SOURCEB - STARTB ))
  if [ ${TAILB} != 0 ]; then
    START128=$(( STARTB / 128 ))
    COUNT128=$(( TAILB / 128 ))
    info "final ${TAILB} bytes ..."
    dd "if=${FILE}" skip=${START128} "of=${TARGET}" seek=${START128} bs=128 count=${COUNT128} > /dev/null 2>&1
    exitonerror ${?} "Failed to restore ${TARGET} (${TARGETINFO}) using ${FILE} (${SOURCEINFO})"
  fi

  success "Restored ${TARGET} (${TARGETINFO}) from ${FILE} (${SOURCEINFO})"

fi

#-------------------------------------------------

# raw z

if [ "${Z}" != "" ] && [ "${FS}" = "" ]; then

  action "Restoring ${TARGET} (${TARGETINFO}) using ${FILEINFO} [rawz]"

  ${Z} -d -c "${FILE}" > "${TARGET}"
  exitonerror ${?} "Failed to restore ${TARGET} (${TARGETINFO}) using ${FILE}"

  success "Restored ${TARGET} (${TARGETINFO}) from ${FILE}"

fi

#-------------------------------------------------

# fs z

if [ "${Z}" != "" ] && [ "${FS}" != "" ]; then

  action "Restoring ${TARGET} (${TARGETINFO}) using ${FILEINFO} [fsz]"

  ${Z} -d -c "${FILE}" | partclone.${FS} -r -o ${TARGET}
  exitonerror ${?} "Failed to restore ${TARGET} (${TARGETINFO}) using ${FILE} (partclone)"

  success "Restored ${TARGET} (${TARGETINFO}) from ${FILE} (partclone)"

fi

#-------------------------------------------------

partdisk "${TARGET}"
partscan "${DISKDEVICE}"

#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# dirmount-vbox * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Mount vbox host share: mountdir = hostdir"
  info ""
  info "Usage:"
  info " ${SCRIPT} <mountdir> <label> [<options>]"
  info ""
  info "Creates <mountdir> if missing*"
  info "(*) permissions inherited from parent directory"
  info ""
  info "Default options:"
  info " uid=<mountdir-uid>,gid=<mountdir-gid>"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

MPOINT="${1}"
LABEL="${2}"
OPTIONS="${3}"

if [ "${MPOINT}" = "" ] || [ "${LABEL}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

MPOINT="$(truepath "${MPOINT}")"
exitonerror ${?}

#-------------------------------------------------

# already mounted?
MOUNTED="$(mount | grep "${LABEL} on " | grep " on ${MPOINT} ")"
if [ "${MOUNTED}" != "" ]; then
  # silent exit
  exit
fi

# mount-point check
MOUNTED="$(mount | grep " on ${MPOINT} ")"
if [ "${MOUNTED}" != "" ]; then
  echo "${MOUNTED}"
  errorexit "Underlying ${MPOINT} points to other mount"
fi

#-------------------------------------------------

# "label=" "value"

setdefault()
{
  FOUND="$(echo "${OPTIONS}" | grep "${1}")"
  if [ "${FOUND}" = "" ]; then
    if [ "${OPTIONS}" = "" ]; then
      OPTIONS="${1}${2}"
    else
      OPTIONS="${OPTIONS},${1}${2}"
    fi
  fi
}

#-------------------------------------------------

if [ ! -d "${MPOINT}" ]; then

  ensuredir "${MPOINT}"
  exitonerror ${?}

else

  # exists
  CONTENT=$(ls -A -1 "${MPOINT}/")
  if [ "${CONTENT}" != "" ]; then
    errorexit "Underlying ${MPOINT} is not empty"
  fi

fi

#-------------------------------------------------

action "Mounting vbox (${LABEL})"

U="$(stat -c %U "${MPOINT}")"
UI="$(stat -c %u "${MPOINT}")"
GI="$(stat -c %g "${MPOINT}")"
exitonerror ${?} "Failed to stat ${MPOINT}"

setdefault "uid=" "${UI}"
setdefault "gid=" "${GI}"

info "Options: ${OPTIONS}"
mount -t vboxsf "${LABEL}" "${MPOINT}" -o "${OPTIONS}"
STATUS=${?}

if [ ${STATUS} != 0 ]; then
  rmdir "${MPOINT}" # only removed if empty
  error "Failed to mount ${LABEL} at ${MPOINT} (exit ${STATUS})"
  exit ${STATUS}
fi

success "Mounted ${LABEL} at ${MPOINT} (vbox)"

#-------------------------------------------------

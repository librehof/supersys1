#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# dir-nocow * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Disable copy-on-write on complete directory (btrfs)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <btrfs-directory>"
  info ""
  info "Use for (almost a requirement when using btrfs):"
  info "- Database directories"
  info "- Virtual-machine directories"
  info ""
  info "Requires EXCLUSIVE access to directory (!)"
  info "==> directory must not be used during operation"
  info ""
  info "Checksumming will be disabled for directory (!)"
  info "==> trades low-level integrity for random-write speed"
  info ""
  info "Will use DOUBLE the space during operation"
  info ""
  info "See also: format-btrfs, dir-cow"
  info ""
}

# TODO: make sure to uncompress too?
# TODO: use defrag instead? (inplace operation?)
# TODO: dir-compress/dir-uncompress

#-------------------------------------------------

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DIR="${1}"

if [ "${DIR}" = "" ]; then
  inputexit
fi

DIR="$(abspath "${DIR}")"
exitonerror ${?}

checkdir "${DIR}"
exitonerror ${?}

#-------------------------------------------------

info "Will disable copy-on-write at ${DIR}"
info "Directory MUST NOT be in use, quickly press CTRL-C to abort!"
keywait 2

action "Disabling copy-on-write on ${DIR}"

mv "${DIR}" "${DIR}.original"
exitonerror ${?} "Failed to temporary move ${DIR}"

mkdir "${DIR}"
exitonerror ${?} "Failed to create new ${DIR}"

chattr +C "${DIR}"
exitonerror ${?} "Failed to set no-cow attribute (+C) on ${DIR}"

cp -ax "${DIR}.original/." "${DIR}/" # -x = do not follow mounts
exitonerror ${?} "Failed to restore ${DIR}"

rm -rf ${DIR}.original
exitonerror ${?} "Failed to delete ${DIR}.original"

success "Disabled copy-on-write at ${DIR}"

#-------------------------------------------------

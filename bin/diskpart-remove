#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# diskpart-remove * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Remove disk partition"
  info ""
  info "Usage:"
  info " ${SCRIPT} <diskdevice> <ordinal>"
  info ""
  info "For an extended partition, EBR will be cleared"
  info "For all other types, partition content will not be touched"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DISK="${1}"
ORDINAL="${2}"

if [ "${DISK}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

DISK="$(abspath "${DISK}")"
exitonerror ${?}

checkdevice "${DISK}"
exitonerror ${?}

TRUEDISK="$(truepath "${DISK}")"
exitonerror ${?}

#-------------------------------------------------

partdevice "${TRUEDISK}" ${ORDINAL}

checkdevice "${PARTDEVICE}"
exitonerror ${?}

#-------------------------------------------------

unmountall "${PARTDEVICE}"
exitonerror ${?}

#-------------------------------------------------

action "Removing partition"

parted "${DISK}" --script rm "${ORDINAL}"
exitonerror ${?} "Failed to remove partition ${PARTDEVICE}"

partscan "${TRUEDISK}"

if [ -b "${PARTDEVICE}" ]; then
  sleep 1
fi

if [ -b "${PARTDEVICE}" ]; then
  sleep 1
fi

if [ -b "${PARTDEVICE}" ]; then
  errorexit "Partition device still visible: ${PARTDEVICE}"
fi

sync # disk commit

success "Removed partition ${DISK}:${ORDINAL}"

#-------------------------------------------------

#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# voldevice-resize * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Resize volume on a specific device"
  info ""
  info "Usage:"
  info " ${SCRIPT} <device> <space>[M]|max"
  info ""
  info "Support:"
  info " ntfs  # unmount before any resizing"
  info " ext   # unmount before shrinking"
  info " btrfs # can be kept mounted during resize"
  info ""
  info "BTRFS: optionally use vol-balance afterwards"
  info ""
  info "See: device-space, vol-info"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICE="${1}"
SPACE="${2}"

if [ "${DEVICE}" = "" ] || [ "${SPACE}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

superlock
exitonerror ${?}

. "${SUPERPATH}/supersys"

DEVICE="$(truepath "${DEVICE}")"
exitonerror ${?}

checkdevice "${DEVICE}"
exitonerror ${?}

if [ "${SPACE}" = "max" ]; then
  BYTES="$("${SUPERPATH}/device-space" "${DEVICE}")"
  exitonerror ${?}
else
  BYTES=$(bytesize "${SPACE}")
  exitonerror ${?}
fi

MIBS=$(( BYTES / MIB ))

checkblocksize ${BYTES} 2048 "${DEVICE}"
exitonerror ${?}

SPACEINFO="$(sizeinfo ${BYTES})"

VTYPE="$(volumetype "${DEVICE}")"

#-------------------------------------------------

action "Resizing ${VTYPE} volume at ${DEVICE} (${SPACEINFO})"

clearfile "${RUNPATH}/voldevice.log"

if [ "${VTYPE}" = "ntfs" ]; then

  ensurecmd ntfs-3g @ ntfs-3g
  exitonerror ${?}

  echo "y" | ntfsresize -f --size ${BYTES} "${DEVICE}" >> "${RUNPATH}/voldevice.log" 2>&1
  if [ ${?} != 0 ]; then
    cat "${RUNPATH}/voldevice.log"
    errorexit "Failed to resize ${VTYPE} volume at ${DEVICE} (to ${SPACEINFO})"
  fi

elif [ "${VTYPE}" = "ext2" ] || [ "${VTYPE}" = "ext3" ] || [ "${VTYPE}" = "ext4" ]; then

  BLK512=$(( BYTES / 512 ))
  echo "y" | resize2fs "${DEVICE}" ${BLK512}s >> "${RUNPATH}/voldevice.log" 2>&1
  if [ ${?} != 0 ]; then
    cat "${RUNPATH}/voldevice.log"
    errorexit "Failed to resize ${VTYPE} volume at ${DEVICE} (to ${SPACEINFO})"
  fi

elif [ "${VTYPE}" = "btrfs" ]; then

  mkdir "${RUNMOUNT}"
  mount -t btrfs "${DEVICE}" "${RUNMOUNT}"
  exitonerror ${?} "Failed to mount ${DEVICE}"

  FSINFO="$(btrfs filesystem show --mbytes "${DEVICE}" | grep "devid" | grep "${DEVICE}")"

  DEVID="$(echo "${FSINFO}" | awk '{print $2;}')"
  checkint "${DEVID}" "DEVID"
  exitonerror ${?}

  note "Found btrfs device ${DEVICE} with devid ${DEVID}"

  if [ "${MIBS}" -lt 256 ]; then
    warning "Device is smaller then 256 MiB and may not be supported (Invalid argument)"
    sleep 2
  fi

  BLK1K=$(( BYTES / 1024 ))
  btrfs filesystem resize ${DEVID}:${BLK1K}k "${RUNMOUNT}" >> "${RUNPATH}/voldevice.log" 2>&1
  if [ ${?} != 0 ]; then
    cat "${RUNPATH}/voldevice.log"
    errorexit "Failed to resize ${VTYPE} volume at ${DEVICE} with devid ${DEVID} (to ${SPACEINFO})"
  fi

else

  errorexit "Unsupported type at ${DEVICE} (${VTYPE})"

fi

sync # disk commit
success "Resized ${VTYPE} at ${DEVICE} to ${SPACEINFO}"

#-------------------------------------------------

#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# format-ext2 * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Format device with ext2 file system (no journal)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <device> <label>"
  info ""
  info "Example:"
  info " ${SCRIPT} /dev/loop0p1 somename"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICE="${1}"
LABEL="${2}"

if [ "${DEVICE}" = "" ] || [ "${LABEL}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

checkvolumelabel "${LABEL}"
exitonerror ${?}

DEVICE="$(truepath "${DEVICE}")"
exitonerror ${?}

checkdevice "${DEVICE}"
exitonerror ${?}

#-------------------------------------------------

unmountall "${DEVICE}"
exitonerror ${?}

#-------------------------------------------------

action "Formatting ${DEVICE}"

clearfile "${RUNPATH}/format.log"

wipefs -f -a "${DEVICE}" >> "${RUNPATH}/format.log"
exitonerror ${?} "Failed to wipe ${DEVICE}"

mkfs.ext2 -L "${LABEL}" "${DEVICE}" >> "${RUNPATH}/format.log" 2>&1
exitonerror ${?} "Failed to format ${DEVICE} with ext2"

sync # disk commit

success "Formatted ${DEVICE} (${LABEL}) with ext2"

#-------------------------------------------------

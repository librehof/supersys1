#!/bin/sh

IMPACT="read"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# diskpart-list * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "List disk partitions (devices or metrics)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <diskdevice> [<ordinal>] [info|table]"
  info ""
  info "standard line:"
  info " /dev/<device>"
  info ""
  info "info line:"
  info " <unspecified partition info rounded to MiB>"
  info ""
  info "table line in bytes (tab separated):"
  info " <n> <start> <space> <entry> <align> <device> <label>"
  info ""
  info " entry:  primary|extended|logical"
  info " align:  misaligned|aligned (MiB-aligned)"
  info " label:  partition label (blank if mbr)"
  info ""
  info "Info example (skip all but partition 1):"
  info " ${SCRIPT} /dev/loop0 1 info"
  info ""
  info "Scripted table example:"
  info " . superlib"
  info " LIST=\$(${SCRIPT} /dev/loop0 table)"
  info " exitonerror \${?}"
  info " for LINE in \${LIST}; do"
  info '   ORDINAL=$(tabfield "${LINE}" 1)'
  info '   START=$(tabfield "${LINE}" 2)'
  info '   echo "${ORDINAL}: ${START}"'
  info " done"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DISK="${1}"
if [ "${2}" = "table" ] || [ "${2}" = "info" ]; then
  FILTER=""
  MODE="${2}"
else
  FILTER="${2}"
  MODE="${3}"
fi

if [ "${DISK}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

if [ "${MODE}" != "" ] && [ "${MODE}" != "table" ] && [ "${MODE}" != "info" ]; then
  inputexit
fi

if [ "${FILTER}" != "" ]; then
  checkint ${FILTER} "ORDINAL"
  exitonerror ${?}
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

DISK="$(truepath "${DISK}")"
exitonerror ${?}

checkdevice "${DISK}"
exitonerror ${?}

#-------------------------------------------------

if [ "${MODE}" = "" ]; then

  LEN="$(stringlen "${DISK}")"
  DEVICES="$(ls -1 ${DISK}*)"

  for DEVICE in ${DEVICES}
  do
    partdisk "${DEVICE}"
    if [ "${DISKDEVICE}" = "${DISK}" ] && [ "${ORDINAL}" != 0 ]; then
      echo "${DEVICE}"
    fi
  done

  exit 0

fi

#-------------------------------------------------

extractbytes()
{
  BYTES=$(echo "${1}" | awk -FB '{print $1}')
  checkint "${BYTES}" "field${1}"
  echo "${BYTES}"
}

#-------------------------------------------------

PT="$("${SCRIPTPATH}/diskpt-type" "${DISK}")"
exitonerror ${?}

if [ "${PT}" = "unknown" ]; then
  errorexit "Unknown partition table on ${DISK}"
fi

if [ "${MODE}" = "info" ]; then

  PRINT=$(parted ${DISK} --script \
    unit MIB \
    print)

else

  PRINT=$(parted ${DISK} --script \
    unit B \
    print)

fi

exitonerror ${?} "Could not list partitions on ${DISK}"

SECTION="header"

for LINE in ${PRINT}
do

  FIELD1="$(echo "${LINE}" | awk '{print $1;}')"
  FIELD2="$(echo "${LINE}" | awk '{print $2;}')"
  FIELD3="$(echo "${LINE}" | awk '{print $3;}')"
  FIELD4="$(echo "${LINE}" | awk '{print $4;}')"

  if [ "${SECTION}" = "header" ]; then
    if [ "${FIELD1}" = "Number" ] && [ "${FIELD2}" = "Start" ] && [ "${FIELD3}" = "End" ] && [ "${FIELD4}" = "Size" ]; then
      SECTION="data"
    fi
  elif [ "${SECTION}" = "data" ]; then
    # data
    if [ "${FILTER}" = "" ] || [ "${FILTER}" = "${FIELD1}" ]; then
      if [ "${MODE}" = "info" ]; then
        echo "${LINE}"
      else
        # table
        if [ "${FIELD1}" != "" ] && [ "${FIELD2}" != "" ] && [ "${FIELD3}" != "" ] && [ "${FIELD4}" != "" ]; then
          FIELD2=$(extractbytes "${FIELD2}")
          exitonerror ${?}
          FIELD3=$(extractbytes "${FIELD3}")
          exitonerror ${?}
          FIELD4=$(extractbytes "${FIELD4}")
          exitonerror ${?}
          MIB2=$((FIELD2/MIB))
          ROUND2=$((MIB2*MIB))
          ALIGNMENT="aligned"
          if [ "${FIELD2}" != "${ROUND2}" ]; then
            ALIGNMENT="misaligned"
          fi
          if [ "${PT}" = "gpt" ]; then
            ENTRY="primary"
          else
            ENTRY=""
            FOUND="$(echo "${LINE}" | grep " primary")"
            if [ "${FOUND}" != "" ]; then
              ENTRY="primary"
            fi
            FOUND="$(echo "${LINE}" | grep " extended")"
            if [ "${FOUND}" != "" ]; then
              ENTRY="extended"
            fi
            FOUND="$(echo "${LINE}" | grep " logical")"
            if [ "${FOUND}" != "" ]; then
              ENTRY="logical"
            fi
            if [ "${ENTRY}" = "" ]; then
              info "${LINE}"
              errorexit "Unknown partition entry found"
            fi
          fi
          LABEL=""
          if [ "${PT}" = "gpt" ]; then
            LABEL="$("${SUPERPATH}/diskpart-label" "${DISK}" "${FIELD1}")"
            exitonerror ${?}
          fi
          partdevice "${DISK}" ${FIELD1}
          printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "${FIELD1}" "${FIELD2}" "${FIELD4}" "${ENTRY}" "${ALIGNMENT}" "${PARTDEVICE}" "${LABEL}"
        fi
      fi
    fi
  fi

done

#-------------------------------------------------

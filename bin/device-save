#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# device-save * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Save device to file"
  info ""
  info "Usage:"
  info " ${SCRIPT} <device> <path/name> raw|sparse|<z>|volume.<z>"
  info ""
  info " raw:"
  info "  full copy (<name>)"
  info ""
  info " sparse:"
  info "  sparse file copy (<name>)"
  info "  will use partclone to skip unused blocks"
  info "  device must contain a supported file system"
  info "  will allocate a loop device during operation"
  info ""
  info " gz|lz|xz|bz2:"
  info "  compressed full copy (<name>.<space>mib.<z>)"
  info ""
  info " volume.gz|volume.lz|volume.xz|volume.bz2:"
  info "  partclone copy (<name>.<space>mib.<voltype>.<z>)"
  info "  device must contain a supported file system"
  info "  restore may require matching partclone version!"
  info ""
  info "Will fail if source is mounted"
  info ""
  info "Owner of file will be derived from directory"
  info "Will print complete file path on success"
  info ""
  info "See:"
  info " device-save, file-compress"
  info " https://en.wikipedia.org/wiki/Partclone"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

SOURCE="${1}"
NAMEPATH="${2}"
METHOD="${3}"

if [ "${SOURCE}" = "" ] || [ "${NAMEPATH}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

NAMEPATH="$(abspath "${NAMEPATH}")"
exitonerror ${?}

SOURCE="$(truepath "${SOURCE}")"
exitonerror ${?}

checkdevice "${SOURCE}"
exitonerror ${?}

#-------------------------------------------------

SOURCEB="$("${SUPERPATH}/device-space" "${SOURCE}")"
exitonerror ${?}

SOURCEINFO=$(sizeinfo ${SOURCEB})

checkblocksize ${SOURCEB} 2048 "${SOURCE}"
exitonerror ${?}

SOURCEM=$(( SOURCEB / MIB ))

#-------------------------------------------------

# file

if [ "${METHOD}" = "raw" ] || [ "${METHOD}" = "sparse" ]; then

  FILE="${NAMEPATH}"
  EXT=""

elif [ "${METHOD}" = "lz" ] || [ "${METHOD}" = "gz" ] || [ "${METHOD}" = "xz" ] || [ "${METHOD}" = "bz2" ]; then

  FILE="${NAMEPATH}.${SOURCEM}mib.${METHOD}"
  EXT="${METHOD}"

elif [ "${METHOD}" = "volume.lz" ] || [ "${METHOD}" = "volume.gz" ] || [ "${METHOD}" = "volume.xz" ] || [ "${METHOD}" = "volume.bz2" ]; then

  VOLTYPE="$("${SCRIPTPATH}/vol-type" "${SOURCE}")"
  exitonerror ${?}

  if [ "${VOLTYPE}" = "" ] || [ "${VOLTYPE}" = "unknown" ]; then
    errorexit "Unknown volume on ${SOURCE}"
  fi

  "${SUPERPATH}/sw-ensure" partclone
  exitonerror ${?}
  
  FOUND="$(which partclone.${VOLTYPE} 2> /dev/null)"
  if [ "${FOUND}" = "" ]; then
    errorexit "No support for ${VOLTYPE} (missing partclone.${VOLTYPE}, try raw)"
  fi  

  findbackward "${METHOD}" "."
  EXT="${found}"
  FILE="${NAMEPATH}.${SOURCEM}mib.${VOLTYPE}.${EXT}"
  VERFILE="${NAMEPATH}.partclone.ver"

else

  inputexit "Unknown method (run without arguments for help)"

fi

if [ "${EXT}" != "" ]; then
  # Z=<command>
  ensurez "${EXT}"
  exitonerror ${?}
fi

#-------------------------------------------------

# source must be unmounted

MOUNTS="$(mount | grep "${SOURCE} ")"
if [ "${MOUNTS}" != "" ]; then
  info "${MOUNTS}"
  errorexit "Mount(s) pointing to ${SOURCE}"
fi

#-------------------------------------------------

# raw

if [ "${METHOD}" = "raw" ]; then

  action "Saving ${SOURCE} (${SOURCEINFO}) to ${FILE} (raw)"

  "${SUPERPATH}/image-create" "${FILE}" ${SOURCEB} 1> /dev/null
  exitonerror ${?}

  LOOP="$("${SUPERPATH}/image-attach" "${FILE}")" 1> /dev/null
  exitonerror ${?}

  "${SUPERPATH}/device-copy" "${SOURCE}" "${LOOP}" 1> /dev/null
  STATUS=${?}

  "${SUPERPATH}/image-detach" "${LOOP}" 1> /dev/null

  if [ ${STATUS} != 0 ]; then
    errorexit
  fi

  echo "${FILE}"
  exit

fi

#-------------------------------------------------

# sparse

if [ "${METHOD}" = "sparse" ]; then

  action "Saving ${SOURCE} (${SOURCEINFO}) to ${FILE} (sparse)"

  "${SUPERPATH}/image-create" "${FILE}" ${SOURCEB} 1> /dev/null
  exitonerror ${?}

  LOOP="$("${SUPERPATH}/image-attach" "${FILE}")"
  exitonerror ${?}

  "${SUPERPATH}/device-copy" "${SOURCE}" "${LOOP}" sparse 1> /dev/null
  STATUS=${?}

  "${SUPERPATH}/image-detach" "${LOOP}" 1> /dev/null

  if [ ${STATUS} != 0 ]; then
    errorexit
  fi

  echo "${FILE}"
  exit

fi

#-------------------------------------------------

# <z>

if [ "${METHOD}" = "lz" ] || [ "${METHOD}" = "gz" ] || [ "${METHOD}" = "xz" ] || [ "${METHOD}" = "bz2" ]; then

  action "Saving ${SOURCE} (${SOURCEINFO}) to ${FILE} (${METHOD})"

  clearfile "${FILE}"
  exitonerror ${?}

  PROGRESS="$(dd --help 2>&1 | grep progress)"
  if [ "${PROGRESS}" != "" ]; then
    dd "if=${SOURCE}" status=progress bs=2048 | ${Z} -c -9 >> "${FILE}"
  else
    dd "if=${SOURCE}" bs=2048 | ${Z} -c -9 >> "${FILE}"
  fi
  exitonerror ${?} "Failed to save ${SOURCE} to ${FILE}"

fi

#-------------------------------------------------

# fs<z>

if [ "${METHOD}" = "volume.lz" ] || [ "${METHOD}" = "volume.gz" ] || [ "${METHOD}" = "volume.xz" ] || [ "${METHOD}" = "volume.bz2" ]; then

  action "Saving ${SOURCE} (${SOURCEINFO}) to ${FILE} (partclone)"

  clearfile "${FILE}"
  exitonerror ${?}

  clearfile "${VERFILE}"
  exitonerror ${?}

  VER="$(partclone.${VOLTYPE} -v)"
  echo "${VER}" >> "${VERFILE}"

  note "${VER}"
  sleep 2

  partclone.${VOLTYPE} -c -s "${SOURCE}" | ${Z} -c -9 >> "${FILE}"
  exitonerror ${?} "Failed to save ${SOURCE} to ${FILE} (partclone)"

fi

#-------------------------------------------------

# verify compressed result

if [ ! -f "${FILE}" ]; then
  errorexit "Missing ${FILE}"
fi

inheritowner "${FILE}"

ZB=$(filesize "${FILE}")

if [ ${ZB} -lt 1024 ]; then
  warning "Very small file: ${FILE} (${ZB} bytes)"
  sleep 3
fi

ZINFO="$(sizeinfo ${ZB})"

success "Saved ${SOURCE} (${SOURCEINFO}) to ${FILE} (${ZINFO} compressed)"

echo "${FILE}"

#-------------------------------------------------

#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# device-closecrypt * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Close password encrypted device (LUKS/dm-crypt)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <encryptdev>|<label>"
  info ""
  info "If <encryptdev> then label = encryptdev's volid"
  info ""
  info "All mounts will be removed first"
  info "Will close decrypted device at /dev/mapper/<label>"
  info ""
  info "Example:"
  info " ${SCRIPT} /dev/loop0p1"
  info ""
  info "See: device-opencrypt"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

SOURCE="${1}"
MODE="${2}"

if [ "${SOURCE}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

# for backward compatibility
if [ "${MODE}" != "" ] && [ "${MODE}" != "force" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

BASE="$(basename "${SOURCE}")"

if [ "${BASE}" != "${SOURCE}" ]; then

  DEVICE="$(truepath "${SOURCE}")"
  exitonerror ${?}

  checkdevice "${DEVICE}"
  exitonerror ${?}

  MATCH="$(startstring "${DEVICE}" 8)"
  if [ "${MATCH}" = "/dev/dm-" ]; then
    errorexit "Expected encrypted device, not ${DEVICE}"
  fi

  MATCH="$(startstring "${DEVICE}" 12)"
  if [ "${MATCH}" = "/dev/mapper/" ]; then
    errorexit "Expected encrypted device, not ${DEVICE}"
  fi

  LABEL="$(volumeid "${DEVICE}")"
  LABEL="$(lowerstring "${LABEL}")"
  if [ "${LABEL}" = "" ]; then
    exit 0 # nothing here
  fi

  OPENDEV="/dev/mapper/${LABEL}"
  if [ ! -e "${OPENDEV}" ]; then
    exit 0 # nothing here
  fi

else

  LABEL="${SOURCE}"
  OPENDEV="/dev/mapper/${LABEL}"
  if [ ! -e "${OPENDEV}" ]; then
    errorexit "Device not found: ${OPENDEV}"
  fi

fi

ensurecmd cryptsetup @ cryptsetup

#-------------------------------------------------

if [ "${DEVICE}" != "" ]; then
  action "Closing ${OPENDEV} (decrypted ${DEVICE})"
else
  action "Closing ${OPENDEV} (decrypted device)"
fi

unmountall "${OPENDEV}"
exitonerror ${?}

cryptsetup close "${LABEL}"
exitonerror ${?}

sync # disk commit

if [ -e "${OPENDEV}" ]; then
  errorexit "Device still open: ${OPENDEV}"
fi

success "Closed ${OPENDEV}"

#-------------------------------------------------

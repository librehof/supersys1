#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# device-unmap * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Unmap block/char device"
  info ""
  info "Usage:"
  info " ${SCRIPT} <device>"
  info ""
  info "Example:"
  info " ${SCRIPT} /mnt/somerootfs/dev/loop0"
  info ""
  info "See: device-map"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICE="${1}"

if [ "${DEVICE}" = "" ] || [ "${2}" != "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

. "${SUPERPATH}/supersys"

#-------------------------------------------------

# no device?
if [ ! -e "${DEVICE}" ]; then
  exit
fi

DEVICE="$(truepath "${DEVICE}")"
exitonerror ${?}

# device check
if [ ! -b "${DEVICE}" ] && [ ! -c "${DEVICE}" ]; then
  errorexit "Unknown block/char device: ${DEVICE}"
fi

#-------------------------------------------------

rm "${DEVICE}"
exitonerror ${?} "Failed to unmap ${DEVICE}"

success "Unmapped ${DEVICE}"

#-------------------------------------------------

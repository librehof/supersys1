#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SCRIPTUNIT="supersys"

SUPERPATH="${SCRIPTPATH}"
. "${SUPERPATH}/superlib"

#=================================================

# voldevice-add * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Add device(s) to existing RAID volume"
  info ""
  info "Usage (btrfs and zfs):"
  info " ${SCRIPT} <device>,... <vol>"
  info ""
  info "Usage (zfs special):"
  info " ${SCRIPT} <device>,... <vol> cache|spare"
  info " ${SCRIPT} <device>,... <vol> log [mirror]"
  info ""
  info "vol: /<mountdir>|<device>|<volid>|<label>"
  info ""
  info "ZFS mirror:"
  info "- Added devices will form a new \"vdev\" mirror"
  info "- Adding devices later will also form a new \"vdev\""
  info "- See format-zfs"
  info ""
  info "ZFS special devices:"
  info "- log (fast devices used as write cache)"
  info "- cache (fast devices used as read cache)"
  info "- spare (available as spares)"
  info ""
  info "BTRFS: consider using vol-balance afterwards"
  info ""
  info "See: vol-info, format-btrfs, format-zfs"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

DEVICES="${1}"
VOL="${2}"
SPECIAL="${3}"
SPECIALMIRROR="${4}"

if [ "${DEVICES}" = "" ] || [ "${VOL}" = "" ] || [ "${5}" != "" ]; then
  inputexit
fi

superlock
exitonerror ${?}

. "${SUPERPATH}/supersys"

volumedevice "${VOL}"
exitonerror ${?}

VTYPE="$(volumetype "${VOLDEVICE}")"
VLABEL="$(volumelabel "${VOLDEVICE}")"

#-------------------------------------------------

DEVICEINFO="$(replaceinstring "/dev/" "" "${DEVICES}")"

if [ "${VTYPE}" = "btrfs" ]; then

  if [ "${SPECIAL}" != "" ]; then
    inputexit
  fi
  if [ "${SPECIALMIRROR}" != "" ]; then
    inputexit
  fi

  ensurecmd btrfs @ zypper:btrfsprogs btrfs-progs btrfs-tools
  exitonerror ${?}

  if [ "${VLABEL}" = "" ]; then
    VLABEL="$(basename "${VOLDEVICE}")"
  fi

elif [ "${VTYPE}" = "zfs" ]; then

  ensurecmd zpool @ zfsutils-linux zfs
  exitonerror ${?}

  if [ "${SPECIAL}" = "" ]; then
    if [ "${SPECIALMIRROR}" != "" ]; then
      inputexit
    fi
  else
    if [ "${SPECIAL}" != "log" ] && [ "${SPECIAL}" != "cache" ] && [ "${SPECIAL}" != "spare" ]; then
      inputexit
    fi
    if [ "${SPECIALMIRROR}" != "" ] && [ "${SPECIALMIRROR}" != "mirror" ]; then
      inputexit
    fi
    if [ "${SPECIALMIRROR}" != "" ]; then
      if [ "${SPECIAL}" = "cache" ]; then
        errorexit "Cache devices can not be mirrored (${VLABEL})"
      fi
      if [ "${SPECIAL}" = "spare" ]; then
        errorexit "Spare devices can not be mirrored (${VLABEL})"
      fi
    fi
  fi

  if [ "${VLABEL}" = "" ]; then
    errorexit "Missing volume label for ${VOLDEVICE}"
  fi

else

  errorexit "Unsupported type at ${VOLDEVICE} (${VTYPE})"

fi

#-------------------------------------------------

# check/unmount devices

oldifs="${IFS}"
IFS=","
for DEVICE in ${DEVICES}
do
  DEVICE="$(truepath "${DEVICE}")"
  exitonerror ${?}

  checkdevice "${DEVICE}"
  exitonerror ${?}

  SPACEM=$("${SUPERPATH}/device-space" "${DEVICE}" mib)
  exitonerror ${?}

  if [ ${SPACEM} -lt 8 ]; then
    errorexit "Expected ${DEVICE} (${SPACEM} MiB) to be at least 8 MiB"
  fi

  if [ ${SPACEM} -lt 256 ]; then
    warning "Device ${DEVICE} is smaller than 256 MiB (${SPACEM} MiB)"
  fi

  FS="$(volumetype "${DEVICE}")"
  if [ "${?}" = 0 ] && [ "${FS}" != "" ] ; then
    warning "Found ${FS} at ${DEVICE}, quickly press CTRL-C to abort!"
    keywait 5
  fi

  unmountall "${DEVICE}"
  exitonerror ${?}
done
IFS="${oldifs}"

#-------------------------------------------------

action "Adding ${DEVICEINFO} to ${VLABEL} (${VOLDEVICE})"

clearfile "${RUNPATH}/voldevice.log"

oldifs="${IFS}"
IFS=","
for DEVICE in ${DEVICES}
do
  note "Wiping header: ${DEVICE}"
  wipefs -f -a "${DEVICE}" >> "${RUNPATH}/voldevice.log"
  exitonerror ${?} "Failed to wipe ${DEVICE}"
done
IFS="${oldifs}"

if [ "${VTYPE}" = "btrfs" ]; then

  mkdir "${RUNMOUNT}"
  mount -t btrfs "${VOLDEVICE}" "${RUNMOUNT}"
  exitonerror ${?} "Failed to mount ${VOLDEVICE}"

  oldifs="${IFS}"
  IFS=","
  for DEVICE in ${DEVICES}
  do
    btrfs device add "${DEVICE}" "${RUNMOUNT}" >> "${RUNPATH}/voldevice.log"
    exitonerror ${?} "Failed to add ${DEVICE} to ${VLABEL} (btrfs)"
  done
  IFS="${oldifs}"
  sync # disk commit
  success "Added ${DEVICEINFO} to ${VLABEL} (btrfs)"

elif [ "${VTYPE}" = "zfs" ]; then

  FSINFO="$(zpool status "${VLABEL}")"
  exitonerror ${?} "Failed to probe zfs volume (${VLABEL})"

  RAIDZ="$(echo "${FSINFO}" | grep " raidz")"
  if [ "${RAIDZ}" != "" ]; then
    errorexit "Only zfs stripe and mirror is supported (${VLABEL})"
  fi

  DEVICELIST="$(replaceinstring "," " " "${DEVICES}")"

  MIRROR="$(echo "${FSINFO}" | grep " mirror-")"

  if [ "${SPECIAL}" != "" ]; then
    if [ "${MIRROR}" = "" ] && [ "${SPECIALMIRROR}" != "" ]; then
      errorexit "Adding mirrored ${SPECIAL} to striped zfs is not supported"
    fi
    if [ "${MIRROR}" != "" ] && [ "${SPECIALMIRROR}" = "" ]; then
      note "Using striped ${SPECIAL} at mirrored zfs"
      sleep 1
    fi
    MIRROR="${SPECIALMIRROR}"
    DEVICEINFO="${SPECIAL} ${DEVICEINFO}"
  fi

  if [ "${MIRROR}" != "" ]; then
    oldifs="${IFS}"
    IFS=" "
    zpool add "${VLABEL}" ${SPECIAL} mirror ${DEVICELIST} >> "${RUNPATH}/voldevice.log"
    exitonerror ${?} "Failed to add ${DEVICEINFO} to ${VLABEL} (mirror zfs)"
    IFS="${oldifs}"
    sync # disk commit
    success "Added vdev with ${DEVICEINFO} to ${VLABEL} (mirror zfs)"
  else
    oldifs="${IFS}"
    IFS=" "
    zpool add "${VLABEL}" ${SPECIAL} ${DEVICELIST} >> "${RUNPATH}/voldevice.log"
    exitonerror ${?} "Failed to add ${DEVICEINFO} to ${VLABEL} (stripe zfs)"
    IFS="${oldifs}"
    sync # disk commit
    success "Added ${DEVICEINFO} to ${VLABEL} (stripe zfs)"
  fi

fi

#-------------------------------------------------

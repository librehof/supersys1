### Supersys 1 | changelog

GPLv3 (C) 2024 [librehof.org](http://librehof.org) [(top)](/README.md)

2024-10-01 (v1.66)
- added: subvol-info, dirmount-nfs, dirmount-nfsv3 and list-nfs

2024-02-11 (v1.65)
- renamed nic-nobridge to nic-unbridge
- dir-copy with rmcp mode (rm + quick cp reflink)

2024-01-12 (v1.64)
- device-copy, device-restore and disk-format supporting less then 2048-byte block size
- added disk-serial

2023-12-19 (v1.63)
- subvol-save supporting recursive zfs snapshots
- subvol-timerotate supporting recursive zfs snapshots
- subvol-timepush supporting recursive zfs snapshots

2023-11-02 (v1.62)
- dir-copy and dir-replicate with .exclude option (will use <dir>/.exclude file)
- new command: dir-savegit (zip a git working directory using .gitignore filter)
- superlib 1.11

2023-09-30 (v1.61)
- dir-replicate: added options `block=<block size>` and `limitkbs=<bandwidth limit>`

2023-09-22 (v1.60)
- superlib 1.10 (cleanup, new scriptlock)
- renamed: subvol-push to subvol-timepush
- device commands will use truepath
- using bzip2 (not pbzip2)
- volumedevice with case-sensitive label search
- fix: vol-id (change fat32 id)
- fix: rootfs-bootmounts (mirrorid calculation)

2023-09-13 (v1.52)
- subvol-dayrotate with fileday option (for rotating backups)

2023-09-09 (v1.51)
- new: dirmount-virtiofs
- dirmount-lukscrypt using 'auto' sector size as default
- device-closecrypt fix

2023-09-07 (v1.50)
- renamed subvol-rotate to subvol-dayrotate (!)
  - force option
  - zfs day and month snapshot mounting fix
- new: subvol-daymount (ensure zfs day/month snapshots)  
- new: subvol-timerotate (keeping N-number of UTC-timestamped snapshots)
- new: subvol-push (streaming subvol to target, UTC-timestamped snapshots)
- dirmount-zfs with submounts option (make it behave similar to btrfs)
- dirmount-remove with submounts option
- dirmount-list with proper inner directory output + info mode

2023-08-30 (v1.47)
- reduced disk sync calls
- dirmount-remove with nosync option
- superlib 1.7 (no sync on success)

2023-08-27 (v1.46)
- format-lukscrypt/zfs: default sector=4096 (no auto/ashift=0)
- support for zfs volumes (virtual block devices)
  - new commands: subdev-create/mountsaved/unmountsaved
  - new subvol-dayrot (can also rotate subdev)
- rootfs-biosgrub/efigrub can take any kernel parameter
- zfs subvol-branch using shorter branch@basename for underlying snapshot
- diskpart-list fix (mbr without label) + updated tests

2023-08-25 (v1.45)
- format-lukscrypt:
  - cipher bits and sector size control
  - default bits=256, cipher set to aes-xts-plain64
- format-zfs:
  - sector parameter (translated to ashift)
  - no sector gives auto ashift (ashift=0), see result with `zdb -C`
- routeset-list (combined routeset-ip4/6)
- supersys volumedevice fix (lowercase logic)

2023-08-24 (v1.44)
- diskpart-list with label (table option)

2023-08-23 (v1.43)
- routeset commands (print/fill nft named sets)
- subvol-rotate: force mtime change on subvol (recreating .day file)
- disk-format: only warn if less then 2048-byte block size

2023-08-08 (v1.42)
- route-publicip improvment (alternative method)

2023-06-20 (v1.41)
- added file-delta
- rootfs-efisdboot cleanup

2023-06-16 (v1.40)
- rootfs-efisdboot supporting debian 12
- rootfs-efisdupdate calling /etc/efisdpostupdate.sh if exists
- format-lukscrypt using LUKS v2 header
- tail-journal|dmesg|syslog "follow"

2023-06-08 (v1.38)
- rootfs-efisdboot supporting redundant /boot/efi2

2023-05-28 (v1.37)
- rootfs-efisd =\> rootfs-efisdboot
  - wait on mkinitramfs
- rootfs-efiupdate
  - improved wait logic

2023-05-25 (v1.36)
- rootfs-efisd:
  - generating kernel and initrd hooks
  - dracut support

2023-05-11 (v1.35)
- rootfs-efisdupdate ignoring missing kernel/initrd
- dir-copy/replicate with shorter initial note delay

2023-05-06 (v1.34)
- removed rootfs-efisd "refresh"
- rootfs-efisdupdate (replaces rootfs-efisd "refresh")

2023-05-05 (v1.33)
- luks with discard enabled
- vol-id can now set swap volid

2023-04-14 (v1.32)
- rootfs-efisd (systemd-boot)
- rootfs-uefigrub => rootfs-efigrub

2023-04-01 (v1.31)
- dir-save preserving mode-bits
- dir-delta without title
- dir-sync a b delta (with title)
- dir-sync a b newest (no default action anymore)
- dir-copy always includes mounts (will cross inner mount-points)
- new: dir-round (mtime in whole seconds)
- new: dirmount-bitlocker

2023-03-26 (v1.30)
- dir-stack REMOVED
- file-replicate REMOVED
- new: file-copy with local, http(s) and rsync (instead of file-replicate)
- dir-replicate REWORK
  - new options: ssh port, noxattr (removed xattr)
- new: dir-copy (with local and stack option)
- dir-sync: preserving dir mode bits, improved pull behaviour
- subvol-rotate: inside (if not absolute) will be relative to subvoldir parent
- superlib 1.5 - ensurecopy will preserve link (-P)

2023-03-18 (v1.21)
- added file-replicate
- superlib 1.4

2023-03-09 (v1.20)
- zfs rework
- subvol-rotate REWORK
  - with \<subvoldir\> instead of vol:subvol (!)
  - supporting both btrfs and zfs
- subvol-list supporting both btrfs and zfs (in a similar way)
- subvol-save always non-recursive (fix)
- dirmount-device can handle luks

2023-02-25 (v1.15)
- dir-delta a b \[missing\]
- dir-delta replaces 'dir-sync status'
- superlib 1.3

2023-02-20 (v1.12)
- route-name2ip/name2ips trim spaces

2023-02-19 (v1.11)
- route-name2ip/name2ips/names2ips with nftables set support

2023-02-12 (v1.10)
- dir-sync with pushsame and pullsame (push/pull mtime)

2023-01-02 (v1.9)
- partition align-check only gives warning

2022-10-29 (v1.8)
- format-exfat checks for mkfs.exfat before installing

2022-10-05 (v1.7)
- dir-stack/replicate with inplace as default (override with live option)
- removed dir-backup

2022-10-02 (v1.6)
- dir-backup, always with inplace
- dir-stack/replicate with inplace option

2022-06-16 (v1.5)
- subvol-rotate writing YYYYMMDD to \<source\>/.day and checking day1/.day for last day

2022-06-05 (v1.4)
- subvol-rotate explicit destination
- dir-replicate "host:" check

2022-05-20 (v1.3)
- nicip-obtain with options "new" and "fg"

2022-05-12 (v1.2)
- subvol-rotate (btrfs)
- dir-save/restore with new password prompt (hidden input)
- dir-replicate/stack with dry and checksum options
- remake of rootfs-biosgrub/uefigrub (working dracut+virtio)
- vimage-attach with trim and cache mode selection

2022-04-24
- rootfs-biosboot/uefiboot: fstab.n into fstab, and crypttab.n into crypttab
- nic-measure megabit

2022-04-02
- vimage-*function* (work with virtual machine images)
- rootfs-bootmounts to generate fstab and support for crypttab/cryptsetup
- rootfs-uefigrub to setup UEFI boot (prefered on newer Intel/AMD systems)
- rootfs-biosgrub for older systems
- dir-save/restore now supporting password prompt (7z)
- other minor improvments

2023-04-21
- osroot =\> rootfs
- list-*objects* => *object*-list
- nicip-list
- nicip-renew
- route-defaultip renew

2023-03-20
- more reliable partition detection
- partdisk: improved separation of disk and ordinal

# File systems | Meta data

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)

Below you find an overview of data that can be stored, in addition to directory/file name and content, 
in (most) linux file systems.
Be aware that making use of meta data beyond ownership, time and mode bits can lead 
to new (for most users, unexpected) behaviour. 

## Ownership (chown)

#### Linux commands
- change: `chown`, `chgrp`
- view: `stat` or `ls -l`

#### Ownership data

- owner - 32bit number mapped to a user name
- group - 32bit number mapped to a group name

<br>

## Modified timestamp (mtime)

#### Linux commands
using human-readable format such as: 2023-03-22 19:37:34.478420298 +0100
- view: `stat -c %y <file>`
- change: `touch "--date=<mtime>" <file>`

<br>

## Mode (chmod)

Defined in POSIX stat structure

#### Linux commands
- change: `chmod`, `mknod`, `nc`
- view: `stat` or `ls -l`

#### Mode bits (st_mode)
- *Special*: setuid (4) + setgid (2) + sticky (1)
- Owner: **r**ead (4) + **w**rite (2) + list/e**x**ecute (1) 
- Group: **r**ead (4) + **w**rite (2) + list/e**x**ecute (1)
- Others: **r**ead (4) + **w**rite (2) + list/e**x**ecute (1)  
  list if directory, execute if file

#### Special bits - examples
- sticky (o+t)
  - the /tmp directory has the **sticky** bit set 
  - => only the owner can change created files within  
- setuid (u+s)
  - passwd is owned by root and has the **setuid** bit set 
  - => executed as the owner root, not as the user
- setgid (g+s)
  - a shared directory may have the **setgid** bit set (like chmod 2755) 
  - => new sub-directories will inherit group from parent

#### Special files (st_mode)

- `l` - symbolic link
- `s` - socket
- `p` - named pipe (FIFO)
- `b` - block device with major:minor (rdev) reference 
- `c` - character device with major:minor (rdev) reference 

<br>

## File attributes (chattr)

Originates from the ext2/3/4 file system  
File attributes (chattr) are not preserved by cp, rsync and more

#### Linux commands
- change: `chattr`
- view: `lsattr`
 
#### Example of flags
  - `a` - Append only
  - `d` - No dump (exclude from backup)
  - `i` - Immutable, only superuser (root) can unset
  - `c` - Compressed (btrfs)
  - `C` - No copy-on-write (btrfs)

<br>

## Extended attributes (xattr)

Extended attributes are name=value pairs associated with files and directories  
They have been implemented with an old POSIX draft in mind ([POSIX 1003.1e](http://wt.tuxomania.net/topics/1999_06_Posix_1e/download.html))   
Names shall start with one of `security.`, `system.`, `trusted.`, or `user.`  
By default, extended attributes are not preserved by cp, rsync and more  

#### Linux commands
- change: `setfattr`
- view: `xattr -l`, `getfattr`
- change acl: `setfacl`
- view acl: `getfacl`

#### Linux capabilities
- Stored in `security.capability`

#### ACL (Access Control Lists)
- ACL:s are stored using the `system.` prefix => `system.posix_acl_access`

#### Journald ACL usage (systemd)
- Journal files are, by default, owned and readable by the "systemd-journal" group
- Adding a user to "systemd-journal" enables them to read the journal files
- Ordinary users will get their own set of journal files in /var/log/journal/
- File system **ACLs are used** to ensure users gets **read access only**

<br>

## Create and Copy behaviour

- If setgid is set on the parent directory, this effects the default group behaviour
- Depending on the group, setgid (g+s) may require superuser priviledges 

Ordinary user:
- Ownership will not be copied (!)
- Ownership will be set to the user and its default group
- Create: default mode will be 777 for directories and 666 for files, then adjusted with user's "umask"
- Copy: by default umask will be applied on the source's mode bits
  - when using cp with `-p`, mode bits can be preserved (skipping umask) along with mtime 

Superuser:
- As a superuser it is possible to invoke command options that fully (*) preserve meta data
  - Example using command cp: `cp -d --preserve=all <sourcefile> <targetfile>`
  - (*) Most linux commands will not preserve "chattr" file attributes

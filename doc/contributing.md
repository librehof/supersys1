## Supersys 1 | contributing

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)


### Summary

- The main objective is to refine and structure Linux system knowledge and put it into simplified/structured commands
- These simplified commands will then wrap underlying system commands


### The ideal information cycle from a maintainers perspective

- Read the **summary** of a supersys command (--help) or [doc/commands](doc/commands.md)
- Find out the **underlying** system command(s) inside the script
- Read the underlying system command's **man page** and play around
- Consult places like [stackoverflow.com](https://stackoverflow.com)
- **Refine** your knowledge and bring it forward to the maintainers of this project


### Reporting problems

- **Match** against already known problems
- Describe how to **repeat the problem** (in a script perhaps)
- If the problem is related to a specific linux distro, provide a **Vagrant** file

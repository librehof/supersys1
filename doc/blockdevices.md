# Storage | Block devices

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)

- Many supersys commands take a **block device** as input 
- Block devices usually originates from **disks**, or **partitions** on disks
- A file could also serve as a backend for a block device (see Loop devices)
  

### Disk devices

- Loop devices (exposing a file as a disk): 
  - **/dev/loop**0 to **/dev/loop**N
- Older disks: 
  - **/dev/sd**a (first disk), **/dev/sd**b (second disk), ...
- Newer disks (NVMe):
  - **/dev/nvme**0n1 is first disk, /dev/nvme0 is the controller (with one disk n1)
  - **/dev/nvme**1n1 is second disk, /dev/nvme1 is the controller (with one disk n1)

  
### Partition devices

- Loop partition devices: 
  - /dev/loop0**p1** (first on loop0), /dev/loop0**p2** (second on loop0), ...
- Older disk partitions: 
  - /dev/sda**1** (first on sda), /dev/sda**2** (second on sda), ... 
- Newer disk partitions: 
  - /dev/nvme0n1**p1** (first on nvme0n1), /dev/nvme0n1**p2** (second on nvme0n1), ... 


### Volumes

- Unique volume id (volid):
  - Once a volume has been created (see **format**-type), it gets a **unique** volume id 
(see **[vol-id](commands.md#vol-id)**)
  - Try: ```ls -al /dev/disk/by-uuid``` (zfs volumes may not appear here)
  - Using **volid** instead of **/dev/deviceN** avoids problems with disks being enumerated 
in another order on startup

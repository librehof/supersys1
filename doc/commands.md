## Supersys 1.66
GPLv3 (C) 2024 librehof.org
<br>

### Content:


 [device-close](#device-close) (bind) <br>
 [device-closecrypt](#device-closecrypt) (bind) <br>
 [device-copy](#device-copy) (write) <br>
 [device-map](#device-map) (bind) <br>
 [device-opencrypt](#device-opencrypt) (bind) <br>
 [device-restore](#device-restore) (write) <br>
 [device-save](#device-save) (write) <br>
 [device-space](#device-space) (read) <br>
 [device-unmap](#device-unmap) (bind) <br>
 [device-unmount](#device-unmount) (bind) <br>
<br>

 [dir-copy](#dir-copy) (write) <br>
 [dir-cow](#dir-cow) (write) <br>
 [dir-delta](#dir-delta) <br>
 [dir-nocow](#dir-nocow) (write) <br>
 [dir-replicate](#dir-replicate) (write) <br>
 [dir-restore](#dir-restore) (write) <br>
 [dir-round](#dir-round) (write) <br>
 [dir-save](#dir-save) (write) <br>
 [dir-savegit](#dir-savegit) (write) <br>
 [dir-squash](#dir-squash) (write) <br>
 [dir-sync](#dir-sync) (write) <br>
<br>

 [dirmount-bitlocker](#dirmount-bitlocker) (bind) <br>
 [dirmount-btrfs](#dirmount-btrfs) (bind) <br>
 [dirmount-device](#dirmount-device) (bind) <br>
 [dirmount-ecryptfs](#dirmount-ecryptfs) (bind) <br>
 [dirmount-exfat](#dirmount-exfat) (bind) <br>
 [dirmount-ext](#dirmount-ext) (bind) <br>
 [dirmount-fat](#dirmount-fat) (bind) <br>
 [dirmount-image](#dirmount-image) (bind) <br>
 [dirmount-iso](#dirmount-iso) (bind) <br>
 [dirmount-list](#dirmount-list) (read) <br>
 [dirmount-nfs](#dirmount-nfs) (bind) <br>
 [dirmount-nfsv3](#dirmount-nfsv3) (bind) <br>
 [dirmount-ntfs](#dirmount-ntfs) (bind) <br>
 [dirmount-overlay](#dirmount-overlay) (bind) <br>
 [dirmount-path](#dirmount-path) (bind) <br>
 [dirmount-ram](#dirmount-ram) (bind) <br>
 [dirmount-remove](#dirmount-remove) (bind) <br>
 [dirmount-share](#dirmount-share) (bind) <br>
 [dirmount-sharev1](#dirmount-sharev1) (bind) <br>
 [dirmount-squashfs](#dirmount-squashfs) (bind) <br>
 [dirmount-sshfs](#dirmount-sshfs) (bind) <br>
 [dirmount-vbox](#dirmount-vbox) (bind) <br>
 [dirmount-virtiofs](#dirmount-virtiofs) (bind) <br>
 [dirmount-zfs](#dirmount-zfs) (bind) <br>
<br>

 [disk-format](#disk-format) (write) <br>
 [disk-info](#disk-info) (read) <br>
 [disk-list](#disk-list) (read) <br>
 [disk-measure](#disk-measure) (read) <br>
 [disk-nextmib](#disk-nextmib) (read) <br>
 [disk-result](#disk-result) (read) <br>
 [disk-selfinfo](#disk-selfinfo) (read) <br>
 [disk-selftest](#disk-selftest) (write) <br>
 [disk-serial](#disk-serial) (read) <br>
 [disk-sleep](#disk-sleep) (read/bind) <br>
<br>

 [diskpart-add](#diskpart-add) (write) <br>
 [diskpart-embed](#diskpart-embed) (write) <br>
 [diskpart-expand](#diskpart-expand) (write) <br>
 [diskpart-flag](#diskpart-flag) (read/write) <br>
 [diskpart-label](#diskpart-label) (read/write) <br>
 [diskpart-list](#diskpart-list) (read) <br>
 [diskpart-remove](#diskpart-remove) (write) <br>
 [diskpart-typeid](#diskpart-typeid) (read/write) <br>
 [diskpart-uuid](#diskpart-uuid) (read/write) <br>
<br>

 [diskpt-id](#diskpt-id) (read/write) <br>
 [diskpt-restore](#diskpt-restore) (write) <br>
 [diskpt-save](#diskpt-save) (write) <br>
 [diskpt-type](#diskpt-type) (read) <br>
<br>

 [file-compress](#file-compress) (write) <br>
 [file-copy](#file-copy) (write) <br>
 [file-delta](#file-delta) (read) <br>
 [file-expand](#file-expand) (write) <br>
 [file-size](#file-size) (read) <br>
 [file-space](#file-space) (read) <br>
<br>

 [format-blank](#format-blank) (write) <br>
 [format-btrfs](#format-btrfs) (write) <br>
 [format-exfat](#format-exfat) (write) <br>
 [format-ext2](#format-ext2) (write) <br>
 [format-ext4](#format-ext4) (write) <br>
 [format-fat32](#format-fat32) (write) <br>
 [format-lukscrypt](#format-lukscrypt) (write) <br>
 [format-ntfs](#format-ntfs) (write) <br>
 [format-swap](#format-swap) (write) <br>
 [format-swapfile](#format-swapfile) (write) <br>
 [format-zfs](#format-zfs) (write) <br>
<br>

 [image-attach](#image-attach) (bind) <br>
 [image-create](#image-create) (write) <br>
 [image-detach](#image-detach) (bind) <br>
 [image-devices](#image-devices) (read) <br>
 [image-vmdk](#image-vmdk) (write) <br>
<br>

 [list-groups](#list-groups) (read) <br>
 [list-nfs](#list-nfs) (read) <br>
 [list-shares](#list-shares) (read) <br>
 [list-users](#list-users) (read) <br>
<br>

 [nic-bridge](#nic-bridge) (bind) <br>
 [nic-down](#nic-down) (bind) <br>
 [nic-info](#nic-info) (read) <br>
 [nic-list](#nic-list) (read) <br>
 [nic-measure](#nic-measure) (read) <br>
 [nic-result](#nic-result) (read) <br>
 [nic-selfinfo](#nic-selfinfo) (read) <br>
 [nic-unbridge](#nic-unbridge) (bind) <br>
 [nic-up](#nic-up) (bind) <br>
<br>

 [nicbridge-create](#nicbridge-create) (bind) <br>
 [nicbridge-delete](#nicbridge-delete) (bind) <br>
 [nicbridge-nics](#nicbridge-nics) (read) <br>
<br>

 [nicip-add](#nicip-add) (bind) <br>
 [nicip-list](#nicip-list) (read) <br>
 [nicip-obtain](#nicip-obtain) (bind) <br>
 [nicip-release](#nicip-release) (bind) <br>
 [nicip-remove](#nicip-remove) (bind) <br>
 [nicip-renew](#nicip-renew) (bind) <br>
<br>

 [rootfs-bind](#rootfs-bind) (bind) <br>
 [rootfs-biosgrub](#rootfs-biosgrub) (write) <br>
 [rootfs-bootmounts](#rootfs-bootmounts) (write) <br>
 [rootfs-efigrub](#rootfs-efigrub) (write) <br>
 [rootfs-efisdboot](#rootfs-efisdboot) (write) <br>
 [rootfs-efisdupdate](#rootfs-efisdupdate) (write) <br>
 [rootfs-mount](#rootfs-mount) (bind) <br>
 [rootfs-password](#rootfs-password) (write) <br>
 [rootfs-release](#rootfs-release) (bind) <br>
 [rootfs-run](#rootfs-run) (read/write) <br>
 [rootfs-unmount](#rootfs-unmount) (bind) <br>
<br>

 [route-defaultip](#route-defaultip) (read/bind) <br>
 [route-defaultnic](#route-defaultnic) (read) <br>
 [route-list](#route-list) (read) <br>
 [route-name2ip](#route-name2ip) (read) <br>
 [route-name2ips](#route-name2ips) (read) <br>
 [route-names2ips](#route-names2ips) (read/write) <br>
 [route-publicip](#route-publicip) (read) <br>
 [route-sockets](#route-sockets) (read) <br>
<br>

 [routeset-fill](#routeset-fill) (write) <br>
 [routeset-list](#routeset-list) (read) <br>
<br>

 [subdev-create](#subdev-create) (write) <br>
 [subdev-mountsaved](#subdev-mountsaved) (bind) <br>
 [subdev-unmountsaved](#subdev-unmountsaved) (bind) <br>
<br>

 [subvol-branch](#subvol-branch) (write) <br>
 [subvol-create](#subvol-create) (write) <br>
 [subvol-daymount](#subvol-daymount) (bind) <br>
 [subvol-dayrotate](#subvol-dayrotate) (write) <br>
 [subvol-delete](#subvol-delete) (write) <br>
 [subvol-info](#subvol-info) (read) <br>
 [subvol-list](#subvol-list) (read) <br>
 [subvol-rename](#subvol-rename) (write) <br>
 [subvol-save](#subvol-save) (write) <br>
 [subvol-timepush](#subvol-timepush) (write) <br>
 [subvol-timerotate](#subvol-timerotate) (write) <br>
<br>

 [superlib](#superlib) <br>
 [supersys](#supersys) <br>
<br>

 [sw-ensure](#sw-ensure) (write) <br>
 [sw-include](#sw-include) (write) <br>
 [sw-install](#sw-install) (write) <br>
 [sw-refresh](#sw-refresh) (bind) <br>
<br>

 [tail-dmesg](#tail-dmesg) (read) <br>
 [tail-journal](#tail-journal) (read) <br>
 [tail-syslog](#tail-syslog) (read) <br>
<br>

 [vimage-attach](#vimage-attach) (bind) <br>
 [vimage-detach](#vimage-detach) (bind) <br>
 [vimage-info](#vimage-info) (read) <br>
 [vimage-qcow2](#vimage-qcow2) (write) <br>
 [vimage-vdi](#vimage-vdi) (write) <br>
 [vimage-vmdk](#vimage-vmdk) (write) <br>
<br>

 [vol-balance](#vol-balance) (bind) <br>
 [vol-id](#vol-id) (read/write) <br>
 [vol-info](#vol-info) (read) <br>
 [vol-label](#vol-label) (read/write) <br>
 [vol-list](#vol-list) (read) <br>
 [vol-scrub](#vol-scrub) (bind) <br>
 [vol-type](#vol-type) (read) <br>
<br>

 [voldevice-add](#voldevice-add) (write) <br>
 [voldevice-check](#voldevice-check) (write) <br>
 [voldevice-remove](#voldevice-remove) (write) <br>
 [voldevice-replace](#voldevice-replace) (write) <br>
 [voldevice-resize](#voldevice-resize) (write) <br>
<br>
<br>

<hr>

### device-close
<tt>Close device <br>
 <br>
Usage: <br>
&nbsp;[device-close](#device-close) &lt;/dev/device&gt; <br>
 <br>
All mounts will be removed <br>
If disk device then all partitions <br>
will also be unmounted (sub-devices) <br>
 <br>
Will also unmount+close decrypted device(s) <br>
at /dev/mapper/&lt;volid&gt; if exist <br>
 <br>
Example: <br>
&nbsp;[device-close](#device-close) /dev/loop0 <br>
 <br>
See: [device-unmount](#device-unmount), [device-closecrypt](#device-closecrypt) <br>
 <br>
</tt>
<br>
<hr>

### device-closecrypt
<tt>Close password encrypted device (LUKS/dm-crypt) <br>
 <br>
Usage: <br>
&nbsp;[device-closecrypt](#device-closecrypt) &lt;encryptdev&gt;|&lt;label&gt; <br>
 <br>
If &lt;encryptdev&gt; then label = encryptdev's volid <br>
 <br>
All mounts will be removed first <br>
Will close decrypted device at /dev/mapper/&lt;label&gt; <br>
 <br>
Example: <br>
&nbsp;[device-closecrypt](#device-closecrypt) /dev/loop0p1 <br>
 <br>
See: [device-opencrypt](#device-opencrypt) <br>
 <br>
</tt>
<br>
<hr>

### device-copy
<tt>Copy device to other device <br>
 <br>
Usage: <br>
&nbsp;[device-copy](#device-copy) &lt;sourcedevice&gt; &lt;targetdevice&gt; [sparse] <br>
 <br>
If sparse: <br>
&dash; will use partclone to skip unused blocks <br>
&dash; device must contain a supported file system <br>
 <br>
Will fail if source is mounted <br>
All target mounts will be unmounted <br>
 <br>
Example (raw copy): <br>
&nbsp;[device-copy](#device-copy) /dev/loop0 /dev/loop1 <br>
 <br>
Example (sparse copy): <br>
&nbsp;[device-copy](#device-copy) /dev/loop0p1 /dev/loop1p1 sparse <br>
 <br>
</tt>
<br>
<hr>

### device-map
<tt>Map block/char device to other location <br>
 <br>
Usage: <br>
&nbsp;[device-map](#device-map) &lt;existing device&gt; &lt;new location&gt; <br>
 <br>
Can be used to expose a host device to a guest <br>
Guest = running inside a "chroot" environment <br>
 <br>
Example: <br>
&nbsp;[device-map](#device-map) /dev/loop0 /mnt/somerootfs/dev/loop0 <br>
 <br>
See: [device-unmap](#device-unmap) <br>
 <br>
</tt>
<br>
<hr>

### device-opencrypt
<tt>Open password encrypted device (LUKS/dm-crypt) <br>
 <br>
Usage: <br>
&nbsp;[device-opencrypt](#device-opencrypt) &lt;encryptdev&gt; [&lt;label&gt;] [key=&lt;keyfile&gt;] <br>
 <br>
Default label is encryptdev's luks volid (uuid) <br>
 <br>
Will print path to decrypted device: /dev/mapper/&lt;label&gt; <br>
 <br>
Example: <br>
&nbsp;[device-opencrypt](#device-opencrypt) /dev/loop0p1 <br>
 <br>
See: [device-closecrypt](#device-closecrypt), [format-lukscrypt](#format-lukscrypt), [vol-id](#vol-id) <br>
 <br>
</tt>
<br>
<hr>

### device-restore
<tt>Restore device from file <br>
 <br>
Usage: <br>
&nbsp;[device-restore](#device-restore) &lt;targetdevice&gt; &lt;file&gt; <br>
 <br>
file: <br>
&nbsp;&lt;name&gt;.&lt;space&gt;mib.&lt;voltype&gt;.&lt;z&gt; ==&gt; partclone <br>
&nbsp;&lt;name&gt;.&lt;space&gt;mib.&lt;z&gt; ==&gt; expand raw <br>
&nbsp;&lt;name&gt;.&lt;z&gt; ==&gt; expand raw (no space check) <br>
&nbsp;&lt;name&gt; ==&gt; raw restore <br>
 <br>
See: [device-save](#device-save) <br>
Supported compression (z): see [file-compress](#file-compress) <br>
 <br>
</tt>
<br>
<hr>

### device-save
<tt>Save device to file <br>
 <br>
Usage: <br>
&nbsp;[device-save](#device-save) &lt;device&gt; &lt;path/name&gt; raw|sparse|&lt;z&gt;|volume.&lt;z&gt; <br>
 <br>
&nbsp;raw: <br>
&nbsp; full copy (&lt;name&gt;) <br>
 <br>
&nbsp;sparse: <br>
&nbsp; sparse file copy (&lt;name&gt;) <br>
&nbsp; will use partclone to skip unused blocks <br>
&nbsp; device must contain a supported file system <br>
&nbsp; will allocate a loop device during operation <br>
 <br>
&nbsp;gz|lz|xz|bz2: <br>
&nbsp; compressed full copy (&lt;name&gt;.&lt;space&gt;mib.&lt;z&gt;) <br>
 <br>
&nbsp;volume.gz|volume.lz|volume.xz|volume.bz2: <br>
&nbsp; partclone copy (&lt;name&gt;.&lt;space&gt;mib.&lt;voltype&gt;.&lt;z&gt;) <br>
&nbsp; device must contain a supported file system <br>
&nbsp; restore may require matching partclone version! <br>
 <br>
Will fail if source is mounted <br>
 <br>
Owner of file will be derived from directory <br>
Will print complete file path on success <br>
 <br>
See: <br>
&nbsp;[device-save](#device-save), [file-compress](#file-compress) <br>
&nbsp;[en.wikipedia.org/wiki/Partclone](https://en.wikipedia.org/wiki/Partclone) <br>
 <br>
</tt>
<br>
<hr>

### device-space
<tt>Print total space of block device <br>
 <br>
Usage: <br>
&nbsp;[device-space](#device-space) &lt;device&gt; [mib|info] <br>
 <br>
Ordinary byte output: <br>
&nbsp;&lt;space in bytes&gt; <br>
 <br>
Truncated "mib" output: <br>
&nbsp;integer(bytes/1048576) <br>
 <br>
Literal "info" output: <br>
&nbsp;if space &lt; 1000000: <br>
&nbsp; "&lt;n&gt; bytes" <br>
&nbsp;elif space is a multiple of MiB: <br>
&nbsp; "&lt;m&gt; MiB" <br>
&nbsp;else (cut to one decimal): <br>
&nbsp; "&lt;m.n&gt; MiB" <br>
 <br>
Example (bytes): <br>
&nbsp;[device-space](#device-space) /dev/loop0 <br>
 <br>
Example (info string): <br>
&nbsp;[device-space](#device-space) /dev/loop0 info <br>
 <br>
</tt>
<br>
<hr>

### device-unmap
<tt>Unmap block/char device <br>
 <br>
Usage: <br>
&nbsp;[device-unmap](#device-unmap) &lt;device&gt; <br>
 <br>
Example: <br>
&nbsp;[device-unmap](#device-unmap) /mnt/somerootfs/dev/loop0 <br>
 <br>
See: [device-map](#device-map) <br>
 <br>
</tt>
<br>
<hr>

### device-unmount
<tt>Unmount device <br>
 <br>
Usage: <br>
&nbsp;[device-unmount](#device-unmount) &lt;device&gt; <br>
 <br>
Remove any mounts associated with device <br>
 <br>
If disk device then all partitions <br>
will also be unmounted (sub-devices) <br>
 <br>
Example: <br>
&nbsp;[device-unmount](#device-unmount) /dev/loop0 <br>
 <br>
</tt>
<br>
<hr>

### dir-copy
<tt>[Re]copy directory to another location <br>
 <br>
Usage: <br>
&nbsp;[dir-copy](#dir-copy) [host:]&lt;dir&gt; [host:]&lt;target&gt; [&lt;port&gt;] [&lt;report&gt;] [&lt;options&gt;] <br>
Special usage: <br>
&nbsp;[dir-copy](#dir-copy) &lt;dir&gt; &lt;target&gt; local # if a few large local files ([dir-sync](#dir-sync)) <br>
&nbsp;[dir-copy](#dir-copy) &lt;dir&gt; &lt;target&gt; rmcp&nbsp; # if many local files (rm + cp reflink) <br>
 <br>
Report: <br>
&nbsp;dry&nbsp; &nbsp; &nbsp; : will print actions, without copying (no changes) <br>
&nbsp;info&nbsp; &nbsp;&nbsp; : will print actions <br>
&nbsp;progress : will print transfer progress per file <br>
 <br>
Options: <br>
&nbsp;stack&nbsp; &nbsp; : copy on top of target directory (no delete) <br>
&nbsp;smart&nbsp; &nbsp; : detect file moves when re-copying <br>
&nbsp;plain&nbsp; &nbsp; : will not preserve mode bits (will use umask logic) <br>
&nbsp;checksum : always use checksum when comparing (not date/size) <br>
&nbsp;fastssh&nbsp; : use faster cipher when copying to remote host (aes128-gcm) <br>
&nbsp;atomic&nbsp;&nbsp; : atomic file updates (consider if target is in use) <br>
&nbsp;limitkbs=: bandwidth limit [kilobytes/s] <br>
&nbsp;maxmib=&nbsp; : exclude files above maxmib=&lt;size&gt; [MiB] <br>
&nbsp;.exclude : use &lt;dir&gt;/.exclude if present <br>
 <br>
One of (or both) source and target must be local <br>
Excluded files will not be deleted if present on the target <br>
Will not preserve ownership/chattr/acl/xattr (will use default) <br>
Uses rsync (or cp if local/rmcp is specified) <br>
 <br>
See also: [dir-sync](#dir-sync) and [dir-replicate](#dir-replicate) <br>
 <br>
</tt>
<br>
<hr>

### dir-cow
<tt>Enable copy-on-write on complete directory (btrfs) <br>
 <br>
Usage: <br>
&nbsp;[dir-cow](#dir-cow) &lt;btrfs-directory&gt; <br>
 <br>
Requires EXCLUSIVE access to directory (!) <br>
==&gt; directory must not be used during operation <br>
 <br>
Will use DOUBLE the space during operation <br>
 <br>
See also: [dir-nocow](#dir-nocow) <br>
 <br>
</tt>
<br>
<hr>

### dir-delta
<tt>Print source mismatches at target <br>
 <br>
Usage: <br>
&nbsp;[dir-delta](#dir-delta) &lt;sourcedir&gt; &lt;targetdir&gt; [missing|pushsame|pullsame] <br>
 <br>
All files involved are defined by the source dir (!) <br>
 <br>
missing: <br>
&nbsp;print missing target files only (skip file differences) <br>
 <br>
pushsame: <br>
&nbsp;source time =&gt; target, if same content but different mtime <br>
 <br>
pullsame: <br>
&nbsp;source &lt;= target time, if same content but different mtime <br>
 <br>
To make a full diff you need to go both ways: <br>
&nbsp;[dir-delta](#dir-delta) &lt;dir1&gt; &lt;dir2&gt; <br>
&nbsp;[dir-delta](#dir-delta) &lt;dir2&gt; &lt;dir1&gt; missing <br>
 <br>
See: [dir-sync](#dir-sync) <br>
 <br>
</tt>
<br>
<hr>

### dir-nocow
<tt>Disable copy-on-write on complete directory (btrfs) <br>
 <br>
Usage: <br>
&nbsp;[dir-nocow](#dir-nocow) &lt;btrfs-directory&gt; <br>
 <br>
Use for (almost a requirement when using btrfs): <br>
&dash; Database directories <br>
&dash; Virtual-machine directories <br>
 <br>
Requires EXCLUSIVE access to directory (!) <br>
==&gt; directory must not be used during operation <br>
 <br>
Checksumming will be disabled for directory (!) <br>
==&gt; trades low-level integrity for random-write speed <br>
 <br>
Will use DOUBLE the space during operation <br>
 <br>
See also: [format-btrfs](#format-btrfs), [dir-cow](#dir-cow) <br>
 <br>
</tt>
<br>
<hr>

### dir-replicate
<tt>[Re]replicate directory at another location <br>
 <br>
Usage: <br>
&nbsp;[dir-replicate](#dir-replicate) [host:]&lt;dir&gt; [host:]&lt;target&gt; [&lt;port&gt;] [&lt;report&gt;] [&lt;options&gt;] <br>
 <br>
Report: <br>
&nbsp;dry&nbsp; &nbsp; &nbsp; : will print actions, without replicating (no changes) <br>
&nbsp;info&nbsp; &nbsp;&nbsp; : will print actions <br>
&nbsp;progress : will print transfer progress per file <br>
 <br>
Options: <br>
&nbsp;smart&nbsp; &nbsp; : detect file moves when re-replicating <br>
&nbsp;noxattr&nbsp; : will not preserve xattr/acl <br>
&nbsp;mounts&nbsp;&nbsp; : will cross volume boundaries <br>
&nbsp;checksum : always use checksum when comparing (not date/size) <br>
&nbsp;fastssh&nbsp; : use faster cipher when copying to remote host (aes128-gcm) <br>
&nbsp;atomic&nbsp;&nbsp; : atomic file updates (consider if target is in use) <br>
&nbsp;block=&nbsp;&nbsp; : explicit block size [bytes] (used by rsync's delta algo) <br>
&nbsp;limitkbs=: bandwidth limit [kilobytes/s] <br>
&nbsp;maxmib=&nbsp; : exclude files above maxmib=&lt;size&gt; [MiB] <br>
&nbsp;.exclude : use &lt;dir&gt;/.exclude if present <br>
 <br>
One of (or both) source and target must be local <br>
Excluded files will not be deleted if present on the target <br>
Require superuser privileges to preserve ownership and attributes <br>
Make sure target filesystem has same user setup <br>
 <br>
Some use-cases: <br>
&dash; Periodically replicate to a remote disk (backup) <br>
&dash; Restore (fully or partially) from a remote disk <br>
&dash; Restore (fully or partially) from a snapshot (btrfs or zfs) <br>
 <br>
Notes: <br>
&dash; Until command succeed, target may be in an INCONSISTENT state <br>
&dash; Uses rsync internally (supporting remote file transfer) <br>
&dash; Extended attributes are file/dir name:value pairs (xattr) <br>
&dash; Will NOT replicate chattr flags (originates from ext2/3/4) <br>
&dash; btrfs chattr +C/+c will be ignored <br>
 <br>
</tt>
<br>
<hr>

### dir-restore
<tt>Restore directory from compressed archive file <br>
 <br>
Usage: <br>
&nbsp;[dir-restore](#dir-restore) &lt;newdir&gt; &lt;file&gt;.7z [password[=&lt;x&gt;]] [stack|force] <br>
&nbsp;[dir-restore](#dir-restore) &lt;newdir&gt; &lt;file&gt;.zip|tgz|tlz|txz [stack|force] <br>
 <br>
See: [dir-save](#dir-save) <br>
 <br>
</tt>
<br>
<hr>

### dir-round
<tt>Round mtime to whole seconds on all files <br>
 <br>
Usage: <br>
&nbsp;[dir-round](#dir-round) &lt;dir&gt; <br>
 <br>
See also: [dir-delta](#dir-delta), [dir-save](#dir-save) <br>
 <br>
</tt>
<br>
<hr>

### dir-save
<tt>Save content INSIDE directory (into compressed archive file) <br>
 <br>
Usage: <br>
&nbsp;[dir-save](#dir-save) &lt;dir&gt; [&lt;file&gt;.]7z [password[=&lt;x&gt;]] ["&lt;pattern&gt;" ...] <br>
&nbsp;[dir-save](#dir-save) &lt;dir&gt; [&lt;file&gt;.]zip|tgz|tlz|txz ["&lt;pattern&gt;" ...] <br>
 <br>
Default &lt;file&gt; = &lt;dir&gt; <br>
Usually mtime is rounded to seconds <br>
 <br>
Patterns (max 9): <br>
&nbsp;+pattern : include recursively (only zip/7z) <br>
&nbsp;:pattern : include NON-recursively (top directory) <br>
&nbsp;&dash;pattern : exclude recursively <br>
 <br>
Formats: <br>
&nbsp;7z&nbsp; - uses LZMA (with optional AES-256 password encryption) <br>
&nbsp;zip - most portable, uses DEFLATE compression <br>
&nbsp;tgz - uses tar + gzip (DEFLATE) <br>
&nbsp;tlz - uses tar + lzip (LZMA) <br>
&nbsp;txz - uses tar + xz (LZMA2) <br>
 <br>
Use tgz when linux compatibility is important <br>
 <br>
Pattern examples: <br>
&nbsp;+&#x2a;.txt&nbsp; :&nbsp; include .txt files recursively <br>
&nbsp;:&#x2a;.txt&nbsp; :&nbsp; include top .txt files <br>
&nbsp;&dash;&#x2a;.txt&nbsp; :&nbsp; exclude .txt files recursively <br>
 <br>
See: [dir-restore](#dir-restore), [dir-round](#dir-round) <br>
 <br>
</tt>
<br>
<hr>

### dir-savegit
<tt>Save local files in git directory (using .gitignore) <br>
 <br>
Usage: <br>
&nbsp;[dir-savegit](#dir-savegit) &lt;dir&gt; [&lt;file&gt;.]7z [password[=&lt;x&gt;]] <br>
&nbsp;[dir-savegit](#dir-savegit) &lt;dir&gt; [&lt;file&gt;.]zip|tgz <br>
 <br>
Default &lt;file&gt; = &lt;dir&gt; <br>
Usually mtime is rounded to seconds <br>
 <br>
Formats: <br>
&nbsp;7z&nbsp; - uses LZMA (with optional AES-256 password encryption) <br>
&nbsp;zip - most portable, uses DEFLATE compression <br>
&nbsp;tgz - uses tar + gzip (DEFLATE) <br>
 <br>
Use tgz when linux compatibility is important <br>
 <br>
See: [dir-restore](#dir-restore), [dir-save](#dir-save) <br>
 <br>
</tt>
<br>
<hr>

### dir-squash
<tt>Save directory to a "mountable" squashfs file <br>
 <br>
Usage: <br>
&nbsp;[dir-squash](#dir-squash) &lt;dir&gt; &lt;file.sqfs&gt; [allroot] [noxattr] <br>
 <br>
See: [dirmount-squashfs](#dirmount-squashfs) <br>
 <br>
</tt>
<br>
<hr>

### dir-sync
<tt>Sync source with target (local directories) <br>
 <br>
Usage: <br>
&nbsp;[dir-sync](#dir-sync) &lt;sourcedir&gt; &lt;targetdir&gt; newest|push|missing|pull <br>
&nbsp;[dir-sync](#dir-sync) &lt;sourcedir&gt; &lt;targetdir&gt; delta [missing|pushsame|pullsame] <br>
 <br>
All files involved are defined by the source dir (!) <br>
May be slow to sync many files (thousands of files) <br>
 <br>
newest: <br>
&nbsp;If source file is newer it is copied to target <br>
&nbsp;If source file is older it is updated <br>
 <br>
push: <br>
&nbsp;Stack source at target (no remove) <br>
 <br>
missing: <br>
&nbsp;Stack source at target if missing (no replace, no remove) <br>
 <br>
pull: <br>
&nbsp;Update source from target (may remove source files) <br>
 <br>
delta: <br>
&nbsp;see [dir-delta](#dir-delta) <br>
 <br>
Will preserve time, mode and links, but not chattr/xattr <br>
If ordinary user, ownership may not be preserved <br>
If superuser, ownership is inherited from parent dir <br>
 <br>
See also: [dir-delta](#dir-delta), [dir-copy](#dir-copy) and [dir-replicate](#dir-replicate) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-bitlocker
<tt>Mount bitlocker ntfs volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-bitlocker](#dirmount-bitlocker) &lt;mountdir&gt; &lt;device&gt; [key=&lt;file&gt;] [&lt;ntfs-opts&gt;] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
CAUTION: <br>
&nbsp;Stored file permissions will not be honored, <br>
&nbsp;unless using the ntfs-3g "usermapping" feature! <br>
 <br>
Global permissions are determined by &lt;options&gt; <br>
[www.tuxera.com/community/open-source-ntfs-3g](https://www.tuxera.com/community/open-source-ntfs-3g) <br>
 <br>
The key option shall point to a file containing the <br>
recovery key: n-n-n-n-n-n-n-n (on a single line) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-btrfs
<tt>Mount btrfs [sub]volume: mountdir =&gt; device(s)[:subvol] <br>
 <br>
Usage: <br>
&nbsp;[dirmount-btrfs](#dirmount-btrfs) &lt;mountdir&gt; &lt;vol&gt;[:&lt;subvol&gt;] [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
If atime is not needed, mount with option "noatime" <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Creates &lt;mountdir&gt; if missing (as superuser) <br>
 <br>
Make sure volume has same user setup as running OS (!) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-device
<tt>Mount (auto-detectable) volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-device](#dirmount-device) &lt;mountdir&gt; &lt;device&gt; [&lt;options&gt;] [stack|any|force] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
stack : mount on top of mountdir <br>
any&nbsp;&nbsp; : use existing mount, before mounting device <br>
 <br>
Note about POSIX (Linux) filesystems: <br>
&nbsp;make sure volume has same user setup as running OS (!) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-ecryptfs
<tt>Mount eCryptfs AES-256 directory <br>
 <br>
Usage: <br>
&nbsp;[dirmount-ecryptfs](#dirmount-ecryptfs) [&lt;mountdir&gt;] &lt;ecryptdir&gt; [keyoptions] <br>
 <br>
&lt;ecryptdir&gt; must end with @aes <br>
default &lt;mountdir&gt; is ecryptdir without @aes <br>
 <br>
creates mountdir if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
keyoptions to avoid prompt: <br>
&nbsp;passphrase_passwd=&lt;password&gt;,ecryptfs_fnek_sig=&lt;hex&gt; <br>
 <br>
when using passphrase_passwd from a console: <br>
&nbsp;clear command history with: history -c <br>
 <br>
Notes: <br>
&nbsp;Will make it harder for others to view your files. <br>
&nbsp;Most useful when you backup to an untrusted location, <br>
&nbsp;see [dir-replicate](#dir-replicate), never backup mountdir (!) <br>
&nbsp;File names &gt; 143 bytes cannot be encrypted. <br>
&nbsp;Avoid running from a network disk. <br>
 <br>
Example (mount/open): <br>
&nbsp;mkdir -p secret@aes # first time <br>
&nbsp;[dirmount-ecryptfs](#dirmount-ecryptfs) secret@aes <br>
 <br>
Example (close/unmount): <br>
&nbsp;[dirmount-remove](#dirmount-remove) secret <br>
 <br>
</tt>
<br>
<hr>

### dirmount-exfat
<tt>Mount exfat volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-exfat](#dirmount-exfat) &lt;mountdir&gt; &lt;vol&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt; | &lt;volid&gt; | &lt;label&gt; <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Individual file permissions are not supported <br>
Global permissions are determined by &lt;options&gt; <br>
 <br>
</tt>
<br>
<hr>

### dirmount-ext
<tt>Mount ext volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-ext](#dirmount-ext) &lt;mountdir&gt; &lt;vol&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt; | &lt;volid&gt; | &lt;label&gt; <br>
 <br>
Creates &lt;mountdir&gt; if missing (as superuser) <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Make sure volume has same user setup as running OS (!) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-fat
<tt>Mount fat/fat32 volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-fat](#dirmount-fat) &lt;mountdir&gt; &lt;vol&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt; | &lt;volid&gt; | &lt;label&gt; <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Individual file permissions not supported <br>
Global permissions are determined by &lt;options&gt; <br>
 <br>
www.kernel.org/doc/Documentation/filesystems/vfat.txt <br>
 <br>
</tt>
<br>
<hr>

### dirmount-image
<tt>Mount (auto-detectable) volume: mountdir =&gt; image file <br>
 <br>
Usage: <br>
&nbsp;[dirmount-image](#dirmount-image) &lt;mountdir&gt; &lt;image&gt;[:partition] [&lt;options&gt;] [stack|any|force] <br>
 <br>
Partition image (not a disk image) <br>
&nbsp;Image must contain (be formatted with) a supported filesystem <br>
 <br>
Disk image (image with partition table): <br>
&nbsp;Give the partition number, like image:partition <br>
&nbsp;Alternatively use [image-attach](#image-attach), then [dirmount-device](#dirmount-device) <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
stack : mount on top of mountdir <br>
any&nbsp;&nbsp; : use existing mount, before mounting image <br>
 <br>
Note about POSIX (Linux) filesystems: <br>
&nbsp;make sure volume has same user setup as running OS (!) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-iso
<tt>Mount cd/dvd (ISO9660/13346): mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-iso](#dirmount-iso) &lt;mountdir&gt; &lt;vol&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt; | &lt;volid&gt; | &lt;label&gt; <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Individual file permissions not supported <br>
Global permissions are determined by &lt;options&gt; <br>
 <br>
Example: <br>
&nbsp;[dirmount-iso](#dirmount-iso) /mnt/cdrom /dev/cdrom ro <br>
 <br>
</tt>
<br>
<hr>

### dirmount-list
<tt>List mounts inside directory (recursive) <br>
 <br>
Usage: <br>
&nbsp;[dirmount-list](#dirmount-list) &lt;directory to search&gt; [info] <br>
 <br>
</tt>
<br>
<hr>

### dirmount-nfs
<tt>Mount NFS (Network File System): mountdir =&gt; exported nfs <br>
 <br>
Usage (mount): <br>
&nbsp;[dirmount-nfs](#dirmount-nfs) &lt;mountdir&gt; &lt;host&gt;:&lt;path&gt; ["&lt;options&gt;"] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
</tt>
<br>
<hr>

### dirmount-nfsv3
<tt>Mount NFS (Network File System): mountdir =&gt; exported nfs <br>
 <br>
Usage (mount): <br>
&nbsp;[dirmount-nfsv3](#dirmount-nfsv3) &lt;mountdir&gt; &lt;host&gt;:&lt;path&gt; ["&lt;options&gt;"] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
</tt>
<br>
<hr>

### dirmount-ntfs
<tt>Mount ntfs volume: mountdir =&gt; device <br>
 <br>
Usage: <br>
&nbsp;[dirmount-ntfs](#dirmount-ntfs) &lt;mountdir&gt; &lt;vol&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;vol: &lt;device&gt; | &lt;volid&gt; | &lt;label&gt; <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
CAUTION: <br>
&nbsp;Stored file permissions will not be honored, <br>
&nbsp;unless using the ntfs-3g "usermapping" feature! <br>
 <br>
Global permissions are determined by &lt;options&gt; <br>
 <br>
[www.tuxera.com/community/open-source-ntfs-3g](https://www.tuxera.com/community/open-source-ntfs-3g) <br>
 <br>
</tt>
<br>
<hr>

### dirmount-overlay
<tt>Mount overlay: mountdir = upperdir + lowerdir <br>
 <br>
Usage: <br>
&nbsp;[dirmount-overlay](#dirmount-overlay) &lt;mountdir&gt; &lt;upperdir&gt; &lt;lowerdir&gt; [stack] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
Creates &lt;upperdir&gt; and &lt;upperdir&gt;+ if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Use "stack" to mount on top of &lt;mountdir&gt; <br>
 <br>
If "stack" then &lt;mountdir&gt; may be equal to &lt;lowerdir&gt; <br>
 <br>
Changed files are stored in &lt;upperdir&gt; <br>
Intermediate data is stored inside &lt;upperdir&gt;+ <br>
 <br>
</tt>
<br>
<hr>

### dirmount-path
<tt>Mount path (bind mount): mountdir =&gt; sourcepath <br>
 <br>
Usage: <br>
&nbsp;[dirmount-path](#dirmount-path) &lt;mountdir&gt; &lt;sourcepath&gt; [&lt;options&gt;] [stack|any] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
stack : mount on top of mountdir <br>
any&nbsp;&nbsp; : use existing mount, before mounting source <br>
 <br>
Not using stack or any: <br>
&nbsp;Will fail if mountdir already points to a mount <br>
 <br>
</tt>
<br>
<hr>

### dirmount-ram
<tt>Mount RAM disk: mountdir =&gt; tmpfs <br>
 <br>
Usage: <br>
&nbsp;[dirmount-ram](#dirmount-ram) &lt;mountdir&gt; half|&lt;maxsize&gt;[M] [&lt;options&gt;] <br>
 <br>
&nbsp;maxsize in bytes, or in MiB if M is used <br>
&nbsp;half will give a max limit of half your physical RAM <br>
 <br>
Example (default): <br>
&nbsp;[dirmount-ram](#dirmount-ram) /mnt/ramdisk half <br>
 <br>
Example (to store data only, max 4GB): <br>
&nbsp;[dirmount-ram](#dirmount-ram) /mnt/ramdata 4000M "nodev,nosuid,noexec" <br>
 <br>
Any user can write to the RAM disk <br>
 <br>
</tt>
<br>
<hr>

### dirmount-remove
<tt>Unmount from directory (and remove directory) <br>
 <br>
Usage: <br>
&nbsp;[dirmount-remove](#dirmount-remove) &lt;mountdir&gt; [stack|release] [submounts[+]] [nosync] <br>
 <br>
default: <br>
&nbsp;Will remove directory on completion <br>
&nbsp;Will silently exit if directory does not exist <br>
 <br>
stack: <br>
&nbsp;Directory must exist, and will not be removed <br>
 <br>
release: <br>
&nbsp;Like default + force device release if ZFS <br>
 <br>
submounts: <br>
&nbsp;Remove any submounts first (submounts+ =&gt; rmdir) <br>
 <br>
nosync: <br>
&nbsp;Skip disk sync (like if read-only mount) <br>
 <br>
ZFS without release: <br>
&nbsp;ZFS will keep holding on to your device(s) <br>
 <br>
To release ZFS manually: <br>
&nbsp;zpool list <br>
&nbsp;zpool export &lt;vol&gt; <br>
 <br>
</tt>
<br>
<hr>

### dirmount-share
<tt>Mount windows network share: mountdir =&gt; cifs (auto vers) <br>
 <br>
Usage (mount): <br>
&nbsp;[dirmount-share](#dirmount-share) &lt;mountdir&gt; "//&lt;host&gt;/&lt;share&gt;" ["&lt;options&gt;"] <br>
 <br>
Unmount all shares when having network problems: <br>
&nbsp;[dirmount-share](#dirmount-share) killall <br>
 <br>
Option examples (comma-separated list): <br>
&nbsp;username=&lt;user&gt;,password=&lt;pass&gt; <br>
&nbsp;credentials=&lt;login file&gt; <br>
&nbsp;guest&nbsp; &nbsp; &nbsp;&nbsp; # guest login (no username/password) <br>
&nbsp;ro&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; # read-only <br>
&nbsp;nobrl&nbsp; &nbsp; &nbsp;&nbsp; # disable byte-range locks (avoid locking problems) <br>
&nbsp;sec=ntlm&nbsp; &nbsp; # downgrade password hashing to NTLM <br>
&nbsp;sec=ntlmv2&nbsp; # downgrade password hashing to NTLMv2 <br>
&nbsp;sec=ntlmssp # enforce NTLMv2 NTLMSSP encapsulated hashing <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt;, <br>
&nbsp;iocharset=utf8,file_mode=0755,dir_mode=0755 <br>
 <br>
</tt>
<br>
<hr>

### dirmount-sharev1
<tt>Mount windows network share: mountdir =&gt; cifs (vers=1.0) <br>
 <br>
Usage (mount): <br>
&nbsp;[dirmount-sharev1](#dirmount-sharev1) &lt;mountdir&gt; "//&lt;host&gt;/&lt;share&gt;" ["&lt;options&gt;"] <br>
 <br>
Unmount all shares when having network problems: <br>
&nbsp;[dirmount-sharev1](#dirmount-sharev1) killall <br>
 <br>
Option examples (comma-separated list): <br>
&nbsp;username=&lt;user&gt;,password=&lt;pass&gt; <br>
&nbsp;credentials=&lt;login file&gt; <br>
&nbsp;guest&nbsp; &nbsp; &nbsp;&nbsp; # guest login (no username/password) <br>
&nbsp;ro&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; # read-only <br>
&nbsp;nobrl&nbsp; &nbsp; &nbsp;&nbsp; # disable byte-range locks (avoid locking problems) <br>
&nbsp;sec=ntlm&nbsp; &nbsp; # downgrade password hashing to NTLM <br>
&nbsp;sec=ntlmv2&nbsp; # downgrade password hashing to NTLMv2 <br>
&nbsp;sec=ntlmssp # enforce NTLMv2 NTLMSSP encapsulated hashing <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt;, <br>
&nbsp;iocharset=utf8,file_mode=0755,dir_mode=0755 <br>
 <br>
</tt>
<br>
<hr>

### dirmount-squashfs
<tt>Mount read-only squashfs volume: mountdir =&gt; squashfs file <br>
 <br>
Usage: <br>
&nbsp;[dirmount-squashfs](#dirmount-squashfs) &lt;mountdir&gt; &lt;file.sqfs&gt; [&lt;options&gt;] [stack|force] <br>
 <br>
&nbsp;stack : mount on top of mountdir <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Note about POSIX (Linux) filesystems: <br>
&nbsp;make sure volume has same user setup as running OS (!) <br>
 <br>
To mount a device with squashfs, see [dirmount-device](#dirmount-device) <br>
To create a squashfs file, see [dir-squash](#dir-squash) <br>
 <br>
Example: <br>
&nbsp;[dirmount-squashfs](#dirmount-squashfs) /mnt/rootfs rootfs.sqfs <br>
 <br>
</tt>
<br>
<hr>

### dirmount-sshfs
<tt>Mount remote ssh directory: mountdir = user@host:dir <br>
 <br>
Usage: <br>
&nbsp;[dirmount-sshfs](#dirmount-sshfs) &lt;mountdir&gt; &lt;user&gt;@&lt;host&gt;:&lt;dir&gt; [&lt;options&gt;] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
It is possible to mount sshfs without sudo (user mount) <br>
 <br>
Some options <br>
&nbsp;reconnect&nbsp; &nbsp; # auto reconnect if connection is lost <br>
&nbsp;allow_other&nbsp; # allow access to other users <br>
&nbsp;allow_root&nbsp;&nbsp; # allow access to root <br>
&nbsp;uid=N&nbsp; &nbsp; &nbsp; &nbsp; # set file owner id <br>
&nbsp;gid=N&nbsp; &nbsp; &nbsp; &nbsp; # set file group id <br>
&nbsp;default_permissions # enable kernel permission checking <br>
&nbsp;idmap=none|user|file <br>
&nbsp;uidfile=FILE # containing username:uid (idmap=file) <br>
&nbsp;gidfile=FILE # containing groupname:gid (idmap=file) <br>
Some SSH options <br>
&nbsp;IdentityFile=&lt;path to public ssh key file&gt; <br>
&nbsp;ServerAliveInterval=&lt;seconds&gt; <br>
&nbsp;ServerAliveCountMax=&lt;give up after count&gt; <br>
 <br>
Use "reconnect" to increase reliability <br>
This may require key authentication <br>
See ssh-keygen and ssh-copy-id &lt;user&gt;@&lt;host&gt; <br>
 <br>
Example: <br>
&nbsp;[dirmount-sshfs](#dirmount-sshfs) ${HOME}/remote someuser@somehost: "reconnect,uid=$(id -u)" <br>
 <br>
</tt>
<br>
<hr>

### dirmount-vbox
<tt>Mount vbox host share: mountdir = hostdir <br>
 <br>
Usage: <br>
&nbsp;[dirmount-vbox](#dirmount-vbox) &lt;mountdir&gt; &lt;label&gt; [&lt;options&gt;] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
Default options: <br>
&nbsp;uid=&lt;mountdir-uid&gt;,gid=&lt;mountdir-gid&gt; <br>
 <br>
</tt>
<br>
<hr>

### dirmount-virtiofs
<tt>Mount virtiofs host directory (mountdir=hostdir) <br>
 <br>
Usage: <br>
&nbsp;[dirmount-virtiofs](#dirmount-virtiofs) &lt;mountdir&gt; &lt;tag&gt; [&lt;options&gt;] <br>
 <br>
Creates &lt;mountdir&gt; if missing&#x2a; <br>
(&#x2a;) permissions inherited from parent directory <br>
 <br>
</tt>
<br>
<hr>

### dirmount-zfs
<tt>Mount zfs [sub]volume: mountdir =&gt; device(s)[:subvol] <br>
 <br>
Usage: <br>
&nbsp;[dirmount-zfs](#dirmount-zfs) &lt;mountdir&gt; &lt;vol&gt;[:&lt;subvol&gt;] [&lt;options&gt;] [&lt;mode&gt;] <br>
 <br>
&nbsp;vol: &lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
&nbsp;mode: stack|force|submounts <br>
 <br>
Use stack (or force) to mount on top of mountdir <br>
Use submounts to mount inner subvols (btrfs like) <br>
 <br>
Creates &lt;mountdir&gt; if missing (as superuser) <br>
 <br>
Permanent zfs volumes can not be used here <br>
Multiple mounts to snapshots may not be supported <br>
 <br>
Make sure volume has same user setup as running OS (!) <br>
 <br>
Possible OpenZFS dependencies on linux: <br>
&nbsp;zfsutils-linux (zfs main package) <br>
&nbsp;zfs-dkms (zfs kernel modules) <br>
&nbsp;zfs-zed (zfs event daemon) <br>
&nbsp;spl (solaris porting layer) <br>
&nbsp;kmod (manage linux kernel modules) <br>
&nbsp;zfs-initramfs (enables zfs initramfs boot) <br>
 <br>
</tt>
<br>
<hr>

### disk-format
<tt>Clear disk (and create partition table) <br>
 <br>
Usage: <br>
&nbsp;[disk-format](#disk-format) &lt;diskdevice&gt; &lt;method&gt; gpt|mbr [single [&lt;type&gt;]] <br>
&nbsp;[disk-format](#disk-format) &lt;diskdevice&gt; &lt;method&gt; blank <br>
 <br>
Minimum space is 8 MiB <br>
Will unmount existing mount(s) first <br>
 <br>
Clear methods: <br>
&nbsp;quick : clear disk edges, and existing partition headers <br>
&nbsp;light : "quick", then zero first 64 KiB of every MiB <br>
&nbsp;zero&nbsp; : zero every block on disk <br>
&nbsp;rand&nbsp; : fill every block with "randomized block" <br>
 <br>
Partition tables: <br>
&nbsp;gpt : GUID Partition Table (supporting large disks) <br>
&nbsp;mbr : MSDOS compatible partition table (2 TiB max) <br>
 <br>
Single partition: <br>
&nbsp;single: add one partition (spanning all space) <br>
&nbsp;[type]: partition type (see [diskpart-add](#diskpart-add)) <br>
 <br>
Example (quick with gpt): <br>
&nbsp;[disk-format](#disk-format) /dev/loop0 quick gpt <br>
 <br>
Example (light with mbr): <br>
&nbsp;[disk-format](#disk-format) /dev/loop0 light mbr <br>
 <br>
Example (totally blank, no partition table): <br>
&nbsp;[disk-format](#disk-format) /dev/loop0 zero <br>
 <br>
</tt>
<br>
<hr>

### disk-info
<tt>Get disk type, size and partition layout <br>
 <br>
Usage: <br>
&nbsp;[disk-info](#disk-info) &lt;diskdevice&gt; <br>
 <br>
</tt>
<br>
<hr>

### disk-list
<tt>List disk devices available in system <br>
 <br>
Usage: <br>
&nbsp;[disk-list](#disk-list) [info] <br>
 <br>
Line format: <br>
/dev/&lt;disk device&gt; <br>
 <br>
Info line format (tabbed): <br>
/dev/&lt;disk device&gt; &lt;model&gt; &lt;space&gt;MiB <br>
 <br>
</tt>
<br>
<hr>

### disk-measure
<tt>Measure disk I/O <br>
 <br>
Usage: <br>
&nbsp;[disk-measure](#disk-measure) &lt;diskdevice&gt; [&lt;seconds&gt; [tab]] <br>
 <br>
Give seconds to measure, or call [disk-result](#disk-result) afterwords <br>
 <br>
To see disk usage per process, try iotop <br>
Example: iotop -ao # accumulated usage per process <br>
 <br>
</tt>
<br>
<hr>

### disk-nextmib
<tt>Get position after last partition, in MiB <br>
 <br>
Usage: <br>
&nbsp;[disk-nextmib](#disk-nextmib) &lt;diskdevice&gt; <br>
 <br>
</tt>
<br>
<hr>

### disk-result
<tt>Print disk I/O, from start of [disk-measure](#disk-measure) <br>
 <br>
Usage: <br>
&nbsp;[disk-result](#disk-result) &lt;diskdevice&gt; [tab] <br>
 <br>
Tab-separated output: <br>
&nbsp;disk period[sec] rsize[KiB] rtime[ms] wsize[KiB] wtime[ms] <br>
 <br>
</tt>
<br>
<hr>

### disk-selfinfo
<tt>Print product info, usage and self-test (S.M.A.R.T) <br>
 <br>
Usage: <br>
&nbsp;[disk-selfinfo](#disk-selfinfo) &lt;diskdevice&gt; <br>
 <br>
See: [disk-selftest](#disk-selftest), [en.wikipedia.org/wiki/S.M.A.R.T.](https://en.wikipedia.org/wiki/S.M.A.R.T.) <br>
 <br>
</tt>
<br>
<hr>

### disk-selftest
<tt>Run disk self test (S.M.A.R.T) in the background <br>
 <br>
Usage: <br>
&nbsp;[disk-selftest](#disk-selftest) &lt;diskdevice&gt; [long] <br>
 <br>
Wait for completion, and view result with [disk-selfinfo](#disk-selfinfo) <br>
 <br>
[en.wikipedia.org/wiki/S.M.A.R.T.](https://en.wikipedia.org/wiki/S.M.A.R.T.) <br>
 <br>
</tt>
<br>
<hr>

### disk-serial
<tt>Print disk's serial number (given by the vendor) <br>
 <br>
Usage: <br>
&nbsp;[disk-serial](#disk-serial) &lt;diskdevice&gt; <br>
 <br>
A blank line will be printed if serial number is missing <br>
 <br>
</tt>
<br>
<hr>

### disk-sleep
<tt>Manage disk sleep (HDD spindown) <br>
 <br>
Get status: <br>
&nbsp;[disk-sleep](#disk-sleep) &lt;diskdevice&gt; status <br>
 <br>
Sleep immediately if no activity (one-time event): <br>
&nbsp;[disk-sleep](#disk-sleep) &lt;diskdevice&gt; now <br>
 <br>
Disable auto-sleep: <br>
&nbsp;[disk-sleep](#disk-sleep) &lt;diskdevice&gt; never <br>
 <br>
Enable auto-sleep (minutes of inactivity): <br>
&nbsp;[disk-sleep](#disk-sleep) &lt;diskdevice&gt; 30|60|120|180|240 <br>
 <br>
</tt>
<br>
<hr>

### diskpart-add
<tt>Add partition to disk <br>
 <br>
Usage: <br>
&nbsp;[diskpart-add](#diskpart-add) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;start&gt;[M] &lt;space&gt;[M] [&lt;type&gt;] <br>
 <br>
&nbsp;ordinal : expected ordinal (must match first available) <br>
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; gpt: 1..n, mbr: 1..4 <br>
 <br>
&nbsp;start&nbsp;&nbsp; : start position on disk <br>
&nbsp;space&nbsp;&nbsp; : partition space <br>
 <br>
&nbsp;types: <br>
&nbsp; linux&nbsp; : [default] any linux native file system <br>
&nbsp; swap&nbsp;&nbsp; : mark as linux swap partition <br>
&nbsp; esp&nbsp; &nbsp; : 550 (100+) MiB UEFI boot partition (format with fat32) <br>
&nbsp; fat32&nbsp; : indicate non-esp fat32 (will become bdp if gpt) <br>
&nbsp; bdp&nbsp; &nbsp; : indicate ntfs/exfat (MS basic data partition) <br>
&nbsp; biosgrub : [gpt] 1 MiB for legacy GRUB boot on gpt (no format) <br>
&nbsp; extended : [mbr] container (max 1) for logical mbr partitions <br>
 <br>
For an extended partition, EBR will be created <br>
For all other types, partition content will not be touched <br>
 <br>
Recommendations: <br>
&nbsp;Give start and space in MiB (M) <br>
&nbsp;Do not use first MiB and last MiB of disk <br>
&nbsp;When creating a swap partition, put it at the end <br>
 <br>
Will print new partition device on success <br>
 <br>
Examples: <br>
&nbsp;[diskpart-add](#diskpart-add) /dev/loop0 1 1M 3M biosgrub <br>
&nbsp;[diskpart-add](#diskpart-add) /dev/loop0 2 4M 116M esp <br>
&nbsp;[diskpart-add](#diskpart-add) /dev/loop0 3 120M 8071M <br>
 <br>
See also: <br>
&nbsp;[device-space](#device-space), [disk-info](#disk-info), [diskpart-list](#diskpart-list), [diskpart-embed](#diskpart-embed) <br>
 <br>
</tt>
<br>
<hr>

### diskpart-embed
<tt>Embed logical partition inside extended partition (mbr only) <br>
 <br>
Usage: <br>
&nbsp;[diskpart-embed](#diskpart-embed) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;start&gt;[M] &lt;space&gt;[M] [&lt;type&gt;] <br>
 <br>
&nbsp;ordinal : expected ordinal 5..n (must match first available) <br>
&nbsp;start&nbsp;&nbsp; : start position on disk <br>
&nbsp;space&nbsp;&nbsp; : partition space <br>
&nbsp;types <br>
&nbsp; linux&nbsp; : [default] any linux native file system <br>
&nbsp; swap&nbsp;&nbsp; : mark as linux swap partition <br>
&nbsp; fat32&nbsp; : indicate fat32 <br>
&nbsp; bdp&nbsp; &nbsp; : indicate ntfs/exfat <br>
 <br>
Space must fit inside extended partition (see [diskpart-add](#diskpart-add)) <br>
Leave room for initial EBR (512 bytes logical header) <br>
 <br>
Recommendations: <br>
&nbsp;Give start and space in MiB (M) <br>
&nbsp;Add 1 MiB to start to leave room for EBR <br>
 <br>
Will print partition device on success <br>
 <br>
Examples: <br>
&nbsp;[diskpart-embed](#diskpart-embed) /dev/loop0 5 121M 3975M <br>
&nbsp;[diskpart-embed](#diskpart-embed) /dev/loop0 6 4097M 4094M fat32 <br>
 <br>
</tt>
<br>
<hr>

### diskpart-expand
<tt>Expand disk partition <br>
 <br>
Usage: <br>
&nbsp;[diskpart-expand](#diskpart-expand) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;new space&gt;[M] <br>
 <br>
Use with caution <br>
 <br>
Will change partition table only (not content) <br>
Expand underlying file system afterwords <br>
 <br>
Example: <br>
&nbsp;[diskpart-expand](#diskpart-expand) /dev/loop0 3 16263M <br>
 <br>
See also: [voldevice-resize](#voldevice-resize) <br>
 <br>
</tt>
<br>
<hr>

### diskpart-flag
<tt>Print (or change) partition flag <br>
 <br>
Print: <br>
&nbsp;[diskpart-flag](#diskpart-flag) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;flag&gt; <br>
 <br>
Change: <br>
&nbsp;[diskpart-flag](#diskpart-flag) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;flag&gt; on|off <br>
 <br>
MBR flag: <br>
&nbsp;boot <br>
 <br>
GPT flags: <br>
&nbsp;system <br>
&nbsp;noefi (ignored by efi boot) <br>
&nbsp;legacyboot (non-efi boot on gpt) <br>
 <br>
GPT Chrome OS flag: <br>
&nbsp;bootok (successful boot) <br>
 <br>
GPT Microsoft flags (bdp): <br>
&nbsp;readonly (mount read-only) <br>
&nbsp;hidden <br>
&nbsp;shadow (shadow copy of another partition) <br>
&nbsp;noauto (do not automount, no drive-letter) <br>
 <br>
Example (enable boot on mbr partition): <br>
&nbsp;[diskpart-flag](#diskpart-flag) &lt;diskdevice&gt; &lt;ordinal&gt; boot on <br>
 <br>
</tt>
<br>
<hr>

### diskpart-label
<tt>Print (or change) GPT partition label <br>
 <br>
Print: <br>
&nbsp;[diskpart-label](#diskpart-label) &lt;diskdevice&gt; &lt;ordinal&gt; <br>
 <br>
Change: <br>
&nbsp;[diskpart-label](#diskpart-label) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;label&gt; <br>
 <br>
</tt>
<br>
<hr>

### diskpart-list
<tt>List disk partitions (devices or metrics) <br>
 <br>
Usage: <br>
&nbsp;[diskpart-list](#diskpart-list) &lt;diskdevice&gt; [&lt;ordinal&gt;] [info|table] <br>
 <br>
standard line: <br>
&nbsp;/dev/&lt;device&gt; <br>
 <br>
info line: <br>
&nbsp;&lt;unspecified partition info rounded to MiB&gt; <br>
 <br>
table line in bytes (tab separated): <br>
&nbsp;&lt;n&gt; &lt;start&gt; &lt;space&gt; &lt;entry&gt; &lt;align&gt; &lt;device&gt; &lt;label&gt; <br>
 <br>
&nbsp;entry:&nbsp; primary|extended|logical <br>
&nbsp;align:&nbsp; misaligned|aligned (MiB-aligned) <br>
&nbsp;label:&nbsp; partition label (blank if mbr) <br>
 <br>
Info example (skip all but partition 1): <br>
&nbsp;[diskpart-list](#diskpart-list) /dev/loop0 1 info <br>
 <br>
Scripted table example: <br>
&nbsp;. superlib <br>
&nbsp;LIST=$([diskpart-list](#diskpart-list) /dev/loop0 table) <br>
&nbsp;exitonerror ${?} <br>
&nbsp;for LINE in ${LIST}; do <br>
&nbsp;&nbsp; ORDINAL=$(tabfield "${LINE}" 1) <br>
&nbsp;&nbsp; START=$(tabfield "${LINE}" 2) <br>
&nbsp;&nbsp; echo "${ORDINAL}: ${START}" <br>
&nbsp;done <br>
 <br>
</tt>
<br>
<hr>

### diskpart-remove
<tt>Remove disk partition <br>
 <br>
Usage: <br>
&nbsp;[diskpart-remove](#diskpart-remove) &lt;diskdevice&gt; &lt;ordinal&gt; <br>
 <br>
For an extended partition, EBR will be cleared <br>
For all other types, partition content will not be touched <br>
 <br>
</tt>
<br>
<hr>

### diskpart-typeid
<tt>Print (or change) partition's type id <br>
 <br>
Print (lowercase): <br>
&nbsp;[diskpart-typeid](#diskpart-typeid) &lt;diskdevice&gt; &lt;ordinal&gt; <br>
 <br>
Change (mbr): <br>
&nbsp;[diskpart-typeid](#diskpart-typeid) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;xx&gt; <br>
 <br>
Change (gpt): <br>
&nbsp;[diskpart-typeid](#diskpart-typeid) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;uuid&gt; <br>
 <br>
Some type ids: <br>
&nbsp;linux (generic)&nbsp; 83 or 0fc63daf-8483-4772-8e79-3d69d8477de4 <br>
&nbsp;swap (linux)&nbsp; &nbsp;&nbsp; 82 or 0657fd6d-a4ab-43c4-84e5-0933c84b4f4f <br>
&nbsp;biosgrub (gpt)&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; 21686148-6449-6e6f-744e-656564454649 <br>
&nbsp;bdp (ntfs/exfat) 07 or ebd0a0a2-b9e5-4433-87c0-68b6b72699c7 <br>
&nbsp;fat32 (lba)&nbsp; &nbsp; &nbsp; 0c or &lt;bdp&gt; <br>
&nbsp;esp (fat32)&nbsp; &nbsp; &nbsp; ef or c12a7328-f81f-11d2-ba4b-00a0c93ec93b <br>
&nbsp;extended (lba)&nbsp;&nbsp; 0f <br>
 <br>
[en.wikipedia.org/wiki/Partition_type](https://en.wikipedia.org/wiki/Partition_type) <br>
[en.wikipedia.org/wiki/GUID_Partition_Table](https://en.wikipedia.org/wiki/GUID_Partition_Table) <br>
 <br>
</tt>
<br>
<hr>

### diskpart-uuid
<tt>Print (or change) GPT partition uuid <br>
 <br>
Print (lowercase): <br>
&nbsp;[diskpart-uuid](#diskpart-uuid) &lt;diskdevice&gt; &lt;ordinal&gt; <br>
 <br>
Change: <br>
&nbsp;[diskpart-uuid](#diskpart-uuid) &lt;diskdevice&gt; &lt;ordinal&gt; &lt;uuid&gt; <br>
 <br>
</tt>
<br>
<hr>

### diskpt-id
<tt>Print (or change) partition table id (lowercase) <br>
 <br>
Usage: <br>
&nbsp;[diskpt-id](#diskpt-id) &lt;diskdevice&gt; [&lt;ptid&gt;] <br>
 <br>
mbr: xxxxxxxx (32-bit hex) <br>
gpt: xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx (UUID) <br>
 <br>
</tt>
<br>
<hr>

### diskpt-restore
<tt>Restore partition table from file <br>
 <br>
Usage (gpt): <br>
&nbsp;[diskpt-restore](#diskpt-restore) &lt;diskdevice&gt; &lt;sgfile&gt; <br>
 <br>
Usage (mbr): <br>
&nbsp;[diskpt-restore](#diskpt-restore) &lt;diskdevice&gt; &lt;binfile&gt; [&lt;bytes&gt;] <br>
 <br>
Example (gpt): <br>
&nbsp;[diskpt-restore](#diskpt-restore) /dev/loop0 disk.gpt <br>
 <br>
Example (mbr): <br>
&nbsp;[diskpt-restore](#diskpt-restore) /dev/loop0 disk.mbr <br>
 <br>
Example (mbr, boot loader only): <br>
&nbsp;[diskpt-restore](#diskpt-restore) /dev/loop0 boot.mbl 440 <br>
 <br>
Example (mbr, boot loader and ptid): <br>
&nbsp;[diskpt-restore](#diskpt-restore) /dev/loop0 boot.mbl 446 <br>
 <br>
</tt>
<br>
<hr>

### diskpt-save
<tt>Extract partition table from disk to file <br>
 <br>
Usage (gpt): <br>
&nbsp;[diskpt-save](#diskpt-save) &lt;diskdevice&gt; &lt;file&gt; <br>
 <br>
Usage (mbr): <br>
&nbsp;[diskpt-save](#diskpt-save) &lt;diskdevice&gt; &lt;file&gt; [&lt;bytes&gt;] <br>
 <br>
GPT file format (sgdisk): <br>
&nbsp;protective MBR, main GPT header, backup GPT header, <br>
&nbsp;one copy of the partition table (in that order) <br>
 <br>
MBR file format: <br>
&nbsp;&nbsp; 0-445 legacy boot loader (+ ptid) <br>
&nbsp;446-512 partition table <br>
 <br>
Owner of file will be derived from directory <br>
 <br>
Example (gpt): <br>
&nbsp;[diskpt-save](#diskpt-save) /dev/loop0 file.gpt <br>
 <br>
Example (full mbr): <br>
&nbsp;[diskpt-save](#diskpt-save) /dev/loop0 file.mbr <br>
 <br>
Example (mbr, boot loader without ptid): <br>
&nbsp;[diskpt-save](#diskpt-save) /dev/loop0 file.mbl 440 <br>
 <br>
Example (mbr, boot loader): <br>
&nbsp;[diskpt-save](#diskpt-save) /dev/loop0 file.mbl 446 <br>
 <br>
</tt>
<br>
<hr>

### diskpt-type
<tt>Print partition table type <br>
 <br>
Usage: <br>
&nbsp;[diskpt-type](#diskpt-type) &lt;diskdevice&gt; <br>
 <br>
Output: mbr | gpt | unknown <br>
 <br>
</tt>
<br>
<hr>

### file-compress
<tt>Compress single file using gz, lz, xz or bz2 <br>
 <br>
Usage: <br>
&nbsp;[file-compress](#file-compress) &lt;inputfile&gt; &lt;zipfile&gt;.gz|lz|xz|bz2 <br>
 <br>
&nbsp;gz&nbsp; : medium compression ratio (fast) <br>
&nbsp;lz&nbsp; : great compression ratio (slow) <br>
&nbsp;xz&nbsp; : more advanced lz (LZMA2) <br>
&nbsp;bz2 : good for compressing sparse files <br>
 <br>
See: <br>
&nbsp;[file-expand](#file-expand) <br>
&nbsp;[en.wikipedia.org/wiki/Gzip](https://en.wikipedia.org/wiki/Gzip) <br>
&nbsp;[en.wikipedia.org/wiki/Lzip](https://en.wikipedia.org/wiki/Lzip) <br>
&nbsp;[en.wikipedia.org/wiki/XZ_Utils](https://en.wikipedia.org/wiki/XZ_Utils) <br>
&nbsp;[en.wikipedia.org/wiki/Bzip2](https://en.wikipedia.org/wiki/Bzip2) <br>
 <br>
</tt>
<br>
<hr>

### file-copy
<tt>[Re]copy file (local or remote) <br>
 <br>
Usage: <br>
&nbsp;[file-copy](#file-copy) [host:]&lt;file&gt; [host:]&lt;target&gt; [&lt;port&gt;] [&lt;options&gt;] <br>
&nbsp;[file-copy](#file-copy) http[s]://&lt;file&gt; &lt;target&gt; [insecure] <br>
&nbsp;[file-copy](#file-copy) &lt;file&gt; &lt;target&gt; local <br>
 <br>
Options: <br>
&nbsp;dry&nbsp; &nbsp; &nbsp; : display action (no change) <br>
&nbsp;plain&nbsp; &nbsp; : will not preserve mode bits (will use umask logic) <br>
&nbsp;checksum : always use checksum when comparing (not date/size) <br>
&nbsp;fastssh&nbsp; : use faster cipher when copying to remote host (aes128-gcm) <br>
&nbsp;atomic&nbsp;&nbsp; : atomic file updates (consider if target is in use) <br>
 <br>
One of (or both) source and target must be local <br>
 <br>
cp (with reflink) will be used if local <br>
curl will be used if http(s) <br>
otherwise rsync will be used <br>
 <br>
See also: [dir-copy](#dir-copy), [dir-replicate](#dir-replicate) <br>
 <br>
</tt>
<br>
<hr>

### file-delta
<tt>Diff file1 and file2 (git like output) <br>
 <br>
Usage: <br>
&nbsp;[file-delta](#file-delta) &lt;file1&gt; &lt;file2&gt; <br>
 <br>
See also: [dir-delta](#dir-delta) <br>
 <br>
</tt>
<br>
<hr>

### file-expand
<tt>Expand single gz, lz, xz or bz2 file <br>
 <br>
Usage: <br>
&nbsp;[file-expand](#file-expand) &lt;zipfile&gt;.gz|lz|xz|bz2 &lt;outfile&gt; <br>
 <br>
See: [file-compress](#file-compress) <br>
 <br>
</tt>
<br>
<hr>

### file-size
<tt>Return logical size of file <br>
 <br>
Usage: <br>
&nbsp;[file-size](#file-size) &lt;file&gt; [info] <br>
 <br>
Outputs size in bytes by default <br>
If info is given it outputs a human readable string <br>
 <br>
</tt>
<br>
<hr>

### file-space
<tt>Return space allocated on disk for a file <br>
 <br>
Usage: <br>
&nbsp;[file-space](#file-space) &lt;file&gt; [info] <br>
 <br>
Outputs space in bytes by default <br>
If info is given it outputs a human readable string <br>
 <br>
</tt>
<br>
<hr>

### format-blank
<tt>Remove file system signature on device(s) <br>
 <br>
Usage: <br>
&nbsp;[format-blank](#format-blank) &lt;device&gt;,... <br>
 <br>
</tt>
<br>
<hr>

### format-btrfs
<tt>Format device(s) with new btrfs volume <br>
 <br>
Usage - no redundancy: <br>
&nbsp;[format-btrfs](#format-btrfs) &lt;device&gt;,... &lt;label&gt; [stripe|portable] [compress] <br>
 <br>
Usage - data at two places, 2+ devices: <br>
&nbsp;[format-btrfs](#format-btrfs) &lt;device&gt;,... &lt;label&gt; mirror [compress] <br>
 <br>
File system characteristics: <br>
&dash; Snapshot (CoW) and RAID capable <br>
&dash; SSD friendly from kernel 4.14+ (prolonging SSD life) <br>
&dash; Copy-on-write (CoW) with Checksumming (CRC) <br>
&dash; Disable CoW+CRC for databases and vm:s (see [dir-nocow](#dir-nocow)) <br>
&dash; Optional Compression (zlib is default) <br>
&dash; Make sure relatime is used (shall be default) <br>
&dash; Keep number of snapshots at a reasonable number <br>
&dash; Recursive (momentary) snapshots not supported <br>
&dash; For high availability, use mirror with at least 3 devices <br>
&dash; [btrfs.wiki.kernel.org/index.php/FAQ](https://btrfs.wiki.kernel.org/index.php/FAQ) <br>
 <br>
Example - single (stripe at [voldevice-add](#voldevice-add)): <br>
&nbsp;[format-btrfs](#format-btrfs) /dev/loop0 somedata <br>
 <br>
Example - stripe (spanning two devices for speed): <br>
&nbsp;[format-btrfs](#format-btrfs) /dev/loop0,/dev/loop1 s2 stripe <br>
 <br>
Example - mirror for redundancy with 3 devices: <br>
&nbsp;[format-btrfs](#format-btrfs) /dev/loop0,/dev/loop1,/dev/loop2 m3 mirror <br>
 <br>
Example - linux portable (be careful with user ids): <br>
&nbsp;[format-btrfs](#format-btrfs) /dev/sdm1 someusb portable <br>
 <br>
See: [subvol-create](#subvol-create), [voldevice-add](#voldevice-add), [dir-nocow](#dir-nocow) <br>
 <br>
</tt>
<br>
<hr>

### format-exfat
<tt>Format device with exfat <br>
 <br>
Usage: <br>
&nbsp;[format-exfat](#format-exfat) &lt;device&gt; &lt;label&gt; <br>
 <br>
USB-stick/SD-card focused file system <br>
Introduced by Microsoft in 2006 <br>
 <br>
Example: <br>
&nbsp;[format-exfat](#format-exfat) /dev/loop0p1 somename <br>
 <br>
[en.wikipedia.org/wiki/ExFAT](https://en.wikipedia.org/wiki/ExFAT) <br>
 <br>
</tt>
<br>
<hr>

### format-ext2
<tt>Format device with ext2 file system (no journal) <br>
 <br>
Usage: <br>
&nbsp;[format-ext2](#format-ext2) &lt;device&gt; &lt;label&gt; <br>
 <br>
Example: <br>
&nbsp;[format-ext2](#format-ext2) /dev/loop0p1 somename <br>
 <br>
</tt>
<br>
<hr>

### format-ext4
<tt>Format device with ext4 file system (with journal) <br>
 <br>
Usage: <br>
&nbsp;[format-ext4](#format-ext4) &lt;device&gt; &lt;label&gt; <br>
 <br>
Example: <br>
&nbsp;[format-ext4](#format-ext4) /dev/loop0p1 somename <br>
 <br>
</tt>
<br>
<hr>

### format-fat32
<tt>Format device with portable fat32 (max 2 TiB) <br>
 <br>
Usage: <br>
&nbsp;[format-fat32](#format-fat32) &lt;device&gt; &lt;uppercase-label&gt; <br>
 <br>
Example: <br>
&nbsp;[format-fat32](#format-fat32) /dev/loop0p1 SOMEFAT32 <br>
 <br>
</tt>
<br>
<hr>

### format-lukscrypt
<tt>Format device with LUKS2 encrypted file system <br>
 <br>
Usage: <br>
&nbsp;[format-lukscrypt](#format-lukscrypt) &lt;device&gt; [key=&lt;keyfile&gt;] [sector=n] [bits=n] <br>
&nbsp;[format-lukscrypt](#format-lukscrypt) &lt;device&gt; &lt;fstype&gt; &lt;fslabel&gt; [&lt;fsargs...&gt;] <br>
 <br>
Default key size: 256 bits <br>
Default sector size: auto (check your disk) <br>
 <br>
Note on nvme disks before format: <br>
&nbsp;nvme command may be used to set 4096 bytes sector <br>
 <br>
A keyfile shall have one line without newline! <br>
 <br>
Structure: <br>
&nbsp;luks2 header =&gt; fs via dm-crypt =&gt; device <br>
 <br>
Check result: cryptsetup luksDump &lt;device&gt; <br>
Run algorithm benchmark: cryptsetup benchmark <br>
 <br>
Links: <br>
&nbsp;[en.wikipedia.org/wiki/Linux_Unified_Key_Setup](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup) <br>
&nbsp;[en.wikipedia.org/wiki/Dm-crypt](https://en.wikipedia.org/wiki/Dm-crypt) <br>
 <br>
Example: <br>
&nbsp;[format-lukscrypt](#format-lukscrypt) /dev/loop0p1 ext4 secretext4 <br>
 <br>
See: <br>
&nbsp;format-&lt;fstype&gt;, [device-opencrypt](#device-opencrypt), [device-closecrypt](#device-closecrypt) <br>
 <br>
</tt>
<br>
<hr>

### format-ntfs
<tt>Format device with ntfs (using ntfs-3g) <br>
 <br>
Usage: <br>
&nbsp;[format-ntfs](#format-ntfs) &lt;device&gt; &lt;label&gt; <br>
 <br>
Example: <br>
&nbsp;[format-ntfs](#format-ntfs) /dev/loop0p1 somename <br>
 <br>
Use MS Windows to format bootable ntfs disks <br>
 <br>
</tt>
<br>
<hr>

### format-swap
<tt>Format device with linux swap (v1) <br>
 <br>
Usage: <br>
&nbsp;[format-swap](#format-swap) &lt;device&gt; [&lt;label&gt;] <br>
 <br>
Default label: swap <br>
 <br>
Example: <br>
&nbsp;[format-swap](#format-swap) /dev/loop0p1 <br>
 <br>
</tt>
<br>
<hr>

### format-swapfile
<tt>Create and format swapfile with linux swap (v1) <br>
 <br>
Usage: <br>
&nbsp;[format-swapfile](#format-swapfile) &lt;swapfile&gt; &lt;space&gt;[M] [ext|btrfs] <br>
 <br>
&nbsp;ext: extent-based file system (will use fallocate) <br>
&nbsp;btrfs: will make file no-cow <br>
 <br>
Space given in bytes or MiB <br>
Minimum space/alignment is 8 MiB <br>
 <br>
Example creating a 8000 MiB swapfile: <br>
&nbsp;[format-swapfile](#format-swapfile) /swapfile 8000M <br>
 <br>
</tt>
<br>
<hr>

### format-zfs
<tt>Format device(s) with new zfs volume (zfs pool) <br>
 <br>
Usage - no redundancy: <br>
&nbsp;[format-zfs](#format-zfs) &lt;device&gt;,... &lt;label&gt; [stripe] [&lt;options&gt;] <br>
 <br>
Usage - n devices with data at n places: <br>
&nbsp;[format-zfs](#format-zfs) &lt;device&gt;,... &lt;label&gt; mirror [&lt;options&gt;] <br>
 <br>
&lt;options&gt;: [compress] [encrypt] [portable] [sector=n] <br>
 <br>
Default sector size: 4096 bytes (ashift=12) <br>
 <br>
Note about ZFS on Linux: <br>
&nbsp;ZFS (v0, v1, v2, ?) is maintained out-of-tree (!) <br>
&nbsp;Native encryption may be slow <br>
 <br>
Mount behaviour: <br>
&dash; This command uses legacy mount =&gt; traditional behavior <br>
&dash; For permanent auto|noauto|none, see: zfs set option <br>
 <br>
General ZFS characteristics: <br>
&dash; Snapshot (CoW) and RAID capable <br>
&dash; Can dedicate fast cache disks and spare disks <br>
&dash; Checksumming (detect silent RAM-to-Disk corruption) <br>
&dash; Optional Compression and Encryption <br>
&dash; Support for device removal is LIMITED (zfs &lt; v0.8) <br>
&dash; Rolling back a snapshot will DELETE later snapshots <br>
&dash; Costless subvolume copy requires related snapshot <br>
&dash; No support for live re-balancing and defrag <br>
&dash; ZFS may hold references to unmounted devices (!) <br>
 <br>
Mirror characteristics: <br>
&dash; Mirrored devices will be grouped into a vdev <br>
&dash; Adding devices to vdev will ONLY increase redundancy <br>
&dash; To add capacity a new vdev mirror must be created <br>
 <br>
Manage ZFS device references (imported zpools): <br>
&dash; zpool list # list active zfs pools (root volumes) <br>
&dash; zpool export &lt;label&gt; # release a zfs pool <br>
&dash; zpool destroy &lt;label&gt; # delete (!) a zfs pool <br>
 <br>
Example - single (stripe on [voldevice-add](#voldevice-add)): <br>
&nbsp;[format-zfs](#format-zfs) /dev/loop0 somedata <br>
 <br>
Example - stripe (spanning two devices for speed): <br>
&nbsp;[format-zfs](#format-zfs) /dev/loop0,/dev/loop1 s2 stripe <br>
 <br>
Example - mirror for redundancy with 2 devices: <br>
&nbsp;[format-zfs](#format-zfs) /dev/loop0,/dev/loop1 m2 mirror <br>
 <br>
See: [dirmount-zfs](#dirmount-zfs), [subvol-create](#subvol-create), [voldevice-add](#voldevice-add) <br>
 <br>
</tt>
<br>
<hr>

### image-attach
<tt>Attach disk image to loop device <br>
 <br>
Usage: <br>
&nbsp;[image-attach](#image-attach) &lt;imagefile&gt; [resue|/dev/loop&lt;0..N&gt;] [ro] <br>
 <br>
&nbsp;reuse: reuse loop device if already attached <br>
&nbsp;ro: read-only <br>
 <br>
Will print loop device on success <br>
 <br>
Example with reuse: <br>
&nbsp;DEVICE="$([image-attach](#image-attach) image.disk reuse)" <br>
 <br>
Example using next available loop device: <br>
&nbsp;DEVICE="$([image-attach](#image-attach) image.disk)" <br>
 <br>
Example with explicit loop device and read-only: <br>
&nbsp;[image-attach](#image-attach) image.disk /dev/loop0 ro <br>
 <br>
</tt>
<br>
<hr>

### image-create
<tt>Create raw disk image file (sparse) <br>
 <br>
Usage: <br>
&nbsp;[image-create](#image-create) &lt;imagefile&gt; &lt;space&gt;[M] <br>
 <br>
Space given in bytes or MiB <br>
Minimum space is 1 MiB <br>
Minimum block size is 2048 bytes <br>
 <br>
Owner inherited from parent directory (if superuser) <br>
 <br>
The file will be created "sparse", which means <br>
it will not take up any disk space initially <br>
 <br>
Unwritten (not stored) space will be returned as zero <br>
 <br>
Attention: <br>
&nbsp;When copied as a "raw stream", the target file will <br>
&nbsp;not end up sparse (will have logical size = space) <br>
 <br>
Example creating a small 1000 MiB image: <br>
&nbsp;[image-create](#image-create) image.disk 1000M <br>
 <br>
</tt>
<br>
<hr>

### image-detach
<tt>Detach image from loop device(s) <br>
 <br>
Usage: <br>
&nbsp;[image-detach](#image-detach) &lt;imagefile&gt; <br>
&nbsp;[image-detach](#image-detach) /dev/loop&lt;0..N&gt; <br>
&nbsp;[image-detach](#image-detach) all # will skip snaps <br>
 <br>
Examples: <br>
&nbsp;[image-detach](#image-detach) image.disk <br>
&nbsp;[image-detach](#image-detach) /dev/loop0 <br>
 <br>
</tt>
<br>
<hr>

### image-devices
<tt>Get list of loop device that image is attached to <br>
 <br>
Usage: <br>
&nbsp;[image-devices](#image-devices) &lt;imagefile&gt; <br>
 <br>
Will output nothing (blank) if not attached <br>
 <br>
Example: <br>
&nbsp;[image-devices](#image-devices) image.disk <br>
 <br>
</tt>
<br>
<hr>

### image-vmdk
<tt>Create vmdk file pointing at disk image <br>
 <br>
Usage: <br>
&nbsp;[image-vmdk](#image-vmdk) &lt;imagefile&gt; &lt;vmdkfile&gt; <br>
 <br>
Use the generated vmdk file to attach disk image <br>
to a virtual machine (VirtualBox, VMware, ...) <br>
 <br>
Re-generate if vmdk file has been moved <br>
CID and UUID = hash(absolute path of vmdkfile) <br>
 <br>
Owner inherited from parent directory (if superuser) <br>
 <br>
Example: <br>
&nbsp;[image-vmdk](#image-vmdk) image.disk image.vmdk <br>
 <br>
The vmdk text format originates from VMware <br>
 <br>
</tt>
<br>
<hr>

### list-groups
<tt>List user groups [experimental] <br>
 <br>
Usage: <br>
&nbsp;[list-groups](#list-groups) [label|label+id] [&lt;minid&gt; [&lt;maxid]] <br>
 <br>
</tt>
<br>
<hr>

### list-nfs
<tt>List exported NFS path(s) at given host <br>
 <br>
Usage: <br>
&nbsp;[list-nfs](#list-nfs) &lt;host&gt; <br>
 <br>
</tt>
<br>
<hr>

### list-shares
<tt>List network shares at given host <br>
 <br>
Usage: <br>
&nbsp;[list-shares](#list-shares) &lt;hostname&gt; [&lt;username&gt; [&lt;password&gt;]] <br>
 <br>
</tt>
<br>
<hr>

### list-users
<tt>List users [experimental] <br>
 <br>
Usage: <br>
&nbsp;[list-users](#list-users) label|label+groups|label+id [&lt;minid&gt; [&lt;maxid]] <br>
 <br>
</tt>
<br>
<hr>

### nic-bridge
<tt>Add network interface to bridge <br>
 <br>
Usage: <br>
&nbsp;[nic-bridge](#nic-bridge) &lt;nic&gt; &lt;bridge&gt; <br>
 <br>
Will ensure both bridge and nic is up first <br>
 <br>
Example: <br>
&nbsp;[nic-bridge](#nic-bridge) eth0 br0 <br>
 <br>
</tt>
<br>
<hr>

### nic-down
<tt>Bring network interface down <br>
 <br>
Usage: <br>
&nbsp;[nic-down](#nic-down) &lt;nic&gt; <br>
 <br>
Use [nic-list](#nic-list) to get available network interfaces <br>
 <br>
</tt>
<br>
<hr>

### nic-info
<tt>Get network interface (nic) address info <br>
 <br>
Usage: <br>
&nbsp;[nic-info](#nic-info) &lt;nic&gt; <br>
 <br>
Use [nic-list](#nic-list) to get available network interfaces <br>
 <br>
</tt>
<br>
<hr>

### nic-list
<tt>List network interfaces and bridges <br>
available in the system <br>
 <br>
Usage: <br>
&nbsp;[nic-list](#nic-list) [info|ip4|ip6] <br>
 <br>
</tt>
<br>
<hr>

### nic-measure
<tt>Measure network interface I/O <br>
 <br>
Usage: <br>
&nbsp;[nic-measure](#nic-measure) &lt;network interface&gt; [seconds [tab|megabit]] <br>
 <br>
Give seconds to measure, or call [nic-result](#nic-result) <br>
 <br>
</tt>
<br>
<hr>

### nic-result
<tt>Print network I/O, from start of [nic-measure](#nic-measure) <br>
 <br>
Usage: <br>
&nbsp;[nic-result](#nic-result) &lt;network interface&gt; [tab|megabit] <br>
 <br>
Tab-separated output: <br>
&nbsp;nic period[sec] read[bytes] write[bytes] <br>
 <br>
</tt>
<br>
<hr>

### nic-selfinfo
<tt>Get network interface link status and more <br>
 <br>
Usage: <br>
&nbsp;[nic-selfinfo](#nic-selfinfo) &lt;network interface&gt; <br>
 <br>
Use "[nic-list](#nic-list)" to get available network interface <br>
 <br>
</tt>
<br>
<hr>

### nic-unbridge
<tt>Remove network interface from any bridge <br>
 <br>
Usage: <br>
&nbsp;[nic-unbridge](#nic-unbridge) &lt;nic&gt; <br>
 <br>
</tt>
<br>
<hr>

### nic-up
<tt>Bring network interface up <br>
 <br>
Usage: <br>
&nbsp;[nic-up](#nic-up) &lt;nic&gt; <br>
 <br>
Use [nic-list](#nic-list) to get available network interfaces <br>
 <br>
</tt>
<br>
<hr>

### nicbridge-create
<tt>Create network interface bridge <br>
 <br>
Usage: <br>
&nbsp;[nicbridge-create](#nicbridge-create) &lt;bridge&gt; <br>
 <br>
The new bridge will act as a "virtual" network interface <br>
bridging a list of underlying network interfaces <br>
 <br>
The bridge can be assigned an ip address/network shared by the <br>
underlying network interfaces (see [nicip-add](#nicip-add), [nicip-obtain](#nicip-obtain)) <br>
 <br>
MAC COLLISION WARNING: <br>
&nbsp;The name of the bridge might be used as input when <br>
&nbsp;generating its unique MAC address (not so unique). <br>
&nbsp;As a workaround, name your bridge &lt;hostname&gt;&lt;N&gt; <br>
 <br>
Example: <br>
&nbsp;[nicbridge-create](#nicbridge-create) br0 <br>
&nbsp;[nic-bridge](#nic-bridge) eth0 br0 <br>
&nbsp;[nic-bridge](#nic-bridge) eth1 br0 <br>
&nbsp;[nicip-add](#nicip-add) br0 192.168.1.1/24 <br>
&nbsp;[nicbridge-nics](#nicbridge-nics) br0 <br>
 <br>
</tt>
<br>
<hr>

### nicbridge-delete
<tt>Delete network interface bridge <br>
 <br>
Usage: <br>
&nbsp;[nicbridge-delete](#nicbridge-delete) &lt;bridge&gt; <br>
 <br>
Example: <br>
&nbsp;[nicbridge-delete](#nicbridge-delete) br0 <br>
 <br>
</tt>
<br>
<hr>

### nicbridge-nics
<tt>List bridged network interfaces in the system <br>
 <br>
Usage: <br>
&nbsp;[nicbridge-nics](#nicbridge-nics) [&lt;bridge&gt;|info] <br>
 <br>
</tt>
<br>
<hr>

### nicip-add
<tt>Add static IP address/network to network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-add](#nicip-add) &lt;nic&gt; &lt;ip/mask&gt; <br>
 <br>
Make sure nic is not activly managed by NetworkManager <br>
or networkctl (systemd) <br>
 <br>
Example: <br>
&nbsp;[nicip-add](#nicip-add) eth0 192.168.1.100/24 <br>
 <br>
</tt>
<br>
<hr>

### nicip-list
<tt>List IPv4/IPv6 addresses at network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-list](#nicip-list) [&lt;nic&gt;] [info|ip4|ip6] <br>
 <br>
</tt>
<br>
<hr>

### nicip-obtain
<tt>Obtain dynamic IP (DHCP) at network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-obtain](#nicip-obtain) &lt;nic&gt; [new] [fg] <br>
 <br>
new: <br>
&nbsp;do not use lease history file <br>
 <br>
fg: <br>
&nbsp;run in foreground with detailed progress <br>
&nbsp;break with CTRL-C <br>
 <br>
Lease history is usually stored at (dhclient): <br>
&nbsp;/var/lib/dhclient/dhclient.leases <br>
&nbsp;or /var/lib/dhcp/dhclient.leases <br>
 <br>
Example: <br>
&nbsp;[nicip-obtain](#nicip-obtain) eth0 <br>
 <br>
</tt>
<br>
<hr>

### nicip-release
<tt>Release dynamic IP (DHCP) from network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-release](#nicip-release) &lt;nic&gt; <br>
 <br>
Example: <br>
&nbsp;[nicip-release](#nicip-release) eth0 <br>
 <br>
</tt>
<br>
<hr>

### nicip-remove
<tt>Remove static IP address from network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-remove](#nicip-remove) &lt;nic&gt; &lt;ip/mask&gt; <br>
 <br>
Example: <br>
&nbsp;[nicip-remove](#nicip-remove) eth0 192.168.1.100/24 <br>
 <br>
</tt>
<br>
<hr>

### nicip-renew
<tt>Renew dynamic IP (DHCP) at network interface <br>
 <br>
Usage: <br>
&nbsp;[nicip-renew](#nicip-renew) &lt;nic&gt; <br>
 <br>
Example: <br>
&nbsp;[nicip-renew](#nicip-renew) eth0 <br>
 <br>
</tt>
<br>
<hr>

### rootfs-bind
<tt>Bind host's system directories inside guest's rootfs <br>
 <br>
Usage: <br>
&nbsp;[rootfs-bind](#rootfs-bind) &lt;rootdir&gt; [resolv] <br>
 <br>
Bind-mounted directories: <br>
&nbsp;/proc <br>
&nbsp;/sys <br>
&nbsp;/dev <br>
&nbsp;/dev/pts <br>
 <br>
If resolv: <br>
&nbsp;Will temporarily replace guest's /etc/resolv.conf <br>
&nbsp;with host's (temporary copy at /etc/resolv.restore) <br>
 <br>
See: [rootfs-release](#rootfs-release), [rootfs-mount](#rootfs-mount), [rootfs-unmount](#rootfs-unmount) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-biosgrub
<tt>Setup BIOS (legacy pc) boot of linux [grub2/initrd] <br>
 <br>
Usage: <br>
&nbsp;[rootfs-biosgrub](#rootfs-biosgrub) &lt;rootdir&gt; &lt;bootdisk&gt;|nodisk [&lt;options&gt;] [&lt;params&gt;] <br>
 <br>
Boot options: <br>
&nbsp;keepconf&nbsp; &nbsp;&nbsp; keep grub config (will skip options below) <br>
&nbsp;menutime=&lt;s&gt; menu display time in seconds, default=0 <br>
&nbsp;verbose&nbsp; &nbsp; &nbsp; activate console and disable quiet mode <br>
&nbsp;multios&nbsp; &nbsp; &nbsp; include other os installations in menu <br>
&nbsp;btrfsraid&nbsp; &nbsp; try this if multi-disk btrfs boot fails <br>
 <br>
Some kernel params: <br>
&nbsp;root=&nbsp; &nbsp; &nbsp; &nbsp; rootfs, like root=UUID=&lt;volid&gt; <br>
&nbsp;rootflags=&nbsp;&nbsp; mount options, like rootflags=subvol=&lt;subvol&gt; <br>
&nbsp;consider using mitigations=off for better performance (!) <br>
 <br>
The rootdir can be the current rootfs or a guest rootfs <br>
A guest rootfs MUST be prepared first, see [rootfs-mount](#rootfs-mount) <br>
 <br>
&lt;rootdir&gt; =&gt; mount to rootfs partition or subvol <br>
&lt;rootdir&gt;/boot =&gt; can be a separate mount (optional) <br>
 <br>
If exists (n = 1..9): <br>
&nbsp;combines fstab.&lt;n&gt; into fstab, and crypttab.&lt;n&gt; into crypttab <br>
 <br>
If multiple disks: select &lt;bootdisk&gt; in BIOS on first boot <br>
 <br>
Examples: <br>
&nbsp;[rootfs-biosgrub](#rootfs-biosgrub) / /dev/loop0 menutime=5 <br>
&nbsp;[rootfs-biosgrub](#rootfs-biosgrub) /mnt/rootfs /dev/loop0 # see [rootfs-mount](#rootfs-mount) <br>
 <br>
See: [rootfs-bootmounts](#rootfs-bootmounts), [rootfs-mount](#rootfs-mount), [rootfs-efigrub](#rootfs-efigrub) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-bootmounts
<tt>Generate rootfs mount files (boot mounts) <br>
 <br>
Usage: <br>
&nbsp;[rootfs-bootmounts](#rootfs-bootmounts) &lt;rootdir&gt; &lt;device&gt; [[vol=&lt;path&gt;] subvol=&lt;subvol&gt;] [boot=&lt;bootdev&gt;] [efi=&lt;efidev&gt;] <br>
 <br>
If LUKS encrypted &lt;device&gt; it is assumed to have been opened with [device-opencrypt](#device-opencrypt) <br>
 <br>
Step 1 <br>
&nbsp;generates &lt;rootdir&gt;/etc/fstab.1 <br>
&nbsp;and &lt;rootdir&gt;/etc/crypttab.1 if LUKS encrypted &lt;device&gt; <br>
Step 2 (n = 1..9) <br>
&nbsp;combines all fstab.&lt;n&gt; into fstab <br>
&nbsp;and all crypttab.&lt;n&gt; into crypttab <br>
 <br>
If vol=&lt;path&gt; is given (must be combined with subvol): <br>
&nbsp;device volume (top volume) will be mounted at vol=&lt;path&gt; <br>
 <br>
Example: <br>
&nbsp;[rootfs-bootmounts](#rootfs-bootmounts) /mnt/rootfs /dev/loop0p2 efi=/dev/loop0p1 <br>
 <br>
Example with additional etc/fstab.2 with swapfile and mnt+tmp in RAM: <br>
/swapfile&nbsp; none&nbsp; swap&nbsp; sw&nbsp; 0&nbsp; 0 <br>
tmpfs&nbsp; /mnt&nbsp; tmpfs&nbsp; noatime,size=128M&nbsp; 0&nbsp; 0 <br>
tmpfs&nbsp; /tmp&nbsp; tmpfs&nbsp; noatime,nosuid,nodev,noexec,mode=1777&nbsp; 0&nbsp; 0 <br>
 <br>
If having /tmp in RAM, make sure RAM + swap = many gigabytes <br>
 <br>
See: [rootfs-mount](#rootfs-mount), [format-swapfile](#format-swapfile), [rootfs-efigrub](#rootfs-efigrub)|efisd|biosgrub <br>
 <br>
</tt>
<br>
<hr>

### rootfs-efigrub
<tt>Setup UEFI boot of linux intel/amd64 [grub2/initrd] <br>
 <br>
Usage: <br>
&nbsp;[rootfs-efigrub](#rootfs-efigrub) &lt;rootdir&gt; [&lt;option&gt; ...] [&lt;param&gt; ...] <br>
 <br>
Boot options: <br>
&nbsp;main&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; setup default EFI/boot/bootx64.efi <br>
&nbsp;nvram&nbsp; &nbsp; &nbsp; &nbsp; put in computer's nvram <br>
&nbsp;entry=&lt;name&gt; boot entry label, default is &lt;vendor&gt; <br>
&nbsp;keepconf&nbsp; &nbsp;&nbsp; keep grub config (will skip options below) <br>
&nbsp;menutime=&lt;s&gt; menu display time in seconds, default=0 <br>
&nbsp;verbose&nbsp; &nbsp; &nbsp; activate console and disable quiet mode <br>
&nbsp;multios&nbsp; &nbsp; &nbsp; include other os installations in menu <br>
&nbsp;btrfsraid&nbsp; &nbsp; try this if multi-disk btrfs boot fails <br>
 <br>
Some kernel params: <br>
&nbsp;root=&nbsp; &nbsp; &nbsp; &nbsp; rootfs, like root=UUID=&lt;volid&gt; <br>
&nbsp;rootflags=&nbsp;&nbsp; mount options, like rootflags=subvol=&lt;subvol&gt; <br>
&nbsp;consider using mitigations=off for better performance (!) <br>
 <br>
The rootdir can be the current rootfs or a guest rootfs <br>
A guest rootfs MUST be prepared first, see [rootfs-mount](#rootfs-mount) <br>
 <br>
&lt;rootdir&gt; =&gt; mount to rootfs partition or subvol <br>
&lt;rootdir&gt;/boot =&gt; can be a separate mount (optional) <br>
&lt;rootdir&gt;/boot/efi =&gt; mount to esp partition (fat32) <br>
 <br>
If exists (n = 1..9): <br>
&nbsp;combines fstab.&lt;n&gt; into fstab, and crypttab.&lt;n&gt; into crypttab <br>
 <br>
UEFI boot order can be stored in the computer's nvram <br>
Verify boot order in the UEFI boot menu at first reboot <br>
If missing in menu, try add EFI/&lt;entry&gt;/grubx64.efi <br>
or use efibootmgr (may not support all UEFI systems) <br>
 <br>
Steps to increase the chance of successful UEFI boot: <br>
&dash; Disable "Secure Boot" in the UEFI menu <br>
&dash; Remove "signed" grub efi package (grub-efi-amd64-signed) <br>
 <br>
Examples: <br>
&nbsp;[rootfs-efigrub](#rootfs-efigrub) / main nvram entry=linux <br>
&nbsp;[rootfs-efigrub](#rootfs-efigrub) /mnt/rootfs main # after [rootfs-mount](#rootfs-mount) <br>
 <br>
See: [rootfs-bootmounts](#rootfs-bootmounts), [rootfs-mount](#rootfs-mount), [rootfs-efisdboot](#rootfs-efisdboot) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-efisdboot
<tt>Setup UEFI boot of linux intel/amd64 [sd-boot/initrd] <br>
 <br>
Usage: <br>
&nbsp;[rootfs-efisdboot](#rootfs-efisdboot) &lt;rootdir&gt; [&lt;option&gt; ...] [&lt;param&gt; ...] <br>
 <br>
Boot options: <br>
&nbsp;main&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; put as default boot in loader.conf <br>
&nbsp;nvram&nbsp; &nbsp; &nbsp; &nbsp; put in computer's nvram <br>
&nbsp;menutime=&lt;s&gt; menu display time in seconds, default=0 <br>
&nbsp;overwrite&nbsp; &nbsp; overwrite existing boot entry <br>
 <br>
Some kernel params: <br>
&nbsp;root=&nbsp; &nbsp; &nbsp; &nbsp; rootfs, like root=UUID=&lt;volid&gt;, root=ZFS=&lt;dataset&gt; <br>
&nbsp;rootflags=&nbsp;&nbsp; mount options, like rootflags=subvol=&lt;subvol&gt; <br>
&nbsp;see: [wiki.archlinux.org/title/Kernel_parameters](https://wiki.archlinux.org/title/Kernel_parameters) <br>
&nbsp;and: [docs.kernel.org/admin-guide/kernel-parameters.html](https://docs.kernel.org/admin-guide/kernel-parameters.html) <br>
&nbsp;consider using mitigations=off for better performance (!) <br>
 <br>
The rootdir can be the current rootfs or a guest rootfs <br>
A guest rootfs MUST be prepared first, see [rootfs-mount](#rootfs-mount) <br>
 <br>
&lt;rootdir&gt; =&gt; mount to rootfs partition or subvol <br>
&lt;rootdir&gt;/boot/efi =&gt; mount to esp partition (fat32) <br>
 <br>
Structure of /boot/efi/ <br>
&nbsp;EFI/ <br>
&nbsp;&nbsp; boot/ # default loader <br>
&nbsp;&nbsp; systemd/ # sd-boot loader <br>
&nbsp;loader/ <br>
&nbsp;&nbsp; loader.conf # loader config with default entry <br>
&nbsp;&nbsp; entries/&lt;host&gt;.conf # boot entry <br>
&nbsp;&lt;host&gt;/ <br>
&nbsp;&nbsp; vmlinuz # linux kernel, copied from /boot <br>
&nbsp;&nbsp; initrd&nbsp; # initramfs or initrd, copied from /boot <br>
 <br>
initrd = minimal rootfs (cpio archive) built from &lt;rootdir&gt; <br>
 <br>
If '/boot/efi/loader/entries.srel' contains 'type1': <br>
&nbsp;will not create host structure above (will rely on OS doing UAPI) <br>
&nbsp;see: [uapi-group.org/specifications/specs](https://uapi-group.org/specifications/specs) <br>
 <br>
If exists (n = 1..9): <br>
&nbsp;combines fstab.&lt;n&gt; into fstab, and crypttab.&lt;n&gt; into crypttab <br>
 <br>
UEFI boot order may be stored in the computer's nvram <br>
Verify boot order in the UEFI boot menu at first reboot <br>
 <br>
Examples: <br>
&nbsp;[rootfs-efisdboot](#rootfs-efisdboot) / main overwrite root=UUID=&lt;volid&gt; <br>
&nbsp;[rootfs-efisdboot](#rootfs-efisdboot) /mnt/rootfs main # after [rootfs-mount](#rootfs-mount) <br>
 <br>
See: [rootfs-bootmounts](#rootfs-bootmounts), [rootfs-mount](#rootfs-mount) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-efisdupdate
<tt>Update UEFI boot of linux intel/amd64 [sd-boot/initrd] <br>
 <br>
Usage: <br>
&nbsp;[rootfs-efisdupdate](#rootfs-efisdupdate) [&lt;rootdir&gt; [main] [currentefi] [menutime=&lt;sec&gt;]] <br>
 <br>
Options: <br>
&nbsp;main&nbsp; &nbsp; &nbsp;&nbsp; put as default boot in loader.conf <br>
&nbsp;currentefi use efi path of current system (/boot/efi) <br>
&nbsp;menutime=&nbsp; update menu display time [seconds] <br>
&nbsp;wait=&nbsp; &nbsp; &nbsp; wait for program [name] to finish, then update <br>
 <br>
The rootdir can be the current rootfs (default) <br>
or a guest rootfs (does not need to be prepared) <br>
 <br>
Will ensure latest vmlinuz/initrd in host's efi dir <br>
See [rootfs-efisdboot](#rootfs-efisdboot) for more information <br>
 <br>
If efi2 exists then efi will be duplicated into efi2 <br>
 <br>
If currentefi and vmlinuz or initrd changed: <br>
&nbsp;/etc/efisdpostupdate.sh will be called (if it exists) <br>
 <br>
Examples: <br>
&nbsp;[rootfs-efisdupdate](#rootfs-efisdupdate) <br>
&nbsp;[rootfs-efisdupdate](#rootfs-efisdupdate) /mnt/rootfs main currentefi menutime=3 <br>
 <br>
</tt>
<br>
<hr>

### rootfs-mount
<tt>Mount guest rootfs [and bind to host] <br>
 <br>
Usage: <br>
&nbsp;[rootfs-mount](#rootfs-mount) &lt;rootdir&gt; &lt;rootdev&gt; [subvol=&lt;subvol&gt;] [boot=&lt;bootdev&gt;] [efi=&lt;efidev&gt;] [bind [resolv]] <br>
 <br>
&lt;rootdir&gt; =&gt; rootdev[:subvol] <br>
&lt;rootdir&gt;/boot =&gt; bootdev <br>
&lt;rootdir&gt;/boot/efi =&gt; efidev <br>
 <br>
Will prompt for password if &lt;rootdev&gt; is luks encrypted <br>
 <br>
See [rootfs-bind](#rootfs-bind) for an explanation of options bind and resolv <br>
 <br>
See also: [dirmount-device](#dirmount-device), [device-opencrypt](#device-opencrypt) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-password
<tt>Change root password inside a guest rootfs <br>
 <br>
Usage: <br>
&nbsp;[rootfs-password](#rootfs-password) &lt;rootdir&gt; password=&lt;password&gt; <br>
 <br>
</tt>
<br>
<hr>

### rootfs-release
<tt>Release system dirs from guest's rootfs <br>
 <br>
Usage: <br>
&nbsp;[rootfs-release](#rootfs-release) &lt;rootdir&gt; <br>
 <br>
See: [rootfs-bind](#rootfs-bind) <br>
 <br>
</tt>
<br>
<hr>

### rootfs-run
<tt>Run program inside guest's rootfs (using chroot) <br>
 <br>
Usage: <br>
&nbsp;[rootfs-run](#rootfs-run) &lt;rootdir&gt; "&lt;command&gt;[ &lt;args&gt;]" <br>
 <br>
Requires /bin/bash or /bin/sh inside rootfs <br>
 <br>
Example: <br>
&nbsp;[rootfs-run](#rootfs-run) /mnt/somerootfs "ls" <br>
 <br>
</tt>
<br>
<hr>

### rootfs-unmount
<tt>Unmount guest rootfs <br>
 <br>
Usage: <br>
&nbsp;[rootfs-unmount](#rootfs-unmount) &lt;rootdir&gt; [&lt;luksdevice&gt;] <br>
 <br>
Optionally give the luks encrypted device, to close, <br>
if rootfs is stored encrypted <br>
 <br>
See: [rootfs-mount](#rootfs-mount), [device-closecrypt](#device-closecrypt) <br>
 <br>
</tt>
<br>
<hr>

### route-defaultip
<tt>Print host's default network IP address (IPv4) <br>
or renew default IP address (dhcp) <br>
 <br>
Usage: <br>
&nbsp;[route-defaultip](#route-defaultip) # print <br>
&nbsp;[route-defaultip](#route-defaultip) renew # then print <br>
 <br>
</tt>
<br>
<hr>

### route-defaultnic
<tt>Print host's default network interface <br>
 <br>
Usage: <br>
&nbsp;[route-defaultnic](#route-defaultnic) <br>
 <br>
</tt>
<br>
<hr>

### route-list
<tt>List active network routes and gateway in system <br>
 <br>
Usage: <br>
&nbsp;[route-list](#route-list) <br>
 <br>
Will print routing table (using: ip route) <br>
 <br>
</tt>
<br>
<hr>

### route-name2ip
<tt>Translate domain name into ip address <br>
 <br>
Usage: <br>
&nbsp;[route-name2ip](#route-name2ip) name <br>
 <br>
If name is n.n.n.n[/mask] it will be printed as is <br>
 <br>
Uses configured Domain Name System (DNS) to translate <br>
a name into its associated ip address, or <br>
a name into the "first returned" ip address (if many) <br>
 <br>
Will return 0 (success) if ip was found/printed <br>
Will return 1 (not found) if no ip was found <br>
 <br>
See: [route-name2ips](#route-name2ips) <br>
 <br>
</tt>
<br>
<hr>

### route-name2ips
<tt>Translate domain name into sorted ip-list <br>
 <br>
Usage: <br>
&nbsp;[route-name2ips](#route-name2ips) name [info] <br>
 <br>
&nbsp;info: include comments in list <br>
 <br>
If name is n.n.n.n[/mask] it will be printed as is <br>
 <br>
Uses configured Domain Name System (DNS) to translate <br>
a name into a list of associated ip addresses <br>
 <br>
The reason a domain name may have more then one ip <br>
address is for "Load Balancing" and "Failover" <br>
 <br>
See: [route-names2ips](#route-names2ips) <br>
 <br>
</tt>
<br>
<hr>

### route-names2ips
<tt>Translate domain names into ip-set <br>
 <br>
Usage: <br>
&nbsp;[route-names2ips](#route-names2ips) &lt;namefile&gt; [info] [strict] <br>
&nbsp;[route-names2ips](#route-names2ips) &lt;namefile&gt; line [strict] <br>
&nbsp;[route-names2ips](#route-names2ips) &lt;namefile&gt; nft &lt;family&gt; &lt;table&gt; &lt;set&gt; [strict] <br>
 <br>
&nbsp;info:&nbsp;&nbsp; include comments in list <br>
&nbsp;line:&nbsp;&nbsp; output a single comma-separated line <br>
&nbsp;nft:&nbsp; &nbsp; add ip list to an existing named nftables set <br>
&nbsp;strict: stop and return error on first lookup failure <br>
 <br>
Each line in the file shall contain a name or ip[/mask] <br>
Empty lines and lines starting with # will be ignored <br>
 <br>
Uses configured Domain Name System (DNS) to translate <br>
all names into a unique set of associated ip addresses <br>
 <br>
The nft option can be used to update a firewall ip-set <br>
list set: nft list set &lt;family&gt; &lt;table&gt; &lt;set&gt; <br>
clear set: nft flush set &lt;family&gt; &lt;table&gt; &lt;set&gt; <br>
See: [wiki.nftables.org](https://wiki.nftables.org) <br>
 <br>
</tt>
<br>
<hr>

### route-publicip
<tt>Lookup and print host's public network IP address <br>
 <br>
Usage: <br>
&nbsp;[route-publicip](#route-publicip) [info] <br>
 <br>
</tt>
<br>
<hr>

### route-sockets
<tt>List active inet sockets with information <br>
 <br>
Usage: <br>
&nbsp;[route-sockets](#route-sockets) [name] <br>
 <br>
name: replace ip with host name when possible <br>
 <br>
</tt>
<br>
<hr>

### routeset-fill
<tt>Fill firewall set (nftables named set) <br>
 <br>
Usage: <br>
&nbsp;[routeset-fill](#routeset-fill) &lt;family&gt; &lt;table&gt; &lt;set&gt; &lt;file&gt; <br>
 <br>
&nbsp;family: ip|ip6|inet|... <br>
 <br>
See: <br>
&nbsp;[routeset-list](#routeset-list), [route-names2ips](#route-names2ips) <br>
 <br>
</tt>
<br>
<hr>

### routeset-list
<tt>Output firewall ip/ip6 set (nftables named set) <br>
 <br>
Usage: <br>
&nbsp;[routeset-list](#routeset-list) ip|ip6|inet &lt;table&gt; &lt;ipset&gt; [&lt;fillfile&gt;|info] <br>
 <br>
&nbsp;info: prints all information about the set <br>
 <br>
Will output one ip per line <br>
 <br>
If &lt;fillfile&gt;, file will be updated with missing ip:s <br>
 <br>
See: [routeset-fill](#routeset-fill), [route-names2ips](#route-names2ips) <br>
 <br>
</tt>
<br>
<hr>

### subdev-create
<tt>Create new zfs device subvol (virtual block device) <br>
 <br>
Usage: <br>
&nbsp;[subdev-create](#subdev-create) &lt;topvol&gt;:&lt;newsubpath&gt; &lt;space&gt;[M] [sparse] <br>
 <br>
&nbsp;topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Restrict label to: A-Z,a-z,0-9,_,-,. <br>
 <br>
See: [subvol-delete](#subvol-delete), [subvol-save](#subvol-save), subvol-dayrot <br>
 <br>
</tt>
<br>
<hr>

### subdev-mountsaved
<tt>Mount zfs device "snapshot" as read-only filesystem <br>
 <br>
Usage: <br>
&nbsp;[subdev-mountsaved](#subdev-mountsaved) &lt;mountdir&gt; &lt;topvol&gt;:&lt;subpath&gt; [ordinal] <br>
 <br>
&nbsp;topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Usefull to make file-based backup <br>
 <br>
See: [subvol-save](#subvol-save), [subdev-unmountsaved](#subdev-unmountsaved), [subdev-create](#subdev-create) <br>
 <br>
</tt>
<br>
<hr>

### subdev-unmountsaved
<tt>Unmount zfs device "snapshot" filesystem <br>
 <br>
Usage: <br>
&nbsp;[subdev-unmountsaved](#subdev-unmountsaved) &lt;mountdir&gt; &lt;topvol&gt;:&lt;subpath&gt; [ordinal] <br>
 <br>
&nbsp;topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Will unmount and remove temporary zfs branch <br>
 <br>
See: [subdev-mountsaved](#subdev-mountsaved) <br>
 <br>
</tt>
<br>
<hr>

### subvol-branch
<tt>Make writable branch of zfs/btrfs subvolume <br>
 <br>
Usage: <br>
&nbsp;[subvol-branch](#subvol-branch) &lt;topvol&gt;:&lt;subpath&gt; &lt;branchpath&gt; <br>
 <br>
topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Makes a momentary writable copy (without child-subvols) <br>
 <br>
ZFS side-effect: <br>
&nbsp;a snapshot &lt;subpath&gt;@&lt;branch&gt; will also be created <br>
&nbsp;it can NOT be removed until branch is removed (!) <br>
 <br>
</tt>
<br>
<hr>

### subvol-create
<tt>Create new zfs/btrfs subvolume <br>
 <br>
Usage: <br>
&nbsp;[subvol-create](#subvol-create) &lt;topvol&gt;:&lt;newsubpath&gt; <br>
 <br>
topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Restrict label to: A-Z,a-z,0-9,_,-,. <br>
 <br>
ZFS: <br>
&nbsp;Inherits mount behaviour etc. from parent <br>
 <br>
</tt>
<br>
<hr>

### subvol-daymount
<tt>Ensure all dayrotate snapshots are mounted <br>
 <br>
Usage: <br>
&nbsp;[subvol-daymount](#subvol-daymount) &lt;subdir&gt; &lt;historydir&gt; <br>
 <br>
Only needed for ZFS, silent exit if BTRFS <br>
 <br>
See: [subvol-dayrotate](#subvol-dayrotate) <br>
 <br>
</tt>
<br>
<hr>

### subvol-dayrotate
<tt>Rotate btrfs/zfs subvol snapshots (days and months) <br>
 <br>
Usage: <br>
&nbsp;[subvol-dayrotate](#subvol-dayrotate) &lt;subdir&gt; &lt;historydir&gt; d&lt;d&gt; [m&lt;m&gt;] [&lt;max&gt;M] [fileday] [force] <br>
 <br>
&lt;subdir&gt;&nbsp; mounted subvol to rotate (source) <br>
&lt;historydir&gt; see structure, relative subdir's parent (if relative) <br>
d&lt;d&gt;&nbsp; &nbsp; &nbsp; number of days to keep <br>
m&lt;m&gt;&nbsp; &nbsp; &nbsp; number of months to keep <br>
&lt;max&gt;M&nbsp; &nbsp; will delete monthly files &gt; max MiB (BTRFS only) <br>
fileday&nbsp;&nbsp; will use &lt;subdir&gt;/.day as current date (if file exists) <br>
force&nbsp; &nbsp;&nbsp; rotate even if same day <br>
 <br>
Will write yyyymmdd to &lt;subdir&gt;/.day (host time) <br>
Month is rotated when oldest day is yyyymm01 (first day of month) <br>
The fileday option is usefull when rotating a daily updated backup <br>
 <br>
BTRFS structure: <br>
&dash; subvoldir <br>
&dash; history-subvoldir/ <br>
&nbsp; &dash; dayN <br>
&nbsp; &dash; monthN <br>
 <br>
ZFS structure: <br>
&dash; subvoldir <br>
&dash; historydir-with-mounted-snapshots/ <br>
&nbsp; &dash; dayN <br>
&nbsp; &dash; monthN <br>
 <br>
Example with zfs using absolute history path: <br>
&nbsp;[subvol-dayrotate](#subvol-dayrotate) /mnt/zroot/archive /mnt/zroot/archive/old d10 m10 <br>
 <br>
Example with btrfs and month max: <br>
&nbsp;[subvol-dayrotate](#subvol-dayrotate) /btrfs/archive archive.old d10 m10 100M <br>
 <br>
</tt>
<br>
<hr>

### subvol-delete
<tt>Delete existing zfs/btrfs subvolume <br>
 <br>
Usage: <br>
&nbsp;[subvol-delete](#subvol-delete) &lt;topvol&gt;:&lt;subpath&gt; <br>
 <br>
topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
</tt>
<br>
<hr>

### subvol-info
<tt>Get btrfs/zfs subvolume info <br>
 <br>
Usage: <br>
&nbsp;[subvol-info](#subvol-info) &lt;vol&gt;[:subpath] <br>
 <br>
&nbsp;vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
</tt>
<br>
<hr>

### subvol-list
<tt>List btrfs/zfs subvolumes and snapshots <br>
 <br>
Usage: <br>
&nbsp;[subvol-list](#subvol-list) &lt;vol&gt;[:subpath] [nosnapshots|snapshotsof|info] <br>
 <br>
&nbsp;vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
ZFS: <br>
&nbsp;The storage pool must have been imported <br>
&nbsp;If your device has been mounted (legacy or auto), <br>
&nbsp;this would already be the case, otherwise try: <br>
&nbsp;&gt; ls /dev/disk/by-label <br>
&nbsp;&gt; zpool import &lt;label&gt; <br>
&nbsp;or <br>
&nbsp;&gt; zpool import -a # all <br>
&nbsp;To release your device(s) afterwords: <br>
&nbsp;&gt; zpool export &lt;label&gt; # be careful <br>
 <br>
</tt>
<br>
<hr>

### subvol-rename
<tt>Rename zfs/btrfs subvolume <br>
 <br>
Usage: <br>
&nbsp;[subvol-rename](#subvol-rename) &lt;topvol&gt;:&lt;subpath&gt; &lt;newsubpath&gt; <br>
 <br>
topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
</tt>
<br>
<hr>

### subvol-save
<tt>Save read-only snapshot of zfs/btrfs subvolume <br>
 <br>
Usage: <br>
&nbsp;[subvol-save](#subvol-save) &lt;topvol&gt;:&lt;subpath&gt;[@&lt;snapshot&gt;] <br>
Usage with zfs (recursive): <br>
&nbsp;[subvol-save](#subvol-save) &lt;topvol&gt;:&lt;subpath&gt;[@&lt;snapshot&gt;] deep! <br>
Usage with btrfs (custom path): <br>
&nbsp;[subvol-save](#subvol-save) &lt;topvol&gt;:&lt;subpath&gt; &lt;snapshotpath&gt; <br>
 <br>
topvol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
Default snapshot name: &lt;subpath&gt;@snapshot <br>
 <br>
Saves a momentary snapshot (non-recursive is default) <br>
BTRFS can't do recursive (deep) momentary snapshots <br>
Will print snapshot path on completion <br>
 <br>
Example: <br>
&nbsp;[subvol-save](#subvol-save) /dev/loop0:somesubvol@20191103 <br>
 <br>
Restore selected directories in subvolume: <br>
&nbsp;mount subvolume and snapshot <br>
&nbsp;use [dir-replicate](#dir-replicate) <br>
 <br>
ZFS full restore (I): <br>
&nbsp;use same method as above ([dir-replicate](#dir-replicate)) <br>
 <br>
ZFS full restore (II): <br>
&nbsp;zfs rollback -r &lt;snapshot&gt; <br>
&nbsp;newer zfs snapshots will be deleted (!) <br>
 <br>
BTRFS full restore: <br>
&nbsp;[subvol-delete](#subvol-delete) &lt;subvolume&gt; <br>
&nbsp;[subvol-branch](#subvol-branch) &lt;snapshot&gt; &lt;subvolume&gt; <br>
 <br>
</tt>
<br>
<hr>

### subvol-timepush
<tt>Push subvol/subdev stream to target (full/incremental) <br>
 <br>
Usage: <br>
&nbsp;[subvol-timepush](#subvol-timepush) &lt;vol&gt;:&lt;subvol&gt; &lt;host&gt; &lt;target&gt; n&lt;keep&gt; [&lt;options&gt;] <br>
 <br>
&nbsp;vol:&nbsp; &nbsp;&nbsp; /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
&nbsp;subvol:&nbsp; subvol to replicate (source) <br>
&nbsp;host:&nbsp; &nbsp; target hostname or ip (will use ssh) <br>
&nbsp;target:&nbsp; mounted target topvol (/&lt;mountdir&gt;) <br>
&nbsp;keep:&nbsp; &nbsp; number of snapshots to keep [1..99] <br>
&nbsp;options: <br>
&nbsp; deep!&nbsp;&nbsp; recursive zfs replication (include subvols) <br>
&nbsp; verify&nbsp; will use rsync to verify afterwords (dry run) <br>
 <br>
Snapshot naming: &lt;subvol&gt;@yyyymmddUhhmm (time in UTC) <br>
 <br>
Only supports first-level subvols <br>
Will start with time rotation if source = target <br>
Will then perform send/receive replication <br>
Full replication if target is blank <br>
 <br>
Example: <br>
&nbsp;[subvol-timepush](#subvol-timepush) /mnt/myvol:archive myhost /mnt/myvol n5 <br>
 <br>
See also: [subvol-timerotate](#subvol-timerotate), [subvol-dayrotate](#subvol-dayrotate) <br>
 <br>
</tt>
<br>
<hr>

### subvol-timerotate
<tt>Rotate btrfs/zfs subvol snapshots using timestamp <br>
 <br>
Usage: <br>
&nbsp;[subvol-timerotate](#subvol-timerotate) &lt;vol&gt;:&lt;subvol&gt; n&lt;keep&gt;|newest|list [deep!] <br>
 <br>
vol:&nbsp; &nbsp; /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
subvol: subvol to rotate (source) <br>
keep:&nbsp;&nbsp; number of snapshots to keep [1..99] <br>
newest&nbsp; print newest snapshot, without rotation! <br>
list&nbsp; &nbsp; print time snapshots, without rotation! <br>
deep!&nbsp;&nbsp; recursive snapshots (zfs only) <br>
 <br>
Snapshot naming: &lt;subvol&gt;@yyyymmddUhhmm (time in UTC) <br>
 <br>
Only supports first-level subvols <br>
Will print name of newest snapshot (if any) <br>
 <br>
Example: <br>
&nbsp;[subvol-timerotate](#subvol-timerotate) /mnt/myvol:archive n5 <br>
 <br>
See also: [subvol-timepush](#subvol-timepush), [subvol-dayrotate](#subvol-dayrotate) <br>
 <br>
</tt>
<br>
<hr>

### superlib
<tt><br>
</tt>
<br>
<hr>

### supersys
<tt><br>
</tt>
<br>
<hr>

### sw-ensure
<tt>Install software package if missing <br>
 <br>
Usage (ensure package): <br>
&nbsp;[sw-ensure](#sw-ensure) [&lt;pm&gt;:]&lt;package&gt;[+] ... <br>
 <br>
Usage (ensure command): <br>
&nbsp;[sw-ensure](#sw-ensure) &lt;cmd&gt; @ [&lt;pm&gt;:]&lt;package&gt;[+] ... <br>
 <br>
Package Managers (pm): <br>
&nbsp;apt (debian/ubuntu) <br>
&nbsp;yum (redhat/centos) <br>
&nbsp;dnf (fedora) <br>
&nbsp;pacman (arch) <br>
&nbsp;zypper (suse) <br>
&nbsp;apk (alpine) <br>
 <br>
Package is installed system-wide <br>
 <br>
It is usually more reliable to ensure a command <br>
Will use edge repo if &lt;package&gt;+ (apk only) <br>
 <br>
See [command-not-found.com](https://command-not-found.com) for package names <br>
 <br>
Example (ensure package): <br>
&nbsp;[sw-ensure](#sw-ensure) yum:mysql mysql-server <br>
 <br>
Example (ensure command): <br>
&nbsp;[sw-ensure](#sw-ensure) xxd @ xxd apt:vim-common vim <br>
 <br>
</tt>
<br>
<hr>

### sw-include
<tt>Install listed (in a file) software packages if missing <br>
 <br>
Usage: <br>
&nbsp;[sw-include](#sw-include) &lt;packagelistfile&gt; <br>
 <br>
One package (multi-distro specification) per line <br>
 <br>
Ensure package (line): <br>
&nbsp;[&lt;pm&gt;:]&lt;package&gt;[+] ... <br>
 <br>
Ensure command (line): <br>
&nbsp;&lt;cmd&gt; @ [&lt;pm&gt;:]&lt;package&gt;[+] ... <br>
 <br>
Package Managers (pm): <br>
&nbsp;apt (debian/ubuntu) <br>
&nbsp;yum (redhat/centos) <br>
&nbsp;dnf (fedora) <br>
&nbsp;pacman (arch) <br>
&nbsp;zypper (suse) <br>
&nbsp;apk (alpine) <br>
 <br>
Packages are installed system-wide <br>
 <br>
It is usually more reliable to ensure a command <br>
Will use edge repo if &lt;package&gt;+ (apk only) <br>
 <br>
See [command-not-found.com](https://command-not-found.com) for package names <br>
 <br>
Example: <br>
&nbsp;[sw-include](#sw-include) package.list <br>
 <br>
</tt>
<br>
<hr>

### sw-install
<tt>Install software package if missing <br>
 <br>
Same as [sw-ensure](#sw-ensure), but outputs package name on install <br>
Outputs nothing if package already existed <br>
 <br>
Script example with post install: <br>
 <br>
. superlib <br>
INSTALLED="$([sw-install](#sw-install) sysstat)" <br>
exitonerror ${?} <br>
if [ "${INSTALLED}" != "" ]; then <br>
&nbsp; &lt;post install actions&gt; <br>
fi <br>
 <br>
</tt>
<br>
<hr>

### sw-refresh
<tt>Refresh package manager's index <br>
 <br>
Usage: <br>
&nbsp;[sw-refresh](#sw-refresh) <br>
 <br>
May require a working internet connection <br>
 <br>
</tt>
<br>
<hr>

### tail-dmesg
<tt>Print kernel log - last (100) lines or follow <br>
 <br>
Usage: <br>
&nbsp;[tail-dmesg](#tail-dmesg) <br>
&nbsp;[tail-dmesg](#tail-dmesg) [-]&lt;lines&gt; <br>
&nbsp;[tail-dmesg](#tail-dmesg) follow <br>
 <br>
</tt>
<br>
<hr>

### tail-journal
<tt>Print systemd journal - last (100) lines or follow <br>
 <br>
Usage: <br>
&nbsp;[tail-journal](#tail-journal) <br>
&nbsp;[tail-journal](#tail-journal) [-]&lt;lines&gt; <br>
&nbsp;[tail-journal](#tail-journal) follow <br>
 <br>
</tt>
<br>
<hr>

### tail-syslog
<tt>Print syslog - last (100) lines or follow <br>
 <br>
Usage: <br>
&nbsp;[tail-syslog](#tail-syslog) <br>
&nbsp;[tail-syslog](#tail-syslog) [-]&lt;lines&gt; <br>
&nbsp;[tail-syslog](#tail-syslog) follow <br>
 <br>
</tt>
<br>
<hr>

### vimage-attach
<tt>Attach virtual-machine image (to /dev/nbd&lt;N&gt;) <br>
 <br>
Usage: <br>
&nbsp;[vimage-attach](#vimage-attach) &lt;imagefile&gt; [/dev/nbd&lt;N&gt;] [notrim] [writethrough|unsafe|directsync] <br>
 <br>
Supported formats: <br>
&nbsp;qcow2 (qemu), vdi (vbox) and vmdk (vmware) <br>
 <br>
If no nbd&lt;N&gt; is given, the next available device will be allocated <br>
If auto-selected, the used /dev/nbd&lt;N&gt; will be printed on success <br>
 <br>
If you need to increase number of nbd devices: <br>
&nbsp;modprobe nbd max_part=&lt;N&gt; <br>
 <br>
Cache modes: <br>
&dash; default is writeback, flush to disk is not done every time (only on request) <br>
&dash; writethrough is slow, flush to disk is done on every write <br>
&dash; unsafe is fast, but may cause damage if having an unexpected power loss <br>
&dash; directsync is helpful when guest do not request flush when needed <br>
 <br>
Example: <br>
&nbsp;[vimage-attach](#vimage-attach) image.qcow2 /dev/nbd0 <br>
 <br>
Example with unsafe cache (highspeed): <br>
&nbsp;[vimage-attach](#vimage-attach) image.qcow2 unsafe <br>
 <br>
</tt>
<br>
<hr>

### vimage-detach
<tt>Detach virtual-machine image (from /dev/nbd&lt;n&gt;) <br>
 <br>
Usage: <br>
&nbsp;[vimage-detach](#vimage-detach) /dev/nbd&lt;N&gt; <br>
&nbsp;[vimage-detach](#vimage-detach) all <br>
 <br>
When using Linux Device Mapper (LVM,dm-crypt,...): <br>
&nbsp;Any reference under /dev/mapper/ to /dev/nbd&lt;n&gt; <br>
&nbsp;must be closed first (!) <br>
 <br>
Example on how to close an LVM group before detach: <br>
&nbsp;vgs <br>
&nbsp;vgchange -a n &lt;group&gt; <br>
 <br>
Example: <br>
&nbsp;[vimage-detach](#vimage-detach) /dev/nbd0 <br>
 <br>
</tt>
<br>
<hr>

### vimage-info
<tt>Get virtual-machine image file info <br>
 <br>
Usage: <br>
&nbsp;[vimage-info](#vimage-info) &lt;imagefile&gt; <br>
 <br>
Will fail if image is in use <br>
 <br>
Example: <br>
&nbsp;[vimage-info](#vimage-info) image.qcow2 <br>
 <br>
</tt>
<br>
<hr>

### vimage-qcow2
<tt>Create qcow2 (qemu) disk image file (sparse/compressed) <br>
 <br>
Usage: <br>
&nbsp;[vimage-qcow2](#vimage-qcow2) &lt;imagefile&gt; &lt;space&gt;[M] <br>
&nbsp;[vimage-qcow2](#vimage-qcow2) &lt;imagefile&gt; base=&lt;basefile&gt; <br>
 <br>
Space given in bytes or MiB <br>
Minimum space is 1 MiB, minimum increment is 1 MiB <br>
 <br>
If basefile is given then imagefile will contain <br>
changes relative to basefile <br>
 <br>
Owner inherited from parent directory (if superuser) <br>
 <br>
Example creating an 80000 MiB image: <br>
&nbsp;[vimage-qcow2](#vimage-qcow2) /var/lib/libvirt/images/myimage.qcow2 80000M <br>
 <br>
</tt>
<br>
<hr>

### vimage-vdi
<tt>Create vdi (vbox) disk image file (sparse) <br>
 <br>
Usage: <br>
&nbsp;[vimage-vdi](#vimage-vdi) &lt;imagefile&gt; &lt;space&gt;[M] <br>
 <br>
Space given in bytes or MiB <br>
Minimum space is 1 MiB, minimum increment is 1 MiB <br>
 <br>
Owner inherited from parent directory (if superuser) <br>
 <br>
Example creating an 80000 MiB image: <br>
&nbsp;[vimage-vdi](#vimage-vdi) image.vdi 80000M <br>
 <br>
</tt>
<br>
<hr>

### vimage-vmdk
<tt>Create vmdk (vmware) disk image file (sparse) <br>
 <br>
Usage: <br>
&nbsp;[vimage-vmdk](#vimage-vmdk) &lt;imagefile&gt; &lt;space&gt;[M] <br>
 <br>
Space given in bytes or MiB <br>
Minimum space is 1 MiB, minimum increment is 1 MiB <br>
 <br>
Owner inherited from parent directory (if superuser) <br>
 <br>
Example creating an 80000 MiB image: <br>
&nbsp;[vimage-vmdk](#vimage-vmdk) image.vmdk 80000M <br>
 <br>
</tt>
<br>
<hr>

### vol-balance
<tt>Balance live RAID volume (BTRFS only) <br>
 <br>
Usage: <br>
&nbsp;[vol-balance](#vol-balance) &lt;device&gt;|&lt;volid&gt;|&lt;label&gt;|/&lt;mountdir&gt; <br>
 <br>
See: <br>
&nbsp;[voldevice-add](#voldevice-add), [voldevice-replace](#voldevice-replace), [voldevice-remove](#voldevice-remove) <br>
 <br>
</tt>
<br>
<hr>

### vol-id
<tt>Print (or change) volume id <br>
 <br>
Print: <br>
&nbsp;[vol-id](#vol-id) &lt;vol&gt; <br>
Change: <br>
&nbsp;[vol-id](#vol-id) &lt;vol&gt; &lt;id&gt; <br>
 <br>
vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;raidlabel&gt; <br>
 <br>
Will print lowercase if uuid, and uppercase if fat32|ntfs <br>
The case needs to be preserved in /etc/fstab <br>
 <br>
Devices can be found, by volid, under /dev/disk/by-uuid/ <br>
 <br>
WARNING WHEN CHANGING ID: <br>
&nbsp;You may end up having devices with same volid, only one <br>
&nbsp;(arbitrary) device will turn up under /dev/disk/by-uuid/ <br>
 <br>
WARNING WHEN CHANGING ID ON BTRFS: <br>
&nbsp;Changing volid on BTRFS can be slow. If the command <br>
&nbsp;is interrupted, the device will not mount afterwords! <br>
&nbsp;You must then resume with: btrfstune -f -u &lt;device&gt; <br>
 <br>
Examples: <br>
&nbsp;[vol-id](#vol-id) /dev/disk/by-label/somelabel <br>
&nbsp;[vol-id](#vol-id) /dev/loop0p1 <br>
&nbsp;[vol-id](#vol-id) somezfslabel <br>
&nbsp;[vol-id](#vol-id) / <br>
 <br>
</tt>
<br>
<hr>

### vol-info
<tt>Print volume info <br>
 <br>
Usage: <br>
&nbsp;[vol-info](#vol-info) &lt;device&gt;|&lt;volid&gt;|&lt;raidlabel&gt;|/&lt;mountdir&gt; <br>
 <br>
Examples: <br>
&nbsp;[vol-info](#vol-info) /dev/loop0p1 <br>
&nbsp;[vol-info](#vol-info) 1a640c17-a9cc-4199-b4d3-a981614e8202 <br>
&nbsp;[vol-info](#vol-info) /dev/disk/by-label/somelabel <br>
&nbsp;[vol-info](#vol-info) somezfslabel <br>
&nbsp;[vol-info](#vol-info) ./somemountdir <br>
 <br>
</tt>
<br>
<hr>

### vol-label
<tt>Print (or change) volume label <br>
 <br>
Usage: <br>
&nbsp;[vol-label](#vol-label) &lt;vol&gt; [&lt;label&gt; [blank]] <br>
 <br>
vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;raidlabel&gt; <br>
 <br>
blank: allow changing to a blank label <br>
 <br>
</tt>
<br>
<hr>

### vol-list
<tt>List filesystems (volumes) available in system <br>
 <br>
Usage: <br>
&nbsp;[vol-list](#vol-list) [info] <br>
 <br>
Line format: <br>
&lt;volid&gt; <br>
 <br>
Info line format (tabbed): <br>
&lt;volid&gt; &lt;label&gt; &lt;type&gt; &lt;first device&gt; <br>
 <br>
</tt>
<br>
<hr>

### vol-scrub
<tt>Scrub live RAID volume <br>
 <br>
Usage: <br>
&nbsp;[vol-scrub](#vol-scrub) &lt;device&gt;|&lt;volid&gt;|&lt;label&gt;|/&lt;mountdir&gt; <br>
 <br>
Supported filesystems: btrfs and zfs <br>
 <br>
See also: [vol-list](#vol-list), [vol-info](#vol-info), [voldevice-check](#voldevice-check) <br>
 <br>
</tt>
<br>
<hr>

### vol-type
<tt>Print volume type (file system) <br>
 <br>
Usage: <br>
&nbsp;[vol-type](#vol-type) &lt;device&gt;|&lt;volid&gt;|&lt;raidlabel&gt;|/&lt;mountdir&gt; <br>
 <br>
Prints "unknown" if not recognized or not present <br>
 <br>
Some file systems: <br>
&nbsp;fat32, ntfs, btrfs, zfs, ext4, swap, udf, iso9660 <br>
 <br>
</tt>
<br>
<hr>

### voldevice-add
<tt>Add device(s) to existing RAID volume <br>
 <br>
Usage (btrfs and zfs): <br>
&nbsp;[voldevice-add](#voldevice-add) &lt;device&gt;,... &lt;vol&gt; <br>
 <br>
Usage (zfs special): <br>
&nbsp;[voldevice-add](#voldevice-add) &lt;device&gt;,... &lt;vol&gt; cache|spare <br>
&nbsp;[voldevice-add](#voldevice-add) &lt;device&gt;,... &lt;vol&gt; log [mirror] <br>
 <br>
vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
ZFS mirror: <br>
&dash; Added devices will form a new "vdev" mirror <br>
&dash; Adding devices later will also form a new "vdev" <br>
&dash; See [format-zfs](#format-zfs) <br>
 <br>
ZFS special devices: <br>
&dash; log (fast devices used as write cache) <br>
&dash; cache (fast devices used as read cache) <br>
&dash; spare (available as spares) <br>
 <br>
BTRFS: consider using [vol-balance](#vol-balance) afterwards <br>
 <br>
See: [vol-info](#vol-info), [format-btrfs](#format-btrfs), [format-zfs](#format-zfs) <br>
 <br>
</tt>
<br>
<hr>

### voldevice-check
<tt>Check volume on device (offline check) <br>
 <br>
Usage: <br>
&nbsp;[voldevice-check](#voldevice-check) &lt;device&gt; <br>
 <br>
Will fail if volume is mounted <br>
 <br>
Returns with exit code 0 if no errors found <br>
 <br>
</tt>
<br>
<hr>

### voldevice-remove
<tt>Remove device(s) from existing RAID volume <br>
 <br>
Usage (btrfs): <br>
&nbsp;[voldevice-remove](#voldevice-remove) &lt;device&gt;|&lt;devid&gt;,... &lt;vol&gt; <br>
 <br>
Usage (zfs): <br>
&nbsp;[voldevice-remove](#voldevice-remove) &lt;device&gt;|&lt;devname&gt;,... &lt;vol&gt; <br>
 <br>
vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
ZFS constraints: <br>
&dash; removing log, cache and spare devices will work <br>
&dash; may not work for other devices (zfs &lt; v0.8) <br>
&dash; zfs version: cat /sys/module/zfs/version <br>
 <br>
BTRFS: optionally use [vol-balance](#vol-balance) afterwards <br>
 <br>
See: [vol-info](#vol-info), [voldevice-add](#voldevice-add) <br>
 <br>
</tt>
<br>
<hr>

### voldevice-replace
<tt>Replace device in existing RAID file system (volume) <br>
 <br>
Usage (btrfs): <br>
&nbsp;[voldevice-replace](#voldevice-replace) &lt;device&gt;|&lt;devid&gt; &lt;newdevice&gt; &lt;vol&gt; <br>
 <br>
Usage (zfs): <br>
&nbsp;[voldevice-replace](#voldevice-replace) &lt;device&gt;|&lt;devname&gt; &lt;newdevice&gt; &lt;vol&gt; <br>
 <br>
vol: /&lt;mountdir&gt;|&lt;device&gt;|&lt;volid&gt;|&lt;label&gt; <br>
 <br>
BTRFS: optionally use [vol-balance](#vol-balance) afterwards <br>
 <br>
See also: [vol-info](#vol-info), [voldevice-remove](#voldevice-remove) <br>
 <br>
</tt>
<br>
<hr>

### voldevice-resize
<tt>Resize volume on a specific device <br>
 <br>
Usage: <br>
&nbsp;[voldevice-resize](#voldevice-resize) &lt;device&gt; &lt;space&gt;[M]|max <br>
 <br>
Support: <br>
&nbsp;ntfs&nbsp; # unmount before any resizing <br>
&nbsp;ext&nbsp;&nbsp; # unmount before shrinking <br>
&nbsp;btrfs # can be kept mounted during resize <br>
 <br>
BTRFS: optionally use [vol-balance](#vol-balance) afterwards <br>
 <br>
See: [device-space](#device-space), [vol-info](#vol-info) <br>
 <br>
</tt>
<br>

<hr>
<br>
generated: 2024-10-01

